# Step – 1(import necessary library)
import os
# import cv2
import pandas as pd

import urllib.request
from flask import Flask, flash, request, redirect, url_for, render_template, send_file, session
from werkzeug.utils import secure_filename

# import User defined functions
import settings
from codes import miscFunctions


app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = settings.UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


# Step – 4 (creating route for login)
@app.route('/', methods=['POST', 'GET'])
def login():
    error=""
    if (request.method == 'POST'):
        if request.form['commit'] == 'Login':  
            username = request.form.get('username')
            password = request.form.get('password')
            if username == settings.user['username'] and password == settings.user['password']:
                session['user'] = username
                return redirect('/browseFile')
            error="Please enter a valid userid or password!"
            return render_template("login.html", notification=error)
    return render_template("login.html", notification=error)

@app.route('/browseFile', methods=['POST', 'GET'])
def browseFile():
    miscFunctions.deleteFiles(settings.pdfUploadPath)
    miscFunctions.deleteFiles(settings.pdftoimagePath)
    if (request.method == 'POST'):
        if request.form['submit'] == 'Submit':
            return redirect('/last_Story1')
    return render_template('browseFile.html')


@app.route('/display/<filename>')
def display_image(filename):
    print('----------------display_image filename: -----------------' + filename)
    return redirect(url_for('output', filename='claim1/' + filename), code=301)



@app.route('/last_Story1', methods=['POST', 'GET'])
def last1():
    if 'files[]' not in request.files:
        # test_df = pd.read_csv(request.files.get('f1_name'), keep_date_col=True, encoding='latin-1')
        # flash('No file part')
        return redirect('/browseFile')

    files = request.files.getlist('files[]')
    file_names = []
    for file in files:
        if file and miscFunctions.allowed_file(file.filename):
            global selected_filename
            selected_filename = secure_filename(file.filename)
            file_names.append(selected_filename)
            # print(os.path.join(app.config['UPLOAD_FOLDER'], selected_filename))
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], selected_filename))
            # print("File name : ", selected_filename)
    # state = {'choice': filename}
    # else:
    #	flash('Allowed image types are -> png, jpg, jpeg, gif')
    #	return redirect(request.url)
    miscFunctions.image_converter(settings.pdfUploadPath)
    pics = os.listdir(settings.pdftoimagePath)
    
    image = os.listdir('static/image/undamage1/')
    count=len(pics)
    n = len(image)
    state = {'choice': selected_filename}
    # pics = os.listdir('static/damage1/')
    # image = os.listdir('static/undamage1/')
    # count=len(pics)
    # n = len(image)
    # try:
    command = "python CHBLNWrapper.py /static/uploads/claim1/"
    os.system(command)
    # except:
    #     print('error')
    # finally:

        # Combination 1
    final1 = pd.read_csv('final_result1.csv')
    final1 = final1.fillna('')
    table1 = final1.to_html(classes='content-table',index=False)
    # Combination 2
    final2 = pd.read_csv('final_result2.csv')
    final2 = final2.fillna('')
    table2 = final2.to_html(classes='content-table',index=False)
    # Combination 3
    final3 = pd.read_csv('final_result3.csv')
    final3 = final3.fillna('')
    table3 = final3.to_html(classes='content-table',index=False)
    # Combination 4
    final4 = pd.read_csv('final_result4.csv')
    final4 = final4.fillna('')
    table4 = final4.to_html(classes='content-table',index=False)
    # Combination 5
    final5 = pd.read_csv('final_result5.csv')
    final5 = final5.fillna('')
    table5 = final5.to_html(classes='content-table',index=False)
    # Combination 6
    final6 = pd.read_csv('final_result6.csv')
    final6 = final6.fillna('')
    table6 = final6.to_html(classes='content-table',index=False)

    # range_i=list(range(1,count+1))
    return render_template('last_Story1.html', pics=pics, count=count, n=n, table1=table1,table2=table2,table3=table3,table4=table4,table5=table5,table6=table6,state=state)
    # return render_template('last_Story1.html', pics=pics,count=count, n=n, file1=final1.to_html(), file2=final2.to_html(), file3=final3.to_html(), file4=final4.to_html(), file5=final5.to_html(), file6=final6.to_html())



@app.route('/download')
def download_home():
    final = pd.read_csv('final_rsult.csv')
    final = final.fillna('')
    return render_template('success.html', file=final.to_html())

@app.route('/downloads')
def download_file():

    return send_file('final_result.csv', as_attachment=True)


if __name__ == "__main__":
   app.run(debug=True, port=5000)
