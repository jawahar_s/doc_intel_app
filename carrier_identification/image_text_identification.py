import glob
import json
import os
import random
import re
import shutil

import numpy as np
import pandas as pd
from nltk.tokenize import word_tokenize
from tqdm import tqdm
# image identification modules
import pdf2image
from pdf2image import convert_from_bytes, convert_from_path
from pdf2image.exceptions import (PDFInfoNotInstalledError, PDFPageCountError,
                                  PDFSyntaxError)
from PyPDF2 import PdfFileReader

class pdf_to_text:
    def __init__(self,path_var,image_df,filename,file_path):

        self.image_df = image_df
        self.data = path_var
        self.create_folder(self.data['text_files_dir'])
        self.location = str(file_path)
        self.carrier_threshold = self.read_json(self.data['carrier_threshold'])
        self.df_text=pd.DataFrame()
        self.filename=filename

        # print(self.filename,self.location)

    def read_json(self,json_file):
        with open(json_file,'r') as f:
            data = json.load(f)
        return data

    def create_folder(self,dir_path):
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

    def convert_files(self) :
        """
        input : location of all the pdf files to be tested
        output : string format of all the data in the pdf
        """
        text_dict = {}
        self.df_text = self.image_df[pd.isnull(self.image_df).any(axis=1)].reset_index(drop=True)

        for i in range(len(self.df_text)):
            string = ("pdftotext -layout -f {} -l {} " + self.location + "/" + self.df_text['carrier_page_wise'][i].split('_')[0] +'.pdf'+ " " + (self.data['text_files_dir'] +str(self.filename).split('/')[-1].replace('.pdf',"")+".txt")).format(int(self.df_text['carrier_page_wise'][i].split('_')[-1]),int(self.df_text['carrier_page_wise'][i].split('_')[-1]))
            try :
                os.system(string)
                fil = self.data['text_files_dir']  + str(self.filename).split('/')[-1].replace('.pdf',"")+".txt"

                with open(fil) as f:
                    text = self.preprocess_text(f.read())
                    filename = self.df_text['carrier_page_wise'][i]
                    # print(filename)
                    text_dict[filename] = text
            except Exception as e:
                print("error in text read " , e)
                continue
        return text_dict

    def preprocess_text(self,lr_string) :
        lr_string = re.sub(r'[^\x00-\x7F]+',' ', lr_string)
        lr_string = re.sub(r'(\n|\t|\r)' , ' ' , lr_string)
        lr_string = re.sub(r'[\x00-\x08\x0b\x0c\x0e-\x1f\x7f-\xff]', ' ', lr_string)
        lr_string = re.sub(r'[^\w\ ]+', ' ' , lr_string)
        lr_string = re.sub(r' +' , ' ' , lr_string)

        return lr_string

    def final_imp(self):
        text_dict = self.convert_files()
        carrier_final_keywords = []
        for carrier_key in self.carrier_threshold['carrier_keywords']:
            carrier_keywords= self.carrier_threshold['carrier_keywords']['{}'.format(carrier_key)]
            carrier_final_keywords.append(carrier_keywords)
        carrier_page_wise = []
        carriers_detected = []
        carrier_final_files = []
        for i in carrier_final_keywords:
            # print(text_dict)
            for key in text_dict:
#                     print(text_dict[key])
                if any(keyword in text_dict[key] for keyword in i):
                    carrier_page_wise.append(key)
                    carriers_detected.append(i[0].split('_')[-1])
                    carrier_final_files.append(key.split('_')[0])

        self.df_text = pd.DataFrame(columns = ['carrier_page_wise','carrier_final_files','carriers_detected'])

        self.df_text['carrier_page_wise'] = carrier_page_wise
        self.df_text['carriers_detected'] = carriers_detected
        self.df_text['carrier_final_files'] = carrier_final_files
#         df_text.to_csv(self.data['text_submission_csv'],chunksize=1000,index=False)
        # print("df_text",df_text)
        return self.df_text


class pdf_to_image:
    def __init__(self,filename,path_var,file_path):
        self.filename = filename
        self.file_path=file_path
        self.data = path_var
        # print("filename ",file_path,filename)

        self.create_folder(self.data['output_dir']+str(self.filename).replace('.pdf','')+'/')

    def create_folder(self,dir_path):

        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        else:
            pass

    def convert_pdf_img(self):
        try:
            images = convert_from_path(str(self.file_path)+'/'+str(self.filename),dpi=200)
            for index, val in enumerate(images):
                val.save(self.data['output_dir']+str(self.filename).replace('.pdf','')+'/'+str(self.filename).split('.pdf')[0]+'_'+str(index+1)+".jpg", "JPEG")
        except:
            print("error in image clacification")
            pass
        return "image Completed"
