import glob
import json
import os
import shutil

import cv2
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


class preprocessing:
    def __init__(self,path_var,filename):
        self.data = path_var
        self.filename=filename

    def preprocess_logic(self):
        columns = ['carrier_page_wise','carrier_final_files']
        all_images = glob.glob(self.data['output_dir']+str(self.filename).replace('.pdf','')+"/*.jpg")
        page_wise = [i.split('/')[-1].split('.jpg')[0] for i in all_images] # Name of the pdf file+page number
        file_wise = [i.split('/')[-1].split('_')[0] for i in all_images] # Name of the pdf file
        df_files = pd.DataFrame(list(zip(page_wise,file_wise)), columns = columns) # list of tuples of pdf file + page number,pdf file name

        # df_files.to_csv(self.data['basic_carrier_info'],chunksize=1000,index=False)
        return df_files
