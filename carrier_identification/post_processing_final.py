import json
from collections import defaultdict

import numpy as np
import pandas as pd


class postprocessing:
    """
    Final module.
    input : json_file,image_dataframe and text_dataframe
    output : required dictionary
    """
    def __init__(self,path_var,image_df,text_df):
        # self.data = path_var
        self.image_df = image_df
        self.text_df = text_df

    def postprocessing_logic(self):
        final_df = self.image_df.append(self.text_df)
        final_df = final_df.drop_duplicates(subset=['carrier_page_wise'],keep='last').reset_index(drop=True)
        values = {'carriers_detected':'others', 'carrier_detected_value': np.nan}
        final_df = final_df.fillna(value=values)
        final_df = final_df.sort_values(by=['carrier_page_wise'], ascending=True)
        final_df['carriers_detected'] = final_df.apply(lambda x:np.nan if x['carriers_detected']=="others"  else x['carriers_detected'],axis=1)
        final_df['carriers_detected'] = final_df.groupby(['carrier_final_files'], sort=False)['carriers_detected'].apply(lambda x: x.ffill())
        final_dict = {}

        for i in range(len(final_df)):
            if final_df['carrier_final_files'][i] not in final_dict:
                final_dict[final_df['carrier_final_files'][i]]={}
            final_dict[final_df['carrier_final_files'][i]][final_df['carrier_page_wise'][i].split("_")[-1]]=final_df['carriers_detected'][i]

        return final_df,final_dict
