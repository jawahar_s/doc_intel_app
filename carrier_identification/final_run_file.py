import json

import numpy as np
import pandas as pd

from carrier_identification.identifying_carrier import carrier_module
from carrier_identification.image_text_identification import pdf_to_image,pdf_to_text

from carrier_identification.post_processing_final import postprocessing
from carrier_identification.pre_processing_carrier import preprocessing


class get_carrier():
    def __init__(self,filename,file_path) :
        self.filename = filename
        self.file_path = file_path
        json_file = "carrier_identification/input_json.json"

        with open(json_file,'r') as f:
            self.path_var = json.load(f)

    def image_text_dict(self):
        # convert_from_bytes(filenames,dpi=200)
        pdf_to_imagee = pdf_to_image(self.filename,self.path_var,self.file_path)
        pdf_image = pdf_to_imagee.convert_pdf_img()
        # print("pdf_image completed")

        carrier_preprocessing = preprocessing(self.path_var,self.filename)
        self.df_files = carrier_preprocessing.preprocess_logic()
        # print("carrier_preprocessing completed")
        carrier_mod = carrier_module(self.path_var,self.df_files,self.filename)
        self.image_df = carrier_mod.create_final_dataframe()
        # print("carrier_mod completed")
        pdf_text = pdf_to_text(self.path_var,self.image_df,self.filename,self.file_path)
        self.text_df = pdf_text.final_imp()
        # print("pdf_text completed")
        final_postprocessing = postprocessing(self.path_var,self.image_df,self.text_df)
        final_dataframe,final_dict = final_postprocessing.postprocessing_logic()
        final_dataframe.to_csv(self.path_var['final_submission_csv'],chunksize=1000,index=False)
        # print("final_postprocessing completed")
        return final_dict
