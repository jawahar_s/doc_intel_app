import glob
import json
import os
import pickle
from collections import Counter

import cv2
# import imutils
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
# from tqdm import tqdm_notebook


class carrier_module:
    """
    The carrier module takes images of the input carrier files and returns the detected carrier information,
    along with the detected values.

    """

    def __init__(self,path_var,df_files,filename):

        self.df_files = df_files
        self.data = path_var
        self.filename = filename


    def carrer_indi_threshold(self):
        """
        parameters:
        input: Json file of the threshold list
        output : list of carrier and respective threshold
        """
        carrier_thres = []
        thres_val = []

        # with open(self.data['carrier_indi_threshold'],'r') as f:
        #     self.threshold_data = json.load(f)
        #
        # print("threshold_data-->",self.data['threshold_list'])

        for keys in self.data['threshold_list']:
            # print(keys)
            carrier_thres.append(keys)
            thres_val.append(self.data['threshold_list'][keys])

        indiv_thre = list(zip(carrier_thres,thres_val)) # List of tuples containing the carrier name and its threshold value

        return indiv_thre

    def template_division(self):
        # print("here temp")
        """
        Parameters:
        input : folder path of the templates decided.
        output : A list containing path of templates and array information of images of respective templates.
        """
        templates = []
        paths = []
        templates_paths = self.data['templates_folder_path']

        for _path in glob.glob(templates_paths+"/*"):
            for file in glob.glob(_path+"/*.jpg"):
                paths.append(file)
                template = cv2.imread(file,0)
                templates.append(template) # Array information of each image

        temp_combined = zip(paths,templates)
        temp_combined = list(temp_combined) # List of tuples containing the image name and its array information
        # print(temp_combined)
        return temp_combined

    def final(self):
        """
        The function takes the neccesary information from the user and output matching value for templates and input carrier files.
        input:
        threshold: The threshold value by the user to measure the performace of the match.

        output: A list containing path of templates, path of images, Matching value.

        """

        unreadable_images_count = 0
        non_template_match_count = 0
        threshold = self.data['eligible_threshold']
#         print(threshold)
        debug_list = []

        #Declaring the template and read them.
        temp_combined = self.template_division()       # List of tuples that contains the image name and its array information
        indiv_threshold = self.carrer_indi_threshold() # List of tuples that contains the carrier name and the respective threshold value

        # print("temp_combined-->",temp_combined)
#         for image in tqdm_notebook(self.all_images):
        files = glob.glob(self.data['output_dir']+str(self.filename).replace('.pdf','')+"/*.jpg") # pdf has been split into several pages
        # print("files-->",files)
        for i in range(len(files)):
            file = files[i] # page-wise iteration
            try:
                crop = cv2.imread(file,0) #image has been converted into grayscale
                x = 0
                y = 0
                h = 600
                _,w = crop.shape
                img_gray = crop[y:y+h, x:x+w] # cropping the image
            except Exception as e:
                unreadable_images_count+=1
                continue
            try:
                for path,template in temp_combined:
                    res_ = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED) # detecting the logo in the page by comparing it with the template
#                     for _carrier,_thres in indiv_threshold:
#                         if path.split('/')[-2] == str(_carrier):
                    if res_.max() > threshold:
                        if res_.max() > self.data['threshold_list']['{}'.format(path.split('/')[-2])]: # checking which carrier has got threshold less than the max value

                            debug_list.append((path.split('/')[-2],path.split('/')[-2],file,res_.max())) # Carrier name,Carrier name,pdf name+page number,max value
                        else:
                            continue
                    else:
                        continue

            except Exception as e:
                print(e)
                non_template_match_count += 1
                continue
        return debug_list

    def create_final_dataframe(self):
        """
        The function outputs a "csv" file which has information of carriers_detected along with the respective threshold.

        outputs: A csv file with image name carriers detected and detected value as columns
        """
        debug_list = self.final()

        selected_list = sorted([(x[1],x[2].split('/')[-1].split('.')[0],x[2].split('/')[-1].split('.')[0].split('_')[0],x[3]) for x in debug_list],reverse=True) # Carrier name,pdf name with page number,page number,max value
        # print("selected_list------>",selected_list)
        carrier_list_detected = [x[0] for x in selected_list] # Name of the carrier
        carrier_files_pages = [x[1] for x in selected_list] # Name of the pdf + page number
        carrier_files_final = [x[2] for x in selected_list] # Name of the pdf
        carrier_detected_value = [x[3] for x in selected_list] # result of Match Template

        df = pd.DataFrame(columns = ['carrier_page_wise','carrier_final_files','carriers_detected','carrier_detected_value'])
        df['carrier_page_wise'] = carrier_files_pages # Name of the pdf + page number
        df['carrier_final_files'] = carrier_files_final # Name of the pdf selected
        df['carriers_detected'] = carrier_list_detected # Name of the carrier detected
        df['carrier_detected_value'] = carrier_detected_value # Max value 

        # print(df['carrier_page_wise'].nunique(dropna=True))
        df_group = df.groupby(['carrier_page_wise'])['carrier_detected_value'].max().reset_index()
        carriers_detected = []
        carrier_final_files = []
        carrier_detected_value = []
        carrier_page_wise = []
        for i in range(len(df)):
            for j in range(len(df_group)):
                try:
                    if df['carrier_page_wise'][i] == df_group['carrier_page_wise'][j] and df['carrier_detected_value'][i] == df_group['carrier_detected_value'][j]:
                        carrier_page_wise.append(df['carrier_page_wise'][i])
                        carrier_final_files.append(df['carrier_final_files'][i])
                        carriers_detected.append(df['carriers_detected'][i])
                        carrier_detected_value.append(df['carrier_detected_value'][i])
                    else:
                        continue
                except:
                    continue
        df_final = pd.DataFrame(list(zip(carrier_page_wise,carrier_final_files,carriers_detected,carrier_detected_value)),columns=['carrier_page_wise','carrier_final_files','carriers_detected','carrier_detected_value'])
        # print("df_final",df_final)
        last_df = df_final.append(self.df_files)
        last_df = last_df.drop_duplicates(subset=['carrier_page_wise']).reset_index(drop=True)
        values = {'carriers_detected':'others', 'carrier_detected_value': np.nan}
        last_df = last_df.fillna(value=values)
#         last_df.to_csv(self.data['image_submission_csv'],chunksize=1000,index=False)
        return last_df
