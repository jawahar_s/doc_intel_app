from pipeline_main import *
from time import perf_counter,strftime
import os
import shutil
import sys, getopt
import pandas as pd
import requests
import json

success = {"success code": str(1),}
failure = {"success code": str(0),}

def lossrunexec(argv):

    try:
        opts, args = getopt.getopt(argv,"i")
    except getopt.GetoptError:
        sys.exit(2)

    input_path=" ".join(args[:])

    starttime = perf_counter()
    execstarttime = strftime('%Y-%m-%d %H:%M:%S')
    headers = {'content-type': "application/json",'cache-control': "no-cache"}

    file_timestamp = str(strftime('%Y%m%d%H%M%S').replace("/","").replace(":","").replace(" ",""))
    for file in os.listdir(input_path):
        result = modelexec(file,input_path,file_timestamp)
        if os.path.exists('OutTable/Temp/'):
            shutil.rmtree('OutTable/Temp/')
        if os.path.exists('unwanted_files_img'+file.replace('.pdf','')+'/'):
            shutil.rmtree('unwanted_files_img'+file.replace('.pdf','')+'/')


if __name__ == '__main__':
    lossrunexec(sys.argv[1:])
