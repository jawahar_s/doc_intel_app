import sys
import time
# sys.path.extend(["/opt/ansh/CV/Loss_run_pipeline/loss_run_ingestion/carrier_specific_extraction/"])
from carrier_identification import final_run_file
from helper_functions import camelot_extractor
from helper_functions import Postprocess_module
from comb_in_out_table import in_out
from OutTable import run
import pandas as pd
from time import perf_counter,strftime
import numpy as np
import re
from  fuzzywuzzy import fuzz
import os
import warnings
import requests
import json
warnings.filterwarnings("ignore")


def modelexec(filename,file_content_type,file_timestamp):
        result={}
        
        flag_Philadelphia = False
#     try:
#         execstarttime = strftime('%Y-%m-%d %H:%M:%S')
#         headers = {'content-type': "application/json",'cache-control': "no-cache"}
#         response = requests.get('http://nausd-lapp0350.chubb.com:8080/lossrun/api/LookupMaster')
#         jsonResponse = response.json()

#         for l in jsonResponse:
#             if l['type'] == "ExecSummary":
#                 masterexecid = l['id']
#             if l['type'] == "ExecDetail":
#                 execid = l['id']

        multi_carr_table=[]
        file_path=file_content_type
        Ci_ = final_run_file.get_carrier(filename,file_path)
        distribution_dict = Ci_.image_text_dict()
        #print("distribution_dict",distribution_dict)
        Td_ = camelot_extractor.Table_extractor(file_path)
        lst_dict=[]
        for dict in distribution_dict.values():
            for lst in list(set(dict.values())):
                if str(lst) not in lst_dict:
                    lst_dict.append(str(lst))

        for lst in lst_dict:
            if str(lst) == 'Tokio':
                table,imgae_file_list_camelot,error_files,no_loss_files = Td_.extractor_tabula(filename)
                new_table = Td_.pre_process_code(table)
            elif str(lst) == 'Philadelphia':
                table,imgae_file_list_camelot,error_files,no_loss_files = Td_.extractor_tabula(filename)
                new_table = Td_.pre_process_code(table)
                flag_Philadelphia = True
#                 print("insside Philadelphia-->",new_table)
            elif str(lst) != 'nan':
                table,imgae_file_list_camelot,error_files,no_loss_files = Td_.extractor(filename,lst)
                new_table = Td_.pre_process_code(table)
            elif str(lst) == 'nan' and len(list(set(lst_dict)))==1:
                raise Exception("carrier out of scope")
                ## print('str(lst)==nan',str(lst),len(set(lst_dict)))
                #result={}
                #result['error'] =["file out of schema"]
                #result['success code'] = 0
                #d = {'filename': filename, 'error': 'carrier identification fail'}
                #final_result = pd.DataFrame(data=d,index=[0])
                #
                #final_result.to_csv('error.csv',mode='a', header=False)
                #
                #return  'result'

        pp_ = Postprocess_module.Postprocess_module()
        results = pp_.post_process(lst,new_table)
        #print("intable results",results)
        results.to_csv("intable_results.csv", mode='a', header=True)
        start_time = time.time()
        outtable=run.main_final(distribution_dict,filename,file_path)
#         print("Total time taken :", time.time()-start_time)
        #print("outtable result final -->",outtable)
        io_ = in_out()
        in_out_table_results = io_.merage_in_out(outtable,results)
        #print("intable out table results ==>",in_out_table_results)
        final_result=io_.get_defined_columns(in_out_table_results)
        #print("intable out table results ==>",final_result)
        #final_result.to_csv("final_result.csv", mode='a', header=False)
        #print("-----------------Data saved to CSV --------------------")
        #print("flag_Philadelphia -------", flag_Philadelphia)

        final_result['CarrierName']=final_result.CarrierName.apply(lambda x: re.sub('\d','',x) if x !="" else x)
        final_result['SubmissionID']=final_result.SubmissionID.apply(lambda x: x.split('/')[-1].split('.')[0] if x !="" else x)
        final_result['ClosedDate']=final_result.ClosedDate.apply(lambda x: '' if len(re.findall(r'[A-Za-z]+', str(x)))>0 else x)
        #print("final_result['TotalGrossIncurred']-----------", final_result['TotalGrossIncurred'])
        
        if flag_Philadelphia == True :
            final_result['TotalGrossIncurred']=""
        else:
                final_result['TotalGrossIncurred']=final_result['TotalGrossIncurred'].astype(str).str.replace('$','').str.replace(',','')
        final_result['TotalPaid']=final_result['TotalPaid'].astype(str).str.replace('$','').str.replace(',','')
        final_result['TotalRecoveries']=final_result['TotalRecoveries'].astype(str).str.replace('$','').str.replace(',','')
        final_result['TotalReserve']=final_result['TotalReserve'].astype(str).str.replace('$','').str.replace(',','')
        final_result['MasterModelCode']="CH01LR"
        #final_result['ExecID']=execid
        #print("final_result-----------------",final_result)
        final_result1 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness']]#1
        final_result2 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate']]#2
        final_result3 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate','NoticeDate']]#3
        final_result4 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate','NoticeDate','ClosedDate']]#4
        final_result5 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate','NoticeDate','ClosedDate','ClaimStatus']]#5
        final_result6 = final_result[['CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate','NoticeDate','ClosedDate','ClaimStatus','TotalGrossIncurred']]#6
        final_result1.to_csv("final_result1.csv", mode='w', index=False, header=True)
        final_result2.to_csv("final_result2.csv", mode='w', index=False, header=True)
        final_result3.to_csv("final_result3.csv", mode='w', index=False, header=True)
        final_result4.to_csv("final_result4.csv", mode='w', index=False, header=True)
        final_result5.to_csv("final_result5.csv", mode='w', index=False, header=True)
        final_result6.to_csv("final_result6.csv", mode='w', index=False, header=True)
        return final_result
#         Master_table = final_result.fillna("")

#         ofilename = './results/Master_table_' + file_timestamp + '.csv'

#         if not os.path.isfile(ofilename):
#             Master_table.to_csv(ofilename)
#         else:
#             Master_table.to_csv(ofilename, mode='a', header=False)

#         #Master_table.drop(Master_table.columns[[0]], axis = 1, inplace = True)#Drop index column
#         Master_table.drop(columns=['No Losses', 'Page#', 'Order'], inplace = True)
#         Master_table = Master_table.fillna("")
#         json_master_table=Master_table.to_json(orient='records')

#         response = requests.request("POST", "http://nausd-lapp0350.chubb.com:8080/lossrun/api/LossRunData", data=json_master_table, headers=headers)
#         print(response.text)


#         execdtl = "["+json.dumps({'masterexecid':masterexecid,'modelcode':'CH01LR001','sourcepath':file_path,'sourcefilename':filename.split('.')[0],'sourcefiletype':filename.split('.')[-1],'sourcefilesize':os.path.getsize(file_path+'/'+filename)/1000,'sourcecarriername':Master_table.CarrierName.iloc[0],'processedfilename':'Master_table_' + file_timestamp + '.csv','processedpath':'','execstarttime':execstarttime,'exceendtime':strftime('%Y-%m-%d %H:%M:%S'),'status':'Success'})+"]"
#         response = requests.request("POST", "http://nausd-lapp0350.chubb.com:8080/lossrun/api/ExecutionDetails", data=execdtl, headers=headers)
#         print("response:",response.text)

#         result['success code'] = 1
#         return  result

#     except Exception as e:

#         execdtl = "["+json.dumps({'masterexecid':masterexecid,'modelcode':'CH01LR001','sourcepath':file_path,'sourcefilename':filename.split('.')[0],'sourcefiletype':filename.split('.')[-1],'sourcefilesize':os.path.getsize(file_path+'/'+filename)/1000,'sourcecarriername':'','processedfilename':'','processedpath':'','execstarttime':execstarttime,'exceendtime':strftime('%Y-%m-%d %H:%M:%S'),'status':'Failure'})+"]"

#         response = requests.request("POST", "http://nausd-lapp0350.chubb.com:8080/lossrun/api/ExecutionDetails", data=execdtl, headers=headers)
#         print("error response:",response.text)

#         result['success code'] = 0
#         return  result


# def modelexec(filename,file_content_type):
#     multi_carr_table=[]
#     file_path=file_content_type
#     Ci_ = final_run_file.get_carrier(filename,file_path)
#     distribution_dict = Ci_.image_text_dict()
#     print("distribution_dict",distribution_dict)
#     Td_ = camelot_extractor.Table_extractor(file_path)
#     lst_dict=[]
#     for dict in distribution_dict.values():
#         for lst in list(set(dict.values())):
#             if str(lst) not in lst_dict:
#                 lst_dict.append(str(lst))
#
#     for lst in lst_dict:
#         if str(lst) == 'Tokio':
#             table,imgae_file_list_camelot,error_files,no_loss_files = Td_.extractor_tabula(filename)
#             new_table = Td_.pre_process_code(table)
#         elif str(lst) != 'nan':
#             table,imgae_file_list_camelot,error_files,no_loss_files = Td_.extractor(filename,lst)
#             new_table = Td_.pre_process_code(table)
#         elif str(lst) == 'nan' and len(list(set(lst_dict)))==1:
#             # print('str(lst)==nan',str(lst),len(set(lst_dict)))
#             result={}
#             result['error'] =["file out of schema"]
#             result['success code'] = 0
#             d = {'filename': filename, 'error': 'carrier identification fail'}
#             final_result = pd.DataFrame(data=d,index=[0])
#
#             final_result.to_csv('error.csv',mode='a', header=False)
#
#             return  'result'
# #
# # #     print("imgae_file_list_camelot-->",imgae_file_list_camelot,"error_files-->",error_files)
#     pp_ = Postprocess_module.Postprocess_module()
#     results = pp_.post_process(lst,new_table)
#     # print("intable completed final result --->",results)
#
#     outtable=run.main_final(distribution_dict,filename,file_path)
#     print("outtable result final -->",outtable)
#     io_ = in_out()
#     in_out_table_results = io_.merage_in_out(outtable,results)
#     print("intable out table results ==>",in_out_table_results)
#     final_result=io_.get_defined_columns(in_out_table_results)
#     print("intable out table results ==>",final_result)
#     final_result.to_csv('test.csv',mode='a', header=False)
#     final_result['CarrierName']=final_result.CarrierName.apply(lambda x: re.sub('\d','',x) if x !="" else x)
#     final_result['SubmissionID']=final_result.SubmissionID.apply(lambda x: x.split('/')[-1].split('.')[0] if x !="" else x)
#     final_result['ClosedDate']=final_result.ClosedDate.apply(lambda x: '' if len(re.findall(r'[A-Za-z]+', str(x)))>0 else x)
#     final_result['TotalGrossIncurred']=final_result['TotalGrossIncurred'].str.replace('$','').str.replace(',','')
#     final_result['TotalPaid']=final_result['TotalPaid'].str.replace('$','').str.replace(',','')
#     final_result['TotalRecoveries']=final_result['TotalRecoveries'].str.replace('$','').str.replace(',','')
#     final_result['TotalReserve']=final_result['TotalReserve'].str.replace('$','').str.replace(',','')
#     final_result['MasterModelCode']="CH01LR"
#     final_result['ExecID']=execid
#     final_result = final_result.fillna("")
#     return  'result'
