import settings
import os
import shutil
from pdf2image import convert_from_path
from pdf2image.exceptions import (
	PDFInfoNotInstalledError,
	PDFPageCountError,
	PDFSyntaxError
)

def __init__(self,):
    return

def deleteFiles(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

def image_converter(folder):
	path = os.listdir(folder)
	for filePath in path:
		if filePath.endswith('.pdf'):
			image = convert_from_path(poppler_path=settings.poppler_bin_path , pdf_path=folder+"/"+filePath)
			name = filePath.split('.')[-2]
			for i, j in enumerate(image):
				j.save(settings.pdftoimagePath + name + "_" + str(i) + ".png", "PNG")

def allowed_file(filename):
   return '.' in filename and filename.rsplit('.', 1)[1].lower() in settings.ALLOWED_EXTENSIONS