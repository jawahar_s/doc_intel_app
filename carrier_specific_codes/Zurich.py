import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class Zurich() :

    def __init__(self):
        self.lob_lst=['Auto Liab (Prop damage/rental)','AUTO LIABILITY / PD ONLY','Auto Liability - Bodily Injury','AUTO LIABILITY BODILY INJ',
             'Auto Bodily Injury','WORKERS COMP MEDICAL ONLY','FIDELITY & SURETY','AUTO PHYSICAL DAMAGE','Auto Property Damage',
             'EXCESS GENERAL LIABILITY','GENERAL LIABILITY BI','AUTO LIAB MED PAY ONLY',
             'Directors and Officers','Errors and Omissions','Employment Practices Liability',
             'Kidnap and Ransom','Fire and Allied','Property and Casualty','Commercial Package Policy',
             'Special Multi-Peril','POLLUTION COVERAGES','Commercial Umbrella','Business Auto','Commercial Package',
             'CONTRACTUAL LIABILITY','WORKERS COMPENSATION','professional liability','PRODUCT LIABILITY','general liability',
             'GARAGE LIABILITY','Public Liability','Inland Marine','Property Damage','Auto Liability','Premises Operations',
             'Fin. Fidelity','Financial Lines','PRODUCTS','EARTHQUAKE','Fiduciary','FIDELITY','FIRE','PROPERTY','BURGLARY',
             'AUTOMOBILE','Crime','Auto']
        self.replace_dict = {}
#         print("", end="")

    def Zurich(self,new_table):
        df=self.get_popular_table(new_table)
        df=self.Post_Processing(df)
        return df


    ls = []
    def is_lob(self,string):
        percent_match={}
        for _val in self.lob_lst:
            ls = self.match_keys(str(string),str(_val))

            try:
                if ls[1]>=70:
                      percent_match[_val]=ls[1]
                else:
                    continue
            except Exception as e:

                continue
        if not percent_match: return ''
        else:
    #         print(percent_match)
            return sorted(percent_match.items(), key =lambda kv:(kv[1]),reverse=True)[0][0]


    def replace_text(self,text) :
        for k,v in self.replace_dict.items():
            text = text.replace(k,v)
        return text

    def lcs_sim(self, s1_text, s2_text , fuzzy_thresh=70):
        s1_text , s2_text = self.replace_text(s1_text) , self.replace_text(s2_text)
        s1 = s1_text.split(' ')#nltk.word_tokenize(s1_text)
        s2 = s2_text.split(' ')
        matrix = [["" for x in range(len(s2))] for x in range(len(s1))]
        for i in range(len(s1)):
            for j in range(len(s2)):
                if fuzz.ratio(s1[i] , s2[j]) >= 70 :
                    if i == 0 or j == 0:
                        matrix[i][j] = s1[i]# + '|' + str(i)
                    else:
                        matrix[i][j] = matrix[i-1][j-1] + ' ' + s1[i]# + '|' + str(i)
                else:
                    matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1],
                                       key=lambda x: fuzz.ratio(x , s2[j]) + len(x.split()))

        matrix = [x for x in matrix if x and x[-1] in s1_text]
        cs = matrix[-1][-1]
        if fuzz.ratio(cs,s2_text) > 0:
            return cs,fuzz.ratio(cs,s2_text)#,len(cs.split(' ')), cs
        else:
            return ''#,0

    def match_keys(self, text,key):
        text = text.lower()
        key = key.lower()
        return self.lcs_sim(text,key)

    def get_lob_16(self,y1,y2,y3,y4,y5,y6,y7):
        lob_16_lst=[y1,y2,y3,y4,y5,y6,y7]
        len_lob=[len(str(y)) for y in lob_16_lst]
        max_len=max(len_lob)
        if max_len==0: return ''
        return lob_16_lst[len_lob.index(max_len)]

    def Zur_Post_Processing_16(self,popular_table):
        popular_table['CarrierName'] ="Zurich"
        popular_table['No Losses']  = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('no claim').any()  else "", axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Date of Claimant'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['Date of Claimant'].str.extract('(\d{8})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber3'] = popular_table.apply(lambda x:x['Date of Claimant'] if (len(str(x['CarrierClaimNumber'])) >7 and x['Total_Gross_Incurred']=="" and len(str(x['Date of Claimant'])) ==10) else x['CarrierClaimNumber']  ,axis=1).shift(-1)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber4'] = popular_table['Indemnity Status'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber5'] = popular_table['Loss_State'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table.apply(lambda x:x['Loss_Date'].split()[0]  if ((len(str(x['Loss_Date']).split()) >=1 ) and (len(str(x['Loss_Date']).split()[0]) ==10) and not x['CarrierClaimNumber'] ) else x['CarrierClaimNumber'],axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].apply(lambda x:x if ((len(str(x)) ==10) and ("/" not in str(x) )) else np.nan)
    #     popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'].ffill(axis = 0,inplace=True)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['Total_Gross_Incurred'] if '$' in str(x['Total']) else x['Total_Paid'],axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['Total_Paid'] if str(x['TotalPaid'])=="" else x['Total_Paid'],axis=1 )

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total'] if '$' in str(x['Total']) else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total_Gross_Incurred'] if (('$' in str(x['Total_Gross_Incurred'])) and (str(x['TotalGrossIncurred']) =="")) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Exp PD/LT'] if (('$' in str(x['Indemnity Date'])) and ( str(x['TotalGrossIncurred']) =="") and ('$' in x['Exp PD/LT'])) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['BI/Med'] if (('$' in str(x['Loss_Date'])) and ( str(x['TotalGrossIncurred']) =="") and ('$' in str(x['BI/Med']))) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total_Gross_Incurred'] if ((str(x['Total_Gross_Incurred']).replace(",","").replace(".","").isdigit()) and ( str(x['TotalGrossIncurred']) =="") ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Incurred Exp Paid'] if( ((str(x['Incurred Exp Paid']).replace(",","").replace(".","").isdigit()) or (str(x['Indemnity Status']).replace(",","").replace(".","").isdigit()) ) and ( str(x['TotalGrossIncurred']) =="") )  else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total_Paid'] if(  (str(x['Total_Paid']).replace(",","").replace(".","").isdigit())  and ( str(x['TotalGrossIncurred']) =="") )  else x['TotalGrossIncurred'] ,axis=1)

        popular_table['CoverageType1'] = popular_table['Date of Claimant'].shift(-1)
        popular_table['CoverageType2'] = popular_table['Date of Claimant'].shift(-2)
        popular_table['CoverageType'] = popular_table.apply(lambda x:str(x['Date of Claimant']) +" " + str(x['CoverageType1'])+" " + str(x['CoverageType2']),axis=1)

        popular_table['CoverageType3'] = popular_table['claim number'].shift(-1)
        popular_table['CoverageType4'] = popular_table.apply(lambda x:str(x['claim number']) +" " + str(x['CoverageType3']),axis=1)

        popular_table['CoverageType5'] = popular_table['Loss_Date'].shift(-1)
        popular_table['CoverageType6'] = popular_table['Loss_Date'].shift(-2)
        popular_table['CoverageType7'] = popular_table.apply(lambda x:str(x['Date of Claimant']) +" " + str(x['CoverageType5'])+" " + str(x['CoverageType6']),axis=1)

        popular_table['CoverageType8'] = popular_table.apply(lambda x:str(x['Date of Claimant']) +" " + str(x['CoverageType5'])+" " + str(x['CoverageType2']),axis=1)

        popular_table['CoverageType9'] = popular_table.apply(lambda x:str(x['Loss_Date']) +" " + str(x['CoverageType5']),axis=1)

        popular_table['CoverageType10'] = popular_table['claim number'].shift(-2)
        popular_table['CoverageType11'] = popular_table.apply(lambda x:str(x['Date of Claimant']) +" " + str(x['CoverageType1'])+" " + str(x['CoverageType10']),axis=1)

        popular_table['CoverageType12'] = popular_table.apply(lambda x:str(x['CoverageType3']) +" " + str(x['CoverageType10']),axis=1)

        popular_table['LossDescription'] = popular_table.apply(lambda x: x['Date of Claimant'] if  "Acc"  in str(x['claim number']) else "" ,axis=1).shift(-3)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss_Date'] if (('/' in str(x['Loss_Date'])) and (str(x['Loss_Date']).count('/')==2)) else "",axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss_State'] if ("/" in str(x['Loss_State']) and str(x['Loss_State']).count("/")>=2 and "/" not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Claim_Status'] if ("/" not in str(x['LossDate']) and "/" in str(x['Claim_Status']) and str(x['Claim_Status']).count("/")>=2) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Indemnity Status'] if (('/' not in str(x['Loss_Date'])) and ('/' in str(x['Indemnity Status']) and "/" not in x['LossDate'])) else (str(x['Indemnity Date'])[:10] if (('/' not in str(x['LossDate'])) and ('/' in str(x['Indemnity Date']))) else x['LossDate']),axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Loss_Date'])[:10] if ( ('/' not in str(x['LossDate'])) and ('/' in str(x['Loss_Date'])) ) else ( str(x['claim number'])[:10] if (('/' not in str(x['LossDate'])) and ('/' in str(x['claim number']))) else ( str(x['Date of Claimant'])[:10] if (('/' not in str(x['LossDate'])) and ('/' in str(x['Date of Claimant']))  )  else x['LossDate'] ) )  ,axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Loss_State'].split()[0][:10]) if ( (len(str(x['Loss_State']).split()) >=1 ) and  ('/' not in str(x['LossDate'])) and ('/' in str(x['Loss_State']).split()[0])) else x['LossDate'], axis=1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Loss_Date']).split("Closed")[0][-10:] if ('Closed' in str(x['Loss_Date']) and '/' in  str(x['Loss_Date'])) else (str(x['Loss_Date']).split("Open")[0][-10:] if ('Open' in str(x['Loss_Date']) and ('/' in  str(x['Loss_Date'])))  else x['LossDate'] ) ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Loss_State']).split("Closed")[0][-10:] if ('Closed' in str(x['Loss_State']) and '/' in  str(x['Loss_State'])) else (str(x['Loss_State']).split("Open")[0][-10:] if ('Open' in str(x['Loss_State']) and ('/' in  str(x['Loss_State'])))  else x['LossDate'] ) ,axis =1 )
    #     print(popular_table['LossDate'].values[0].replace("/","").replace(" ","").isdigit())
        popular_table['LossDate'] = popular_table.apply(lambda x: x['LossDate'] if ("/" in str(x['LossDate']) and str(x['LossDate']).count("/")>=2  and str(x['LossDate']).replace("/","").replace(" ","").isdigit()) else None,axis=1)
    #     popular_table['LossDate'].ffill(axis=0,inplace=True)

        popular_table['LossState'] = popular_table.apply(lambda x:x['Loss_State'] if str(x['Loss_State']) == str(x['Loss_State']).upper() else "" ,axis=1)
        popular_table['LossState'] = popular_table.apply(lambda x:x['Claim_Status'] if str(x['Claim_Status']) == str(x['Claim_Status']).upper()  else x['LossState'],axis=1)


        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Loss_Date'])) or ('Close' in str(x['Loss_Date'])) or ('CLOSE' in str(x['Loss_Date']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Loss_Date'])) or ('Open' in str(x['Loss_Date'])) or ('OPEN' in str(x['Loss_Date']) )) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Loss_State'])) or ('Close' in str(x['Loss_State']))  or ('CLOSE' in str(x['Loss_State']) )) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Loss_State'])) or ('Open' in str(x['Loss_State'])) or ('OPEN' in str(x['Loss_State']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Claim_Status'])) or ('Close' in str(x['Claim_Status'])) or ('CLOSE' in str(x['Claim_Status']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Claim_Status'])) or ('Open' in str(x['Claim_Status'])) or ('OPEN' in str(x['Claim_Status']) )) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Indemnity Status'])) or ('Close' in str(x['Indemnity Status'])) or ('CLOSE' in str(x['Indemnity Status']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Indemnity Status'])) or ('Open' in str(x['Indemnity Status'])) or ('OPEN' in str(x['Indemnity Status'])) ) else x['ClaimStatus'],axis=1)
    #     print(popular_table['ClaimStatus'])
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Date of Claimant'])) or ('Close' in str(x['Date of Claimant'])) or ('CLOSE' in str(x['Date of Claimant'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Date of Claimant'])) or ('Open' in str(x['Date of Claimant'])) or ('OPEN' in str(x['Date of Claimant'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].apply(lambda x:x if (('close' in str(x)) or ('Close' in str(x)) or ('open' in str(x)) or ('Open' in str(x)) ) else "")
    #     popular_table['ClaimStatus'].ffill(axis = 0,inplace=True)

        popular_table['Indemnity Status1'] = popular_table['Indemnity Status'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else "")

        popular_table['ClosedDate']=popular_table['BI/Med'].apply(lambda x:x if ('/' in str(x) and str(x).count("/")>=2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:x['Ind Date'] if ('/' in str(x['Ind Date']) and str(x['Ind Date']).count("/")>=2 and "/" not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate']=popular_table.apply(lambda x:x['Indemnity Date'] if (('/' in str(x['Indemnity Date']) and str(x['Indemnity Date']).count("/")>=2) and ('/' not in str(x['ClosedDate']))) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Loss_Date']).split("Closed")[1][:11] if ( ('Closed' in str(x['Loss_Date'])) and ('/' in  str(x['Loss_Date'])) ) else (str(x['Loss_Date']).split("Open")[1][:11] if ('Open' in str(x['Loss_Date']) and ('/' in  str(x['Loss_Date'])))  else x['ClosedDate'] ) ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Loss_State']).split("Closed")[1][:11] if ( ('Closed' in str(x['Loss_State'])) and ('/' in  str(x['Loss_State'])) ) else (str(x['Loss_State']).split("Open")[1][:11] if ('Open' in str(x['Loss_State']) and ('/' in  str(x['Loss_State'])))  else x['ClosedDate'] ) ,axis =1 )
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")==2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

        popular_table['NoticeDate']=popular_table.apply(lambda x:x['Indemnity Status1'] if ("/" in str(x['Indemnity Status1']) and str(x['Indemnity Status1']).count("/")>=2) else "",axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Indemnity Status1'] if ("/" in str(x['NoticeDate']) and str(x['NoticeDate'])==str(x['Indemnity Status1'])) else "",axis=1)
        popular_table['NoticeDate']=popular_table.apply(lambda x:x['Indemnity Date'] if ('/' in str(x['Indemnity Date']) and str(x['Indemnity Date']).count("/")>=2 and "/" not in str(x['NoticeDate'])) else x['NoticeDate'],axis=1)
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")==2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

        popular_table['LineOfBusiness1'] = popular_table.apply(lambda x:x['CoverageType'] if ( len(x['CoverageType'].split())>1 and ((x['CoverageType'].lower() in set(self.lob_lst)) or (x['CoverageType'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType']),axis=1)
        popular_table['LineOfBusiness2'] = popular_table.apply(lambda x:x['CoverageType4'] if ( len(x['CoverageType4'].split())>1 and ((x['CoverageType4'].lower() in set(self.lob_lst)) or (x['CoverageType4'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType4']),axis=1)
        popular_table['LineOfBusiness3'] = popular_table.apply(lambda x:x['CoverageType7'] if ( len(x['CoverageType7'].split())>1 and ((x['CoverageType7'].lower() in set(self.lob_lst)) or (x['CoverageType7'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType7']),axis=1)
        popular_table['LineOfBusiness4'] = popular_table.apply(lambda x:x['CoverageType8'] if ( len(x['CoverageType8'].split())>1 and ((x['CoverageType8'].lower() in set(self.lob_lst)) or (x['CoverageType8'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType8']),axis=1)
        popular_table['LineOfBusiness5'] = popular_table.apply(lambda x:x['CoverageType9'] if ( len(x['CoverageType9'].split())>1 and ((x['CoverageType9'].lower() in set(self.lob_lst)) or (x['CoverageType9'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType9']),axis=1)
        popular_table['LineOfBusiness6'] = popular_table.apply(lambda x:x['CoverageType11'] if ( len(x['CoverageType11'].split())>1 and ((x['CoverageType11'].lower() in set(self.lob_lst)) or (x['CoverageType11'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType11']),axis=1)
        popular_table['LineOfBusiness7'] = popular_table.apply(lambda x:x['CoverageType12'] if ( len(x['CoverageType12'].split())>1 and ((x['CoverageType12'].lower() in set(self.lob_lst)) or (x['CoverageType12'].upper() in set(self.lob_lst)))) else self.is_lob(x['CoverageType12']),axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.get_lob_16(x['LineOfBusiness1'],x['LineOfBusiness2'],x['LineOfBusiness3'],x['LineOfBusiness4'],x['LineOfBusiness5'],x['LineOfBusiness6'],x['LineOfBusiness7']),axis=1)
        popular_table = popular_table[((popular_table['ClosedDate'].str.contains("/")) | (popular_table['NoticeDate'].str.contains("/")) | (popular_table['ClaimStatus']!=""))]
        ##     popular_table = popular_table[popular_table.TotalGrossIncurred.str.contains("$")]
        popular_table.drop(['CarrierClaimNumber3', 'CarrierClaimNumber2','CarrierClaimNumber1','CarrierClaimNumber5','CarrierClaimNumber4'],inplace=True, axis=1)
        popular_table.drop(['LineOfBusiness1','LineOfBusiness2','LineOfBusiness3','LineOfBusiness4','LineOfBusiness5','LineOfBusiness6',#'LineOfBusiness7',,'CoverageType12'
                           'CoverageType1','CoverageType2','CoverageType3','CoverageType4','CoverageType5','CoverageType6',
                           'CoverageType7','CoverageType8','CoverageType9','CoverageType10','CoverageType11'],inplace=True, axis=1)

        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['TotalPaid1'] = popular_table['TotalPaid'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:"$" + x['TotalPaid1'] if ((x['TotalGrossIncurred1'] and x['TotalPaid1'] and float(x['TotalPaid1'])>=float(x['TotalGrossIncurred1']))
                                            or (x['TotalPaid1'] and not x['TotalGrossIncurred1'] )) else ("$" + x['TotalGrossIncurred1'] if x['TotalGrossIncurred1'] else ''),axis=1)

        popular_table = popular_table.drop(['TotalPaid1','TotalGrossIncurred1'],axis=1)
        return popular_table

    def loss_date_fetch_14(self,df,clm):
        for ab in str(df[clm]).split():
            try:
                if '/' in ab:
    #                 print(ab[:10])
                    return ab[:10]
                else:
                    return ""
            except:
                continue

    def loss_date(self,df,clm):
        if 'Closed' in str(df[clm]) :
    #         print(df['Claim Number'].split("Closed")[0][-10:])
            return df[clm].split("Closed")[0][-10:]

        elif 'Open'in str(df[clm]):
            return df[clm].split("Open")[0][-10:]


        else:return ""

    def close_date(self,df,clm):
        if 'Closed' in str(df[clm]) :
            return df[clm].split("Closed")[1][:11]
        elif 'Open'in str(df[clm]):
            return df[clm].split("Open")[1][:11]


        else:return ""

    def Zur_Post_Processing_14(self,popular_table):
        popular_table['CarrierName'] ="Zurich"
        popular_table['No Losses']  = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('no claim').any()  else "", axis=1)
    #     try:
        popular_table['CarrierClaimNumber']  = popular_table['Claim Number'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Insured Name'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['Date of Loss'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber3'] = popular_table['Client Name'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
    #     popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'].ffill(axis = 0,inplace=True)


        popular_table['Total Net Incurred1'] = popular_table.apply(lambda x: str(x['Total Net Incurred']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Total Net Incurred']) else "0",axis=1)
        popular_table['Recovery Total1'] = popular_table.apply(lambda x: str(x['Recovery Total']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Recovery Total']) else "0",axis=1)
        popular_table['Claim Recovery Total1'] = popular_table.apply(lambda x: str(x['Claim Recovery Total']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Claim Recovery Total']) else "0",axis=1)
        popular_table['Paid Total  Reserves Total1'] = popular_table.apply(lambda x: str(x['Paid Total  Reserves Total']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Paid Total  Reserves Total']) else "0",axis=1)
        popular_table['Paid Expense  Reserves Expense1'] = popular_table.apply(lambda x: str(x['Paid Expense  Reserves Expense']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Paid Expense  Reserves Expense']) else "0",axis=1)

        popular_table['Total Net Incurred2'] = popular_table.apply(lambda x: float(x['Total Net Incurred1'].split()[-2]) + float(x['Total Net Incurred1'].split()[-1]) if str(x['Total Net Incurred1']).count(".")>=2 else x['Total Net Incurred1'],axis=1)
        popular_table['Recovery Total2'] = popular_table.apply(lambda x: float(x['Recovery Total1'].split()[-2]) + float(x['Recovery Total1'].split()[-1]) if str(x['Recovery Total1']).count(".")>=2 else x['Recovery Total1'],axis=1)
        popular_table['Claim Recovery Total2'] = popular_table.apply(lambda x: float(x['Claim Recovery Total1'].split()[-2]) + float(x['Claim Recovery Total1'].split()[-1]) if str(x['Claim Recovery Total1']).count(".")>=2 else x['Claim Recovery Total1'],axis=1)
        popular_table['Paid Total  Reserves Total2'] = popular_table.apply(lambda x: float(x['Paid Total  Reserves Total1'].split()[-2]) + float(x['Paid Total  Reserves Total1'].split()[-1]) if str(x['Paid Total  Reserves Total1']).count(".")>=2 else x['Paid Total  Reserves Total1'],axis=1)
        popular_table['Paid Expense  Reserves Expense2'] = popular_table.apply(lambda x: float(x['Paid Expense  Reserves Expense1'].split()[-2]) + float(x['Paid Expense  Reserves Expense1'].split()[-1]) if str(x['Paid Expense  Reserves Expense1']).count(".")>=2 else x['Paid Expense  Reserves Expense1'],axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Recovery Total2']) + float(x['Total Net Incurred2'])) if ('$' in str(x['Total Net Incurred']) and '$' in str(x['Recovery Total'])) else "",axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Recovery Total2']) + float(x['Total Net Incurred2'])) if ('$' in str(x['Total Net Incurred']) and '$' in str(x['Recovery Total']) and "(" in str(x['Recovery Total']) ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Recovery Total2'])) if (str(x['Recovery Total']).count(".")>=2 and '$' not in str(x['Total Net Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Claim Recovery Total2']) + float(x['Recovery Total2'])) if ("$" in str(x['Claim Recovery Total']) and "$" in str(x['Recovery Total']) and "$" not in  str(x['Total Net Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Claim Recovery Total2']) + float(x['Recovery Total2'])) if ("$" in str(x['Claim Recovery Total']) and "$" in str(x['Recovery Total']) and "$" not in  str(x['Total Net Incurred']) and "(" in str(x['Claim Recovery Total'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Claim Recovery Total2'])) if (str(x['Claim Recovery Total']).count(".")>=2 and '$' not in  str(x['Recovery Total']) and '$' not in str(x['Total Net Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Paid Total  Reserves Total2']) + float(x['Claim Recovery Total2'])) if ('$' in str(x['Claim Recovery Total']) and '$' not in  str(x['Total Net Incurred']) and '$' not in  str(x['Recovery Total']) and '$' in str(x['Paid Total  Reserves Total'])) else x['TotalPaid'] ,axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Paid Total  Reserves Total2']) + float(x['Claim Recovery Total2'])) if ('$' in str(x['Claim Recovery Total']) and '$' not in  str(x['Total Net Incurred']) and '$' not in  str(x['Recovery Total']) and '$' in str(x['Paid Total  Reserves Total']) and "(" in str(x['Paid Total  Reserves Total'])) else x['TotalPaid'] ,axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Paid Expense  Reserves Expense2']) + float(x['Paid Total  Reserves Total2'])) if ('$' not in  str(x['Claim Recovery Total']) and '$' not in  str(x['Recovery Total']) and '$' not in str(x['Total Net Incurred']) and '$' in str(x['Paid Expense  Reserves Expense']) and '$' in str(x['Paid Total  Reserves Total'])) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Paid Expense  Reserves Expense2']) + float(x['Paid Total  Reserves Total2'])) if ('$' not in  str(x['Claim Recovery Total']) and '$' not in  str(x['Recovery Total']) and '$' not in str(x['Total Net Incurred']) and '$' in str(x['Paid Expense  Reserves Expense']) and '$' in str(x['Paid Total  Reserves Total']) and "(" in str(x['Paid Expense  Reserves Expense'])) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Paid Total  Reserves Total2'])) if ('$' not in  str(x['Claim Recovery Total']) and '$' not in  str(x['Recovery Total']) and '$' not in str(x['Total Net Incurred']) and (str(x['Paid Total  Reserves Total']).count(".")>=2)) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Paid Expense  Reserves Expense2'])) if ('$' not in  str(x['Claim Recovery Total']) and '$' not in  str(x['Recovery Total']) and '$' not in str(x['Total Net Incurred']) and (str(x['Paid Expense  Reserves Expense']).count(".")>=2)) else x['TotalPaid'],axis=1 )
        #     popular_table['TotalPaid'] = popular_table.apply(lambda x:x['State'] if (('$' not in  x['Total Net Incurred']) and ('$' not in  x['Recovery Total']) and ('$' not in  x['Reserves']) and ('$' in  x['State'])) else x['TotalGrossIncurred'],axis=1 )
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:"$" + str(float(x.split("$")[-1])) if x.count("$")>=2 else x)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:x.split()[1] if len(x.split())>=2 else x)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Claim Recovery Total'] if "$" in str(x['Total Net Incurred'])  else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Paid Total  Reserves Total'] if ("$" not in str(x['Total Net Incurred']) and "$" in str(x['Recovery Total'])) else x['TotalGrossIncurred'],axis=1 )
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Paid Expense  Reserves Expense'] if  ('$' not in str(x['Recovery Total']) and ("$" not in str(x['TotalGrossIncurred'])))  else x['TotalGrossIncurred'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:"$" + str((x.split("$")[-1].replace(",","").replace("(","").replace(")","").replace("-","")) +
                                                                                                                 (x.split("$")[-2].replace(",","").replace("(","").replace(")","").replace("-",""))) if (str(x).count(".")>=2 and len(str(x).split())<=3) else x)
        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:"$" + str(float(x.replace("$","").replace(",","").replace("(","").replace(")","").replace("-",""))) if ("" not in str(x) and len(str(x).split())<=3) else x)
        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:str(x).split()[1] if len(str(x).split())>=2 else x)
        popular_table['TotalGrossIncurred'] =  popular_table['TotalGrossIncurred'].apply(lambda x: x if "$" in str(x) else "")
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Claim Recovery Total'] if "$" not in str(x['Total Net Incurred']) and re.search(r'^[$|\d]',x['Claim Recovery Total'])  else "",axis=1)


        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date_fetch_14(x ,'Insured Name') if (str(x['Insured Name']).count("/")>=2) else "" ,axis=1)
    #     print(popular_table['LossDate'].values)
        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date(x,'Claim Number') if ('/' not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Client Name'])[:10] if ('/' not in str(x['LossDate']) and '/' in str(x['Client Name']) and x['Client Name'].count("/")>=2) else x['LossDate'] ,axis =1 )

        popular_table['LossDate'] = popular_table.apply(lambda x: x['Date of Loss'] if (('/' in str(x['Date of Loss'])) and ('/' not in str(x['LossDate'])) ) else x['LossDate'],axis=1 )
        popular_table['LossDate1'] = popular_table['Claim Status  Closed Date'].str.extract('(\d{8})', expand=True)
        popular_table['LossDate']  = popular_table.apply(lambda x:x['LossDate1'] if str(x['LossDate']) =="" else x['LossDate'] ,axis=1)
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

        popular_table['ClosedDate1'] = popular_table['Paid BI   Reserves BI'].apply(lambda x:x if (str(x).count("/")>=2) else "0/0")
        popular_table['ClosedDate2'] = popular_table['Paid PD   Reserves PD'].apply(lambda x:x if (str(x).count("/")>=2) else "1/1")
    #     print(popular_table['ClosedDate1'].values)
    #     print(popular_table['ClosedDate2'].values)

        popular_table['ClosedDate'] = popular_table.apply(lambda x: x['ClosedDate1'] if  str(x['ClosedDate1']).count("/")>=2 else "" if x['ClosedDate2'].count("/")==1 else x['ClosedDate2'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Claim Number') if ("/" not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Insured Name']).split("Closed")[1][:11] if( ( len(str(x['Insured Name']).split()) >= 3 ) and ('/' not in str(x['ClosedDate'])) and ('Closed' in str(x['Insured Name']))) else x['ClosedDate'] ,axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Insured Name']).split("Open")[1][:11] if( ( len(str(x['Insured Name']).split()) >= 3 ) and ('/' not in str(x['ClosedDate'])) and ('Open' in str(x['Insured Name']))) else x['ClosedDate'] ,axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Client Name']).split("Closed")[1][:11] if( ( len(str(x['Client Name']).split()) >= 3 ) and ('/' not in str(x['ClosedDate'])) and ('Closed' in str(x['Client Name']))) else x['ClosedDate'] ,axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['Client Name']).split("Open")[1][:11] if( ( len(str(x['Client Name']).split()) >= 3 ) and ('/' not in str(x['ClosedDate'])) and ('Open' in str(x['Client Name']))) else x['ClosedDate'] ,axis=1)
    #     popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Client Name'] if (x['Client Name'].count("/")>=2 and "/" not in x['ClosedDate']) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

    #     popular_table['ClosedDate'] = popular_table.apply(lambda x: x['ClosedDate1'] if  x['ClosedDate1'].count("/")>=2 else "" if x['ClosedDate2'].count("/")==1 else x['ClosedDate2'],axis=1)
        #     popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'].split("Open")[1][:11] if( ( len(x['Client Name'].split()) >= 3 ) and ('/' not in x['ClosedDate']) and ('Open' in str(x['Client Name']))  )else x['ClosedDate'] ,axis=1)
        popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Date of Loss'][:10] if ('/' in str(x['Date of Loss'])) else "",axis=1 )
        popular_table['NoticeDate2'] = popular_table.apply(lambda x:x['Loss State'] if (('/' in str(x['Loss State'])) and (str(x['Loss State']).count("/")>=2)) else "",axis=1)
    #     print(popular_table['ClosedDate'].values)
    #     print(popular_table['NoticeDate'])
        popular_table['Loss State Date'] = popular_table.apply(lambda x:x['Loss State'] if (('/' in str(x['Loss State'])) and (str(x['Loss State']).count("/")>=2)) else "",axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['NoticeDate2'] if ("/" not in str(x['NoticeDate1']) or str(x['ClosedDate']) == None) else x['NoticeDate1'],axis=1)
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)
        #     print(popular_table['NoticeDate'].values)
    #     try:
    #     popular_table['ClosedDate3'] = popular_table.apply(lambda x:x['Loss State Date'] if ((x['NoticeDate']!= x['Loss State Date'])) else x['ClosedDate'],axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Loss State'])) or ('Close' in str(x['Loss State'])) or ('CLOSE' in str(x['Loss State']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Loss State'])) or ('Open' in str(x['Loss State'])) or ('OPEN' in str(x['Loss State']))) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Claim Number'])) or ('Close' in str(x['Claim Number'])) or ('CLOSE' in str(x['Loss State']))) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Claim Number'])) or ('Open' in str(x['Claim Number'])) or ('OPEN' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Client Name'])) or ('Close' in str(x['Client Name']))or ('CLOSE' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Client Name'])) or ('Open' in str(x['Client Name'])) or ('OPEN' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Insured Name'])) or ('Close' in str(x['Insured Name'])) or ('CLOSE' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Insured Name'])) or ('Open' in str(x['Insured Name'])) or ('OPEN' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Claim Status  Closed Date'])) or ('Close' in str(x['Claim Status  Closed Date']))or ('CLOSE' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Claim Status  Closed Date'])) or ('Open' in str(x['Claim Status  Closed Date'])) or ('OPEN' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Claim Status  Closed Date'])) or ('Close' in str(x['Claim Status  Closed Date']))or ('CLOSE' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Claim Status  Closed Date'])) or ('Open' in str(x['Claim Status  Closed Date']))  or ('OPEN' in str(x['Loss State']))) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Paid BI   Reserves BI'])) or ('Close' in str(x['Paid BI   Reserves BI'])) or ('CLOSE' in str(x['Loss State']))) else str(x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Paid BI   Reserves BI'])) or ('Open' in str(x['Paid BI   Reserves BI'])) or ('OPEN' in str(x['Loss State'])) ) else str(x['ClaimStatus']),axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Client Name'])),axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Insured Name'])) if x['LineOfBusiness'] not in lob_lst else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Date of Loss'])) if x['LineOfBusiness'] not in lob_lst else x['LineOfBusiness'],axis=1)

    #     popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClaimStatus'] !="") | ( popular_table['CarrierClaimNumber'] !=0) )]
        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClosedDate'].str.contains("/"))
                                       | (popular_table['NoticeDate'].str.contains("/")) | (popular_table['ClaimStatus']!=""))]

        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber3','LossDate1','NoticeDate1','NoticeDate2','ClosedDate1','ClosedDate2'],inplace=True, axis=1)
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['TotalPaid1'] = popular_table['TotalPaid'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:"$" + x['TotalPaid1'] if ((x['TotalGrossIncurred1'] and x['TotalPaid1'] and float(x['TotalPaid1'])>=float(x['TotalGrossIncurred1']))
                                            or (x['TotalPaid1'] and not x['TotalGrossIncurred1'] )) else ("$" + x['TotalGrossIncurred1'] if x['TotalGrossIncurred1'] else ''),axis=1)

        popular_table = popular_table.drop(['TotalPaid1','TotalGrossIncurred1'],axis=1)
        return popular_table

    def loss_date_fetch(self,df,clm):
        for ab in str(df[clm]).split():
                if '/' in ab:
                    try:
                        return ab[:10]
                    except:np.nan
                else:
                    continue

        return ""

    def Zur_Post_Processing_11(self,popular_table):
        popular_table['CarrierName'] ="Zurich"
        popular_table['No Losses']  = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('no claim').any()  else "", axis=1)
    #     try:
        popular_table['CarrierClaimNumber']  = popular_table['Claim Number'].str.extract('(\d{10})', expand=True)

    #     popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'].ffill(axis=0,inplace=True)


        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['Loss'] if (('Total' in str(x['Date of Claimant'])) and ('$' in str(x['Loss']))) else "",axis=1).shift(-4)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['Closed Date'] if '$' in str(x['Closed Date']) else x['TotalPaid'], axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total Paid'] if '$' in str(x['Total Paid']) else "",axis=1 )

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Reported Date'] if (('Total' in str(x['Date of Claimant'])) and ('$' in str(x['Reported Date'])) and "$" not in x['TotalGrossIncurred']) else x['TotalGrossIncurred'],axis=1)

        popular_table['LossDescription'] = popular_table.apply(lambda x: x['Site Code'] if  "Acc"  in str(x['Claim Number'])  else "" ,axis=1).shift(-2)

        popular_table['LossState'] = popular_table['Loss']


        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss Type'][:10] if (('/' in str(x['Loss Type'])) and (str(x['Loss Type']).count('/')==2)) else "" ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss'][:10] if (('/' in str(x['Loss'])) and (str(x['Loss']).count('/')==2) and "/" not in x['LossDate']) else x['LossDate'] ,axis =1 )
        popular_table['LossDate1'] = popular_table.apply(lambda x:str(x['Date of Claimant'])[:10] if (('/' not in str(x['LossDate'])) and (~str(x['Claim Number']).isdigit()) and ('/' in str(x['Date of Claimant'])) and (str(x['Date of Claimant']).count('/')==2)) else x['LossDate'] ,axis =1 ).shift(-1)
        popular_table['LossDate']  = popular_table['LossDate'].combine_first(popular_table['LossDate1'])
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['Site Code']) if (('/' not in str(x['LossDate'])) and (str(x['Site Code']).count('/')==2) and ( "/" in str(x['Site Code'])) ) else x['LossDate'] ,axis =1 )


        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date(x,'Site Code') if ('/' not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")==2 and str(x).replace("/","").replace(" ","").isdigit()) else None)


        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Closed Date'] if (("/" in str(x['Closed Date'])) and (str(x['Closed Date']).count('/')==2)) else "",axis=1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Reported Date'] if (("/" in str(x['Reported Date'])) and (str(x['Reported Date']).count('/')==2) and "/" not in x['ClosedDate']) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Site Code'] if (("/" in str(x['Site Code'])) and (str(x['Site Code']).count('/')==2) and "/" not in x['ClosedDate']) else x['ClosedDate'],axis=1 )
        popular_table['ClosedDate1'] = popular_table.apply(lambda x:x['Site Code'] if (("/" in str(x['Site Code'])) and (str(x['Site Code']).count('/')==2)) else "",axis=1 ).shift(-4)
        popular_table['ClosedDate']  = popular_table['ClosedDate'].combine_first(popular_table['ClosedDate1'])
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Site Code') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")==2 and str(x).replace("/","").replace(" ","").isdigit()) else None)


        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status'] if ('/' in str(x['Status']) and str(x['Status']).count("/")>=2) else "",axis=1)
        popular_table['NoticeDate1'] = popular_table.apply(lambda x:str(x['Site Code']) if (("/" not in str(x['NoticeDate'])) and ("/" in str(x['Site Code'])) and (str(x['Site Code']).count('/')==2)) else x['NoticeDate'],axis=1 ).shift(-2)
        popular_table['NoticeDate']  = popular_table['NoticeDate'].combine_first(popular_table['NoticeDate1'])
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Reported Date'] if (("/" in str(x['Reported Date'])) and (str(x['Reported Date']).count('/')==2 and "/" not in x['NoticeDate'])) else x['NoticeDate'],axis=1)
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")==2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Loss State'])) or ('Close' in str(x['Loss State'])) or ('CLOSE' in str(x['Loss State']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Loss State'])) or ('Open' in str(x['Loss State'])) or ('OPEN' in str(x['Loss State']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Date of Claimant'])) or ('Close' in str(x['Date of Claimant'])) or ('CLOSE' in str(x['Date of Claimant']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Date of Claimant'])) or ('Open' in str(x['Date of Claimant'])) or ('OPEN' in str(x['Date of Claimant'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Status'])) or ('Close' in str(x['Status'])) or ('CLOSE' in str(x['Status']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Status'])) or ('Open' in str(x['Status'])) or ('OPEN' in str(x['Status'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Loss Type'])) or ('Close' in str(x['Loss Type'])) or ('CLOSE' in str(x['Loss Type']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Loss Type'])) or ('Open' in str(x['Loss Type'])) or ('OPEN' in str(x['Loss Type'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Site Code'])) or ('Close' in str(x['Site Code'])) or ('CLOSE' in str(x['Site Code'])) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Site Code'])) or ('Open' in str(x['Site Code'])) or ('OPEN' in str(x['Site Code'])) ) else x['ClaimStatus'],axis=1)
    #     popular_table['ClaimStatus'].ffill(axis = 0,inplace=True)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Loss Type'])) if x['Loss Type'] else '',axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Date of Claimant'])) if (x['Date of Claimant'] and not x['LineOfBusiness']) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Loss State'])) if (x['Loss State'] and not x['LineOfBusiness']) else x['LineOfBusiness'],axis=1)


    #     popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClaimStatus'] !="") | ( popular_table['CarrierClaimNumber'] !=0) )]
        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClosedDate'].str.contains("/")) | (popular_table['NoticeDate'].str.contains("/")) | (popular_table['ClaimStatus']!=""))]
        popular_table.drop(['NoticeDate1','ClosedDate1','LossDate1'],inplace=True, axis=1)
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['TotalPaid1'] = popular_table['TotalPaid'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:"$" + x['TotalPaid1'] if ((x['TotalGrossIncurred1'] and x['TotalPaid1'] and float(x['TotalPaid1'])>=float(x['TotalGrossIncurred1']))
                                            or (x['TotalPaid1'] and not x['TotalGrossIncurred1'] )) else ("$" + x['TotalGrossIncurred1'] if x['TotalGrossIncurred1'] else ''),axis=1)

        popular_table = popular_table.drop(['TotalPaid1','TotalGrossIncurred1'],axis=1)
        return popular_table

    def get_lob(self,x):
        string_con_1=''
        string_con_2=''
        for ind in x.index:
            if ind>0:
                if pdf_prev!=x['SubmissionID'][ind] or claim_prev!=x['CarrierClaimNumber'][ind] or page_prev!=x['Page#'][ind]:
                    string_con_1=''
                    string_con_2=''
            if (x['Claimant'][ind] and not re.search(r'\d{2}/\d{2}/\d{4}',str(x['Date of Status'][ind]))):
                string_con_1=string_con_1+' '+str(x['Claimant'][ind])
            if re.search(r'\d{2}/\d{2}/\d{4}',str(x['Date of Status'][ind])):
                string_con_1=string_con_1+' '+str(x['Claimant'][ind])
                x['Lob_1'][ind]=string_con_1
                string_con_1=''

            if (x['Claim Number'][ind] and not re.search(r'\d{2}/\d{2}/\d{4}',str(x['Claimant'][ind])) and not re.search(r'\d{10}',str(x['Claim Number'][ind]))):
                string_con_2=string_con_2+' '+str(x['Claim Number'][ind])
            if re.search(r'\d{2}/\d{2}/\d{4}',str(x['Claimant'][ind])):
                string_con_2=string_con_2+' '+str(x['Claim Number'][ind])
                x['Lob_2'][ind]=string_con_2
                string_con_2=''
            pdf_prev=x['SubmissionID'][ind]
            claim_prev=x['CarrierClaimNumber'][ind]
            page_prev=x['Page#'][ind]
        return x


    def loss_date1(self,df,clm):
        if 'Closed' in str(df[clm]) :
            return df[clm].split("Closed")[0][-11:]

        elif 'Open'in str(df[clm]):
            return df[clm].split("Open")[0][-11:]


        else:return ""

    def Zur_Post_Processing_13(self,popular_table):
        popular_table['CarrierName'] ="Zurich"
        popular_table['No Losses']  = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('no claim').any()  else "", axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['Claim Number'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Claimant'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber2'] = popular_table['State'].str.extract('(\d{10})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])
    #     popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'].ffill(axis=0,inplace = True)

        popular_table['Incurred1'] = popular_table.apply(lambda x: str(x['Incurred']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Incurred']) else "",axis=1)
        popular_table['Recoveries1'] = popular_table.apply(lambda x: str(x['Recoveries']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Recoveries']) else "",axis=1)
        popular_table['Reserves1'] = popular_table.apply(lambda x: str(x['Reserves']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Reserves']) else "",axis=1)
        popular_table['State1'] = popular_table.apply(lambda x: str(x['State']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['State']) else "",axis=1)
        popular_table['Date1'] = popular_table.apply(lambda x: str(x['Date']).replace("$"," ").replace(",","").replace("(","").replace(")","") if "$" in str(x['Date']) else "",axis=1)

        popular_table['Incurred2'] = popular_table.apply(lambda x: float(x['Incurred1'].split()[-2]) + float(x['Incurred1'].split()[-1]) if str(x['Incurred1']).count(".")>=2 else x['Incurred1'],axis=1)
        popular_table['Recoveries2'] = popular_table.apply(lambda x: float(x['Recoveries1'].split()[-2]) + float(x['Recoveries1'].split()[-1]) if str(x['Recoveries1']).count(".")>=2 else x['Recoveries1'],axis=1)
        popular_table['Reserves2'] = popular_table.apply(lambda x: float(x['Reserves1'].split()[-2]) + float(x['Reserves1'].split()[-1]) if str(x['Reserves1']).count(".")>=2 else x['Reserves1'],axis=1)
        popular_table['State2'] = popular_table.apply(lambda x: float(x['State1'].split()[-2]) + float(x['State1'].split()[-1]) if str(x['State1']).count(".")>=2 else x['State1'],axis=1)
        popular_table['Date2'] = popular_table.apply(lambda x: float(x['Date1'].split()[-2]) + float(x['Date1'].split()[-1]) if str(x['Date1']).count(".")>=2 else x['Date1'],axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Recoveries2']) + float(x['Incurred2'])) if ('$' in  str(x['Incurred']) and '$' in str(x['Recoveries']) ) else "",axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Recoveries2']) + float(x['Incurred2'])) if ('$' in  str(x['Incurred']) and '$' in str(x['Recoveries']) and "(" in str(x['Recoveries']) ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Recoveries2'])) if (str(x['Recoveries']).count(".")>=2 and '$' not in str(x['Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Reserves2']) + float(x['Recoveries2'])) if ("$" in str(x['Reserves']) and "$" in str(x['Recoveries']) and "$" not in  str(x['Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Reserves2']) + float(x['Recoveries2'])) if ("$" in str(x['Reserves'])  and "(" in str(x['Reserves']) and "$" in str(x['Recoveries']) and "$" not in  str(x['Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Reserves2'])) if (str(x['Reserves']).count(".")>=2 and '$' not in  str(x['Recoveries']) and '$' not in str(x['Incurred'])) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['State2']) + float(x['Reserves2'])) if ('$' in str(x['Reserves']) and '$' not in  str(x['Incurred']) and '$' not in  str(x['Recoveries']) and '$' in str(x['State'])) else x['TotalPaid'] ,axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['State2']) + float(x['Reserves2'])) if ('$' in str(x['Reserves']) and '$' not in  str(x['Incurred']) and '$' not in  str(x['Recoveries']) and '$' in str(x['State']) and "(" in str(x['State'])) else x['TotalPaid'] ,axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(-float(x['Date2']) + float(x['State2'])) if ('$' not in  str(x['Reserves']) and '$' not in  str(x['Recoveries']) and '$' not in str(x['Incurred']) and '$' in str(x['Date']) and '$' in str(x['State'])) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Date2']) + float(x['State2'])) if ('$' not in  str(x['Reserves']) and '$' not in  str(x['Recoveries']) and '$' not in str(x['Incurred']) and '$' in str(x['Date']) and "(" in str(x['Date'])) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['State2'])) if ('$' not in  str(x['Reserves']) and '$' not in  str(x['Recoveries']) and '$' not in str(x['Incurred']) and (str(x['State']).count(".")>=2)) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:"$" + str(float(x['Date2'])) if ('$' not in  str(x['Reserves']) and '$' not in  str(x['Recoveries']) and '$' not in str(x['Incurred']) and (str(x['Date']).count(".")>=2)) else x['TotalPaid'],axis=1 )
    #     popular_table['TotalPaid'] = popular_table.apply(lambda x:x['State'] if (('$' not in  x['Incurred']) and ('$' not in  x['Recoveries']) and ('$' not in  x['Reserves']) and ('$' in  x['State'])) else x['TotalGrossIncurred'],axis=1 )
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:"$" + str(float(x.split("$")[-1])) if x.count("$")>=2 else x)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:x.split()[1] if len(x.split())>=2 else x)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Reserves'] if '$' in  str(x['Incurred']) else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['State'] if ('$' not in str(x['Incurred']) and '$' in str(x['Recoveries']) and ('$' in str(x['State']))) else x['TotalGrossIncurred'] ,axis=1 )
    #     popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Incurred'] if (('$' not in  x['Reserves']) and ('$' not in  x['State'])) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Date'] if (('$' not in str(x['Incurred'])) and ('$' not in str(x['Recoveries'])) and ('$' in  str(x['Date']))) else x['TotalGrossIncurred'],axis=1 )
        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:"$" + str(float(x.split("$")[-1].replace(",","").replace("(","").replace(")","").replace("-","")) +
                                                                                                                 float(x.split("$")[-2].replace(",","").replace("(","").replace(")","").replace("-",""))) if x.count(".")>=2 else x)
        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:"$" + str(float(x.replace("$","").replace(",","").replace("(","").replace(")","").replace("-",""))) if "" not in x else x)
        popular_table['TotalGrossIncurred'] = popular_table['TotalGrossIncurred'].apply(lambda x:x.split()[1] if len(x.split())>=2 else x )

        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date1(x,'Claimant') if ('/'  in str(x['Claimant']) and x['Claimant'].count('/')>=2) else "",axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date_fetch(x,'Claimant') if (('/'  in str(x['Claimant']) and x['Claimant'].count('/')>=2) and ('/' not in str(x['LossDate']))) else x['LossDate'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date1(x,'Date of Status') if ('/' not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Date of Status'][:10] if (('/' not in str(x['LossDate'])) and ('/' in str(x['Date of Status'])) and (str(x['Date of Status']).count('/')>=2)) else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date1(x,'Date Rptd Site') if ('/' not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Date Rptd Site'][:10] if (('/' not in str(x['LossDate'])) and ('/' not in str(x['Date of Status'])) and ('/' in str(x['Date Rptd Site'])) and (str(x['Date Rptd Site']).count('/')>=2)) else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:self.loss_date1(x,'Date') if ('/' not in str(x['LossDate'])) else x['LossDate'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Closed Paid'][:10] if (('/' in str(x['Closed Paid'])) and (str(x['Closed Paid']).count('/')==2) and ('/' not in str(x['LossDate']))) else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)


        popular_table['LossState'] = popular_table.apply(lambda x:x['Date'] if str(x['Date']) == str(x['Date']).upper() else "" ,axis=1)


        popular_table['ClosedDate1'] = popular_table.apply(lambda x:x['to ZA'] if ("/" in str(x['to ZA']) and (str(x['to ZA']).count('/')==2)) else " ",axis=1 )
        popular_table['ClosedDate2'] = popular_table.apply(lambda x:x['Loss Loss'] if ("/" in str(x['Loss Loss']) and ("/" not in str(x['ClosedDate1'])) and (str(x['Loss Loss']).count('/')==2)) else " ",axis=1)
    #     popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Loss Loss'] if ('/' in x['Loss Loss'] and x['Loss Loss'].count('/')>=2 and "/" not in x['ClosedDate2']) else " " ,axis=1)
    #     popular_table['NoticeDate2'] = popular_table.apply(lambda x:x['Closed Paid'][:10] if ('/' in x['Closed Paid'] and x['Closed Paid'].count('/')>=2 and "/" not in str(x['NoticeDate1'])) else x['NoticeDate1'] ,axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['to ZA'] if ("/" in str(x['to ZA']) and (str(x['to ZA']).count('/')==2)) else " ",axis=1 )
    #     popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Loss Loss'] if ("/" in x['Loss Loss'] and ("/" not in str(x['ClosedDate'])) and (str(x['Loss Loss']).count('/')==2)) else x['ClosedDate'],axis=1)
    #     popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date1(x,'Date of Status') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Date of Status') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Date Rptd Site') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Claimant') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:self.close_date(x,'Date') if ('/' not in str(x['ClosedDate'])) else x['ClosedDate'],axis=1)
    #     popular_table['ClosedDate7'] = popular_table.apply(lambda x:x['Closed Paid'][:10] if (('/' in x['Closed Paid']) and (str(x['Closed Paid']).count('/')==2) and ('/' not in str(x['ClosedDate6']))) else x['ClosedDate6'] ,axis =1 )
    #     popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2) else None)

    #     popular_table['NoticeDate'] = popular_table.apply(lambda x:x['to ZA'] if ("/" in x['to ZA'] and (str(x['to ZA']).count('/')==2)) else " ",axis=1 )
        popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Date of Status'][:10] if ('/' in str(x['Date of Status']) and x['Date of Status'].count('/')>=2 and "/" in x['Claimant']) else "" ,axis=1)
        popular_table['NoticeDate2'] = popular_table.apply(lambda x:x['Loss Loss'] if (str(x['Loss Loss']).count('/')>=2 and "/" not in str(x['NoticeDate1'])) else "",axis=1)
    #     popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Date of Status'][:10] if ('/' in x['Date of Status'] and x['Date of Status'].count('/')>=2 and "/" not in x['NoticeDate']) else x['NoticeDate'] ,axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['NoticeDate2'] if (("/" not in str(x['NoticeDate1']) and "/" not in str(x['ClosedDate'])) and ("/" not in x['NoticeDate1'] or "/" not in str(x['ClosedDate']))) else x['NoticeDate1'],axis=1)
    #     print(popular_table['NoticeDate1'].values)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Closed Paid'][:10] if ('/' in str(x['Closed Paid']) and str(x['Closed Paid']).count('/')>=2 and (x['NoticeDate']=="00/00/0000") ) else x['NoticeDate'] ,axis=1)
        #     popular_table['NoticeDate'] = popular_table['Loss Loss'].apply(lambda x:x if '/' in x else np.nan)
    #     print(popular_table['NoticeDate'].values)
        popular_table['Loss State Date'] = popular_table.apply(lambda x:x['Loss Loss'] if (str(x['Loss Loss']).count('/')>=2 and str(x['Loss Loss']).count("/")>=2) else "" ,axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Loss Loss'] if (str(x['Loss Loss']).count("/")>=2 and "/" not in str(x['ClosedDate']) and (str(x['NoticeDate'])!= str(x['Loss State Date']))) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State Date'] if (str(x['ClosedDate'])!= str(x['Loss State Date']) and str(x['NoticeDate'])!= str(x['Loss State Date'])) else x['NoticeDate'],axis=1)
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x:x if ("/" in str(x) and str(x).count("/")>=2 and str(x).replace("/","").replace(" ","").isdigit()) else None)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Closed Paid'] if ("/" in str(x['Closed Paid']) and str(x['Closed Paid']).count("/")>=2 and str(x['Closed Paid']).replace("/","").replace(" ","").isdigit()) else x['NoticeDate'],axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Date of Status'])) or ('Close' in str(x['Date of Status'])) or ('CLOSE' in str(x['Date of Status']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Date of Status'])) or ('Open' in str(x['Date of Status'])) or ('OPEN' in str(x['Date of Status']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Date Rptd Site'])) or ('Close' in str(x['Date Rptd Site'])) or ('CLOSE' in str(x['Date Rptd Site']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Date Rptd Site'])) or ('Open' in str(x['Date Rptd Site'])) or ('OPEN' in str(x['Date Rptd Site']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Claimant'])) or ('Close' in str(x['Claimant'])) or ('CLOSE' in str(x['Claimant']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Claimant'])) or ('Open' in str(x['Claimant'])) or ('OPEN' in str(x['Claimant']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Closed Paid'])) or ('Close' in str(x['Closed Paid'])) or ('CLOSE' in str(x['Closed Paid']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Closed Paid'])) or ('Open' in str(x['Closed Paid'])) or ('OPEN' in str(x['Closed Paid']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['Date'])) or ('Close' in str(x['Date'])) or ('CLOSE' in str(x['Date']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['Date'])) or ('Open' in str(x['Date'])) or ('OPEN' in str(x['Date'])) ) else x['ClaimStatus'],axis=1)

        popular_table['Lob_1']=''
        popular_table['Lob_2']=''
        popular_table=self.get_lob(popular_table)


        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClosedDate'].str.contains("/"))
                                       | (popular_table['NoticeDate'].str.contains("/")) | (popular_table['ClaimStatus']!=""))]
        popular_table = popular_table[((popular_table['Date Rptd Site']!='') | (popular_table['Closed Paid']!='') |
                                       (popular_table['Loss Loss']!='')|(popular_table['State']!='')|(popular_table['Date']!=''))]

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Lob_1'])),axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Lob_2'])) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(str(x['Date Rptd Site'])) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)


    #     popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | (popular_table['ClosedDate'].str.contains("/")) | (popular_table['NoticeDate'].str.contains("/")) | (popular_table['ClaimStatus'] !=""))]
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','NoticeDate1','NoticeDate2','ClosedDate1','ClosedDate2'],inplace=True, axis=1)
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x: x if ("/" in str(x) and str(x).count("/")>=2) and str(x).replace("/","").replace(" ","").isdigit() else "")
        popular_table['TotalPaid1'] = popular_table['TotalPaid'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].apply(lambda x:x.replace("$","").replace("(","").replace(")","").replace(",","").replace(" ","") if x else '')

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:"$" + x['TotalPaid1'] if ((x['TotalGrossIncurred1'] and x['TotalPaid1'] and float(x['TotalPaid1'])>=float(x['TotalGrossIncurred1']))
                                            or (x['TotalPaid1'] and not x['TotalGrossIncurred1'] )) else ("$" + x['TotalGrossIncurred1'] if x['TotalGrossIncurred1'] else ''),axis=1)

        popular_table = popular_table.drop(['TotalPaid1','TotalGrossIncurred1'],axis=1)
        return popular_table

    ############# combined code for getting popular table for schema of any length  ##############

    ############# combined code for getting popular table for schema of any length  ##############

    def get_popular_table(self,new_table):

        schemas_len=[11,13,14,16]
        schemas=[
                ['Claim Number', 'Site Code', 'Date of Claimant', 'Loss Type', 'Loss',
                                    'Loss State', 'Status', 'Reported Date', 'Closed Date', 'Total Paid','SubmissionID','Page#','Order'],
                 ['Claim Number', 'Claimant', 'Date of Status', 'Date Rptd Site','Closed Paid', 'Loss Loss', 'to ZA',
                                    'Date', 'State', 'Reserves','Recoveries', 'Incurred', 'SubmissionID','Page#','Order'],
                 ['Claim Number', 'Insured Name', 'Client Name', 'Date of Loss', 'Claim Status  Closed Date','Loss State', 'Paid BI   Reserves BI', 'Paid PD   Reserves PD',
                                    'Paid Expense  Reserves Expense', 'Paid Total  Reserves Total','Claim Recovery Total','Recovery Total', 'Total Net Incurred', 'SubmissionID','Page#','Order'],
                 ['claim number', 'Date of Claimant', 'Loss_Date', 'Loss_State', 'Claim_Status', 'Indemnity Status', 'Indemnity Date',
                                    'Ind Date', 'BI/Med', 'Exp PD/LT','Reserve', 'Incurred Exp Paid', 'Total_Paid', 'Total_Gross_Incurred', 'Total', 'SubmissionID','Page#','Order']

                ]
        sch={}
        df={}
        k=0
        for i in schemas_len:
            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):
            l=len(new_table[a].columns)
            if l-2 in schemas_len:
                new_table[a].columns = sch[l-2]
                sch['popular_table_'+str(l-2)].append(new_table[a])

        for k,v in df.items():
            k_sch_len=''.join(re.findall(r'\d',k))
            try:
                df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)
            except:
                continue

        return df

    def Post_Processing(self,df):
        post_pro=[self.Zur_Post_Processing_11,self.Zur_Post_Processing_13,self.Zur_Post_Processing_14,self.Zur_Post_Processing_16]
        post_pro_fun=['Zur_Post_Processing_11','Zur_Post_Processing_13','Zur_Post_Processing_14','Zur_Post_Processing_16']

        for idx ,pp in enumerate(post_pro):
            sch_len=str(post_pro_fun[idx])[-2:]
            if len(df['df_'+sch_len])>0:
                try:
                    df['df_'+sch_len]=pp(df['df_'+sch_len])
                except Exception as e:
                    df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
                    continue

        return df
