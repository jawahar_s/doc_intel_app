import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class AIG():

    def __init__(self):
        self.ls = []
        self.replace_dict = {}
        self.lob_lst=['auto liability','unins motorist','auto property damage','auto prop dam','smp bodily injury liab covrg',
             'inland marine','workers compensation','liability','REGULAR PROD PD','MISCELLANEOUS CASUALTY','Crime','Auto',
         'Directors and Officers','Errors and Omissions','Employment Practices Liability','Fiduciary','Fin. Fidelity',
         'General Liability','Property','Kidnap and Ransom','Financial Lines',
         'Fire and Allied','Commercial Umbrella','Auto Physical Damage','Automobile','Business Auto','Commercial Package',
         'Property and Casualty','Property Damage','Public Liability','Commercial Package Policy',
         'Special Multi-Peril','Premises Operations']
        print("", end="")


    def replace_text(self,text) :
       _text = text
       for k,v in self.replace_dict.items():
           _text = _text.replace(k,v)
       return _text

    def lcs_sim(self,s1_text, s2_text , fuzzy_thresh=70):
       s1_text = s1_text
       s2_text = s2_text
       fuzzy_thresh = fuzzy_thresh
       s1_text , s2_text = self.replace_text(s1_text) , self.replace_text(s2_text)
       s1 = s1_text.split(' ')#nltk.word_tokenize(s1_text)
       s2 = s2_text.split(' ')
       matrix = [["" for x in range(len(s2))] for x in range(len(s1))]
       for i in range(len(s1)):
           for j in range(len(s2)):
               if fuzz.ratio(s1[i] , s2[j]) >= 70 :
                   if i == 0 or j == 0:
                       matrix[i][j] = s1[i]# + '|' + str(i)
                   else:
                       matrix[i][j] = matrix[i-1][j-1] + ' ' + s1[i]# + '|' + str(i)
               else:
                   matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1],
                                      key=lambda x: fuzz.ratio(x , s2[j]) + len(x.split()))

       matrix = [x for x in matrix if x and x[-1] in s1_text]

       cs = matrix[-1][-1]
       if fuzz.ratio(cs,s2_text) > 0:
           return cs,fuzz.ratio(cs,s2_text)#,len(cs.split(' ')), cs
       else:
           return ''#,0

    def match_keys(self,text,key):
       text = text
       key = key
       text = text.lower()
       key = key.lower()
       return self.lcs_sim(text,key)

    def is_lob(self,string):
        string = string
        lob_lst = self.lob_lst
        percent_match={}
        for _val in lob_lst:
            self.ls = self.match_keys(str(string),str(_val))
            try:
                if self.ls[1]>=80:
                      percent_match[_val]=self.ls[1]
                else:
                    continue
            except Exception as e:

                continue
        if not percent_match: return ''
        else:
            return sorted(percent_match.items(), key =lambda kv:(kv[1]),reverse=True)[0][0]

    def get_popular_table(self,new_table):
        schemas_len=[8,9,10,11,12,13,14]
        schemas=[['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
               'Adjuster Name', 'Loss Description',
               'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
               'Adjuster Name', 'Loss Description', 'USD',
               'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
               'Adjuster Name', 'Loss Description', 'USD','Loss Paid',
               'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
               'Adjuster Name', 'Loss Description', 'USD','Loss Paid','total' ,
               'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD',"Loss Paid","total","total1",
            'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD',"Loss Paid","total","total1","total2",
            'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD',"Loss Paid","total","total1","total2",'total3',
            'SubmissionID', 'Page#', 'Order']
                ]
        sch={}
        df={}
        k=0
        for i in schemas_len:

            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):

            l=len(new_table[a].columns)

            if (l-2) in schemas_len:

                new_table[a].columns = sch[l-2]
                sch['popular_table_'+str(l-2)].append(new_table[a])

        for k,v in df.items():
            k_sch_len=''.join(re.findall(r'\d',k))

            try:
                df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)
            except:
                continue

        return df

    def shift_cell_value(self,x,sch):
        # print('shift_cell_value',x,sch)
        x= x
        sch = sch
        leng=len(x)
        k,j=0,0
        tot_cols=['L.O.B','loss date','Adjuster Name','Loss Description','USD','Loss Paid','total','total1','total2','total3']
        cols=[tot_cols[c] for c in range(sch)]
        check=[el for el in cols if x[el]]
        while(k<leng):
            if not x[cols[k]] and j<len(check):
                x[cols[k]]=x[check[j]]
                x[check[j]]=''
                j+=1
            elif x[cols[k]]: j+=1
            k+=1
        return x

    def post_TGI_cal(self,x,sch):
        # print('post_TGI_cal',x,sch)
        x= x
        sch = sch
        tzz=pd.DataFrame()
        tot_cols=['L.O.B','loss date','Adjuster Name','Loss Description','USD','Loss Paid','total','total1','total2','total3']
        cols=[tot_cols[c] for c in range(sch)]
        tzz=x[cols]
        tzz=tzz.apply(lambda x : self.shift_cell_value(x,len(cols)),axis=1)
        x[cols]=tzz
        #display(tzz)
        return x



    def AIG_Post_Processing_14(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)

        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber3']  = popular_table['Status Closed Date'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber3'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])[0] if (re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])
                                                                                                                                              and (re.findall(r'\d',x['Loss Paid']) or re.findall(r'\d',x['USD'])) ) else x['CarrierClaimNumber'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['Adjuster Name'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)

        popular_table['C_Status'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else '',axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['C_Status'],axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['C_Status'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['C_Status'] if (x['C_Status']) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3)  else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and (str(x['Status Closed Date']).count('/')==2)) else x['NoticeDate'] ,axis =1 )

        popular_table['SCD']=popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Status Closed Date']).count('/')==2) and (len(re.findall(r'(\d{2}/\d{2}/\d{4})',x['Loss State']))==0))  else "",axis =1 )
        popular_table['SCD'] = popular_table['SCD'].shift(1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['LossDate'] if (x['LossDate']) else(x['SCD'] if x['SCD'] else '') ,axis =1 )

        popular_table['ClosedDate']=popular_table['L.O.B'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if (re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date']) and ('/' not in x['ClosedDate'])
                                                                                                                                            and (x['ClaimStatus'] not in {'Open','N/O'}) ) else x['ClosedDate'] ,axis=1)
        popular_table["lob2"]=popular_table.apply(lambda x: x['L.O.B'] if (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdate2"]=popular_table.apply(lambda x: x['loss date'] if (re.search(r'\d{0,8}[.]\d{2}',x['loss date']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["adjustername2"]=popular_table.apply(lambda x: x['Adjuster Name'] if (re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdescription2"]=popular_table.apply(lambda x: x['Loss Description'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Description']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["usd2"]=popular_table.apply(lambda x: x['USD'] if (re.search(r'\d{0,8}[.]\d{2}',x['USD']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["losspaid2"]=popular_table.apply(lambda x: x['Loss Paid'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Paid']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total_2"]=popular_table.apply(lambda x: x['total'] if (re.search(r'\d{0,8}[.]\d{2}',x['total']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total1_2"]=popular_table.apply(lambda x: x['total1'] if (re.search(r'\d{0,8}[.]\d{2}',x['total1']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total2_2"]=popular_table.apply(lambda x: x['total2'] if (re.search(r'\d{0,8}[.]\d{2}',x['total2']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total3_2"]=popular_table.apply(lambda x: x['total3'] if (re.search(r'\d{0,8}[.]\d{2}',x['total3']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)

        popular_table["L.O.B"]=popular_table.apply(lambda x: str(x['lob2']) if (x['lob2']) else x['L.O.B'],axis=1)
        popular_table["loss date"]=popular_table.apply(lambda x: str(x['lossdate2']) if (x['lossdate2']) else x['loss date'],axis=1)
        popular_table["Adjuster Name"]=popular_table.apply(lambda x: str(x['adjustername2']) if (x['adjustername2']) else x['Adjuster Name'],axis=1)
        popular_table["Loss Description"]=popular_table.apply(lambda x: str(x['lossdescription2']) if (x['lossdescription2']) else x['Loss Description'],axis=1)
        popular_table["USD"]=popular_table.apply(lambda x: str(x['usd2']) if (x['usd2']) else x['USD'],axis=1)
        popular_table["Loss Paid"]=popular_table.apply(lambda x: str(x['losspaid2']) if (x['losspaid2']) else x['Loss Paid'],axis=1)
        popular_table["total"]=popular_table.apply(lambda x: str(x['total_2']) if (x['total_2']) else x['total'],axis=1)
        popular_table["total1"]=popular_table.apply(lambda x: str(x['total1_2']) if (x['total1_2']) else x['total1'],axis=1)
        popular_table["total2"]=popular_table.apply(lambda x: str(x['total2_2']) if (x['total2_2']) else x['total2'],axis=1)
        popular_table["total3"]=popular_table.apply(lambda x: str(x['total3_2']) if (x['total3_2']) else x['total3'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table['total3']
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total2'] if ((x['TotalGrossIncurred'] =="") and (x['total2'] !="") and ("." in x['total2']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total1'] if ((x['TotalGrossIncurred'] =="") and (x['total1'] !="") and ("." in x['total1']) ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table=self.post_TGI_cal(popular_table,10)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['Loss Paid'])  else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',',''))+float(x['USD'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['total']) and '$' not in x['loss date'] and '$' not in x['total']) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',','')))
                                                                                 if ((re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['USD']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['USD'] )
                                                                                     or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['L.O.B'] and '$' not in x['Loss Description'] )
                                                                                    or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Adjuster Name']) and not re.search(r'([.]\d{2})',x['Loss Description']) and '$' not in x['L.O.B'] and '$' not in x['Adjuster Name'] ) ) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if ( (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Paid']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['total']) and '$' not in x['Loss Paid'] and '$' not in x['loss date'] )
                                                                                     or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['USD'])  and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['USD'] and '$' not in x['loss date'] )
                                                                                    or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['loss date'] and '$' not in x['Loss Description'] ) ) else x['TotalGrossIncurred'],axis=1)

    #     popular_table['lob_losstype']=popular_table.apply(lambda x: self.is_lob(x['Loss State']),axis=1).shift(2)
    #     popular_table['L.O.B']=popular_table.apply(lambda x: x['L.O.B']+' '+x['lob_losstype'] if (x['lob_losstype'] and re.search('^[\w\d]{3}\s*[-]\s*$',x['L.O.B']) ) else x['L.O.B'],axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else x['LineOfBusiness']),axis=1)

        popular_table['lob3'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1).shift(2)
        popular_table['lob4'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else ''),axis=1).shift(2)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob3']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob4']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)


        popular_table = popular_table[( (popular_table['ClaimStatus'] !="") | (popular_table['No Losses'] !="") | (popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber2','CarrierPolicyNumber3'],inplace=True, axis=1)

        popular_table["is_duplicate"]= popular_table.duplicated(subset=['CarrierClaimNumber'],keep='first')
        popular_table["is_duplicate"]=popular_table.apply(lambda x:False if x['CarrierClaimNumber']==0 else x["is_duplicate"] ,axis=1)

        popular_table=popular_table[(popular_table["is_duplicate"] ==False)]
        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]


        return popular_table


    def AIG_Post_Processing_9(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)

        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber3']  = popular_table['Status Closed Date'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber3'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['L.O.B'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3)  else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and ("/" in x['Status Closed Date'])) else x['NoticeDate'] ,axis =1 )


        popular_table['ClosedDate']=popular_table['L.O.B'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:x['Status Closed Date'] if((str(x['Status Closed Date']).count('/')==2) and ('/' not in x['ClosedDate'])) else x['ClosedDate'] ,axis=1)

        popular_table['TotalGrossIncurred']=popular_table.apply(lambda x:x['Adjuster Name'] if(x['Adjuster Name']) else x['Loss Description'] ,axis=1)

    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['L.O.B']),axis=1)
    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['Status Closed Date']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
    #     popular_table['lob3'] = popular_table.apply(lambda x: x['L.O.B'],axis=1).shift(2)
    #     popular_table['lob4'] = popular_table.apply(lambda x: x['Status Closed Date'],axis=1).shift(2)
    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob3']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob4']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[( (popular_table['ClaimStatus'] !="") | (popular_table['No Losses'] !="") | (popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber2','CarrierPolicyNumber3'],inplace=True, axis=1)

        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]
        return popular_table

    def AIG_Post_Processing_11(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)

        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber3']  = popular_table['Status Closed Date'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber3'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)# x['Loss State'] in {'Closed','Open','N/O'}
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])[0] if (re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])
                                                                                                                                                and (re.findall(r'\d',x['Loss Paid']) or re.findall(r'\d',x['USD'])) ) else x['CarrierClaimNumber'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['L.O.B'] ) ) else x['ClaimStatus'],axis=1)

        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else '',axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['C_Status'],axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['C_Status'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['C_Status'] if (x['C_Status']) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3)  else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and (str(x['Status Closed Date']).count('/')==2)) else x['NoticeDate'] ,axis =1 )

        popular_table['SCD']=popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Status Closed Date']).count('/')==2) and (len(re.findall(r'(\d{2}/\d{2}/\d{4})',x['Loss State']))==0))  else "",axis =1 )
        popular_table['SCD'] = popular_table['SCD'].shift(1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['LossDate'] if (x['LossDate']) else(x['SCD'] if x['SCD'] else '') ,axis =1 )

        popular_table['ClosedDate']=popular_table['L.O.B'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if (re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date']) and ('/' not in x['ClosedDate'])
                                                                                                                                           and (x['ClaimStatus'] not in {'Open','N/O'}) ) else x['ClosedDate'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table['total']
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Loss Paid'] if ((x['TotalGrossIncurred'] =="") and (x['Loss Paid'] !="") and ("." in x['Loss Paid']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['USD'] if ((x['TotalGrossIncurred'] =="") and (x['USD'] !="") and ("." in x['USD']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                           if((x['Loss Paid']) and (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['loss date']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Loss Description'])) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Status Closed Date'].replace(',','')))
                                                                           if((x['Loss Paid']) and (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['loss date']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Status Closed Date'])) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['USD'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                           if((x['Loss Paid']) and not (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']))
                                                                                        and not (re.search(r'\d{0,8}[.]\d{2}',x['loss date']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']))
                                                                                        and(re.search(r'\d{0,8}[.]\d{2}',x['Status Closed Date'])) ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['TotalGrossIncurred1'] = popular_table.apply(lambda x:x['USD'] if ((x['TotalGrossIncurred'] =="") and (x['USD'] !="") and ("." in x['USD']) ) else x['TotalGrossIncurred'] ,axis=1).shift(-1)
        popular_table['TotalGrossIncurred']  = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if ((x['TotalGrossIncurred'] =="") and (x['TotalGrossIncurred1'] !="") ) else x['TotalGrossIncurred'] ,axis=1)

    #     popular_table['lob_losstype']=popular_table.apply(lambda x: self.is_lob(x['Loss State']),axis=1).shift(2)
    #     popular_table['L.O.B']=popular_table.apply(lambda x: x['L.O.B']+' '+x['lob_losstype'] if (x['lob_losstype'] and re.search('^[\w\d]{3}\s*[-]\s*$',x['L.O.B']) ) else x['L.O.B'],axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else x['LineOfBusiness']),axis=1)

        popular_table['lob3'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1).shift(2)
        popular_table['lob4'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else ''),axis=1).shift(2)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob3']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob4']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[( (popular_table['ClaimStatus'] !="") |(popular_table['No Losses'] !="") | (popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber2','CarrierPolicyNumber3'],inplace=True, axis=1)

        popular_table["is_duplicate"]= popular_table.duplicated(subset=['CarrierClaimNumber'],keep='first')
        popular_table["is_duplicate"]=popular_table.apply(lambda x:False if x['CarrierClaimNumber']==0 else x["is_duplicate"] ,axis=1)
        popular_table=popular_table[(popular_table["is_duplicate"] ==False)]

        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]
        return popular_table


    def AIG_Post_Processing_13(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)
        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber3']  = popular_table['Status Closed Date'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber3'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])[0] if (re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])
                                                                                                                                              and (re.findall(r'\d',x['Loss Paid']) or re.findall(r'\d',x['USD'])) ) else x['CarrierClaimNumber'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['Adjuster Name'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)

        popular_table['C_Status'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else '',axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['C_Status'],axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['C_Status'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['C_Status'] if (x['C_Status']) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3)  else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and (str(x['Status Closed Date']).count('/')==2)) else x['NoticeDate'] ,axis =1 )

        popular_table['SCD']=popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Status Closed Date']).count('/')==2) and (len(re.findall(r'(\d{2}/\d{2}/\d{4})',x['Loss State']))==0))  else "",axis =1 )
        popular_table['SCD'] = popular_table['SCD'].shift(1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['LossDate'] if (x['LossDate']) else(x['SCD'] if x['SCD'] else '') ,axis =1 )

        popular_table['ClosedDate']=popular_table['L.O.B'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if (re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date']) and ('/' not in x['ClosedDate'])
                                                                                                                                            and (x['ClaimStatus'] not in {'Open','N/O'}) ) else x['ClosedDate'] ,axis=1)

        popular_table["lob2"]=popular_table.apply(lambda x: x['L.O.B'] if (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdate2"]=popular_table.apply(lambda x: x['loss date'] if (re.search(r'\d{0,8}[.]\d{2}',x['loss date']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["adjustername2"]=popular_table.apply(lambda x: x['Adjuster Name'] if (re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdescription2"]=popular_table.apply(lambda x: x['Loss Description'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Description']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["usd2"]=popular_table.apply(lambda x: x['USD'] if (re.search(r'\d{0,8}[.]\d{2}',x['USD']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["losspaid2"]=popular_table.apply(lambda x: x['Loss Paid'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Paid']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total_2"]=popular_table.apply(lambda x: x['total'] if (re.search(r'\d{0,8}[.]\d{2}',x['total']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total1_2"]=popular_table.apply(lambda x: x['total1'] if (re.search(r'\d{0,8}[.]\d{2}',x['total1']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total2_2"]=popular_table.apply(lambda x: x['total2'] if (re.search(r'\d{0,8}[.]\d{2}',x['total2']) and (re.search(r'\S+@\S+', x['Status Closed Date']) or re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)


        popular_table["L.O.B"]=popular_table.apply(lambda x: str(x['lob2']) if (x['lob2']) else x['L.O.B'],axis=1)
        popular_table["loss date"]=popular_table.apply(lambda x: str(x['lossdate2']) if (x['lossdate2']) else x['loss date'],axis=1)
        popular_table["Adjuster Name"]=popular_table.apply(lambda x: str(x['adjustername2']) if (x['adjustername2']) else x['Adjuster Name'],axis=1)
        popular_table["Loss Description"]=popular_table.apply(lambda x: str(x['lossdescription2']) if (x['lossdescription2']) else x['Loss Description'],axis=1)
        popular_table["USD"]=popular_table.apply(lambda x: str(x['usd2'])if (x['usd2']) else x['USD'],axis=1)
        popular_table["Loss Paid"]=popular_table.apply(lambda x: str(x['losspaid2']).replace(',','') if (x['losspaid2']) else x['Loss Paid'],axis=1)
        popular_table["total"]=popular_table.apply(lambda x: str(x['total_2']) if (x['total_2']) else x['total'],axis=1)
        popular_table["total1"]=popular_table.apply(lambda x: str(x['total1_2']) if (x['total1_2']) else x['total1'],axis=1)
        popular_table["total2"]=popular_table.apply(lambda x: str(x['total2_2']) if (x['total2_2']) else x['total2'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table['total2']
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total1'] if ((x['TotalGrossIncurred'] =="") and (x['total1'] !="") and ("." in x['total1']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total'] if ((x['TotalGrossIncurred'] =="") and (x['total'] !="") and ("." in x['total']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table=self.post_TGI_cal(popular_table,9)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['Loss Paid'])  else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',',''))+float(x['USD'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['total']) and '$' not in x['loss date'] and '$' not in x['total']) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',','')))
                                                                                 if ((re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['USD']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['USD'] )
                                                                                     or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['L.O.B'] and '$' not in x['Loss Description'] )
                                                                                    or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Adjuster Name']) and not re.search(r'([.]\d{2})',x['Loss Description']) and '$' not in x['L.O.B'] and '$' not in x['Adjuster Name'] ) ) else x['TotalGrossIncurred'],axis=1)
        #display(popular_table)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if ( (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Paid']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['total']) and '$' not in x['Loss Paid'] and '$' not in x['loss date'] )
                                                                                     or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['USD'])  and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['USD'] and '$' not in x['loss date'] )
                                                                                    or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['loss date'] and '$' not in x['Loss Description'] ) ) else x['TotalGrossIncurred'],axis=1)
        #display(popular_table)
    #     popular_table['lob_losstype']=popular_table.apply(lambda x: self.is_lob(x['Loss State']),axis=1).shift(2)
    #     popular_table['L.O.B']=popular_table.apply(lambda x: x['L.O.B']+' '+x['lob_losstype'] if (x['lob_losstype'] and re.search('^[\w\d]{3}\s*[-]\s*$',x['L.O.B']) ) else x['L.O.B'],axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else x['LineOfBusiness']),axis=1)

        popular_table['lob3'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1).shift(2)
        popular_table['lob4'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else ''),axis=1).shift(2)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob3']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob4']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[( (popular_table['ClaimStatus'] !="") | (popular_table['No Losses'] !="") | (popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber2','CarrierPolicyNumber3'],inplace=True, axis=1)

        popular_table["is_duplicate"]= popular_table.duplicated(subset=['CarrierClaimNumber'],keep='first')
        popular_table["is_duplicate"]=popular_table.apply(lambda x:False if x['CarrierClaimNumber']==0 else x["is_duplicate"] ,axis=1)

        popular_table=popular_table[(popular_table["is_duplicate"] ==False)]
        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]
        return popular_table

    def AIG_Post_Processing_12(self,popular_table):
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)
        popular_table['CarrierPolicyNumber']  = popular_table['Loss Description'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber1'] = popular_table['claim number'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber1'])
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber']  = popular_table.apply(lambda x:x['claim number'].split(':')[1] if("Policy:" in x['claim number']) else x['CarrierPolicyNumber'] ,axis=1)
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\d{3}-\w{10})',x['claim number'])[0] if re.findall(r'(\d{3}-\w{10})',x['claim number']) else x['CarrierClaimNumber'],axis=1)
        popular_table['CarrierClaimNumber'] = popular_table['CarrierClaimNumber'].shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number'])[0] if (re.findall(r'(\d{3}-\d{6}-\d{3})',x['claim number']) and (re.findall(r'\d',x['Loss Paid']) or re.findall(r'\d',x['USD'])) ) else x['CarrierClaimNumber'],axis=1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster Name']) or ('Close' in x['Adjuster Name']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster Name']) or ('Open' in x['Adjuster Name']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Adjuster Name']) or ('N/o' in x['Adjuster Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['Adjuster Name'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)

        popular_table['C_Status'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else '',axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['C_Status'],axis=1)
        popular_table['C_Status'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['C_Status'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['C_Status'] if (x['C_Status']) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and (str(x['Status Closed Date']).count('/')==2)) else x['NoticeDate'] ,axis =1 )

        popular_table['SCD']=popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Status Closed Date']).count('/')==2) and (len(re.findall(r'(\d{2}/\d{2}/\d{4})',x['Loss State']))==0))  else "",axis =1 )
        popular_table['SCD'] = popular_table['SCD'].shift(1)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['LossDate'] if (x['LossDate']) else(x['SCD'] if x['SCD'] else '') ,axis =1 )

        popular_table['ClosedDate']=popular_table['Status Closed Date'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if (re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date']) and ('/' not in x['ClosedDate']) and (x['ClaimStatus'] not in {'Open','N/O'}) ) else x['ClosedDate'] ,axis=1)

        popular_table["lob2"]=popular_table.apply(lambda x: x['L.O.B'] if (re.search(r'\d{0,8}[.]\d{2}',x['L.O.B']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdate2"]=popular_table.apply(lambda x: x['loss date'] if (re.search(r'\d{0,8}[.]\d{2}',x['loss date']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["adjustername2"]=popular_table.apply(lambda x: x['Adjuster Name'] if (re.search(r'\d{0,8}[.]\d{2}',x['Adjuster Name']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["lossdescription2"]=popular_table.apply(lambda x: x['Loss Description'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Description']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["usd2"]=popular_table.apply(lambda x: x['USD'] if (re.search(r'\d{0,8}[.]\d{2}',x['USD']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["losspaid2"]=popular_table.apply(lambda x: x['Loss Paid'] if (re.search(r'\d{0,8}[.]\d{2}',x['Loss Paid']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total2"]=popular_table.apply(lambda x: x['total'] if (re.search(r'\d{0,8}[.]\d{2}',x['total']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)
        popular_table["total12"]=popular_table.apply(lambda x: x['total1'] if (re.search(r'\d{0,8}[.]\d{2}',x['total1']) and (re.search(r'\S+@\S+', x['L.O.B']) or re.search(r'\S+@\S+', x['Loss State']))) else '',axis=1).shift(-1)

        popular_table["L.O.B"]=popular_table.apply(lambda x: str(x['lob2']) if (x['lob2']) else x['L.O.B'],axis=1)
        popular_table["loss date"]=popular_table.apply(lambda x: str(x['lossdate2']) if (x['lossdate2']) else x['loss date'],axis=1)
        popular_table["Adjuster Name"]=popular_table.apply(lambda x: str(x['adjustername2']) if (x['adjustername2']) else x['Adjuster Name'],axis=1)
        popular_table["Loss Description"]=popular_table.apply(lambda x: str(x['lossdescription2']) if (x['lossdescription2']) else x['Loss Description'],axis=1)
        popular_table["USD"]=popular_table.apply(lambda x: str(x['usd2'])if (x['usd2']) else x['USD'],axis=1)
        popular_table["Loss Paid"]=popular_table.apply(lambda x: str(x['losspaid2']).replace(',','') if (x['losspaid2']) else x['Loss Paid'],axis=1)
        popular_table["total"]=popular_table.apply(lambda x: str(x['total2']) if (x['total2']) else x['total'],axis=1)
        popular_table["total1"]=popular_table.apply(lambda x: str(x['total12']) if (x['total12']) else x['total1'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table['total1']
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total'] if ((x['TotalGrossIncurred'] =="") and (x['total'] !="") and ("." in x['total']) ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Loss Paid'] if ((x['TotalGrossIncurred'] =="") and (x['Loss Paid'] !="") and ("." in x['Loss Paid']) ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table=self.post_TGI_cal(popular_table,8)
        # print('AIG_Post_Processing_12---1',popular_table)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['Loss Paid'])  else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',',''))+float(x['USD'].replace(',','')))
                                                                                 if (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['total']) and '$' not in x['loss date'] and '$' not in x['total']) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['L.O.B'].replace(',',''))+float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',','')))
                                                                                 if ((re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['USD']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['L.O.B'] and '$' not in x['USD'] )
                                                                                     or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['L.O.B'] and '$' not in x['Loss Description'] )
                                                                                    or (re.search(r'([.]\d{2})',x['L.O.B']) and re.search(r'([.]\d{2})',x['Adjuster Name']) and not re.search(r'([.]\d{2})',x['Loss Description']) and '$' not in x['L.O.B'] and '$' not in x['Adjuster Name'] ) ) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(x['loss date'].replace(',',''))+float(x['Adjuster Name'].replace(',',''))+float(x['Loss Description'].replace(',','')))
                                                                                 if ( (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Paid']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['total']) and '$' not in x['Loss Paid'] and '$' not in x['loss date'] )
                                                                                     or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['USD'])  and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['Loss Paid']) and '$' not in x['USD'] and '$' not in x['loss date'] )
                                                                                    or (re.search(r'([.]\d{2})',x['loss date']) and re.search(r'([.]\d{2})',x['Loss Description']) and not re.search(r'([.]\d{2})',x['L.O.B']) and not re.search(r'([.]\d{2})',x['USD']) and '$' not in x['loss date'] and '$' not in x['Loss Description'] ) ) else x['TotalGrossIncurred'],axis=1)

        popular_table['TotalGrossIncurred1'] = popular_table.apply(lambda x:x['USD'] if ((x['TotalGrossIncurred'] =="") and (x['total1'] =="") and  (x['total'] =="") and  (x['USD'] !="") and ("." in x['USD']) ) else "" ,axis=1).shift(-1)
        popular_table['TotalGrossIncurred']  = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if ((x['TotalGrossIncurred'] =="") and (x['TotalGrossIncurred1'] !="") ) else x['TotalGrossIncurred'] ,axis=1)

    #     popular_table['lob_losstype']=popular_table.apply(lambda x: self.is_lob(x['Loss State']),axis=1).shift(2)
    #     popular_table['L.O.B']=popular_table.apply(lambda x: x['L.O.B']+' '+x['lob_losstype'] if (x['lob_losstype'] and re.search('^[\w\d]{3}\s*[-]\s*$',x['L.O.B']) ) else x['L.O.B'],axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip())
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (self.is_lob(re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip())
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else x['LineOfBusiness']),axis=1)

        popular_table['lob3'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['L.O.B']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['L.O.B'])) else ''),axis=1).shift(2)
        popular_table['lob4'] = popular_table.apply(lambda x:re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date'])).group(1).strip()
                                                              if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)\s*[(]',str(x['Status Closed Date']))
                                                              else (re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])).group(1).strip()
                                                                    if re.search(r'\d{3}\s*[-]\s*([\w+\s]+)',str(x['Status Closed Date'])) else ''),axis=1).shift(2)

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob3']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:self.is_lob(x['lob4']) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[( (popular_table['ClaimStatus'] !="") | (popular_table['No Losses'] !="") | (popular_table['CarrierPolicyNumber'] !=0)  |(popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber1','CarrierPolicyNumber2'],inplace=True, axis=1)

        popular_table["is_duplicate"]= popular_table.duplicated(subset=['CarrierClaimNumber'],keep='first')
        popular_table["is_duplicate"]=popular_table.apply(lambda x:False if x['CarrierClaimNumber']==0 else x["is_duplicate"] ,axis=1)

        popular_table=popular_table[(popular_table["is_duplicate"] ==False)]

        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]
        # print("schema 12 ",popular_table)
        return popular_table

    def AIG_Post_Processing_10(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)

        popular_table['CarrierPolicyNumber']  = popular_table['Loss Description'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber1']  = popular_table['claim number'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber1'])
        popular_table['CarrierPolicyNumber2']  = popular_table['Loss State'].str.extract(r'(\d{10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else "" ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster Name']) or ('Close' in x['Adjuster Name']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster Name']) or ('Open' in x['Adjuster Name']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Adjuster Name']) or ('N/o' in x['Adjuster Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['Adjuster Name'] or x['L.O.B'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)


        popular_table['NoticeDate']=popular_table['Loss State'].apply(lambda x:x  if (str(x).count('/')==2) else "")
        popular_table['NoticeDate']=popular_table.apply(lambda x: re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if ((str(x['Status Closed Date']).count('/')==2) and ((x['ClaimStatus']) in {'N/o','N/O','open','Open'}) and (x['NoticeDate']=='')) else x['NoticeDate'],axis=1)
        popular_table['NoticeDate']=popular_table.apply(lambda x: re.findall(r'(\d{2}/\d{2}/\d{4})',x['Status Closed Date'])[0] if ((str(x['Status Closed Date']).count('/')==2) and (str(x['L.O.B']).count('/')==2) and ('Closed' in (x['ClaimStatus'])) and (x['NoticeDate']=='')) else x['NoticeDate'],axis=1)

        popular_table['ClosedDate']=popular_table['Status Closed Date'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:'' if ((x['ClaimStatus']) in {'N/O','Open'}) else x['ClosedDate'],axis=1)
        popular_table['ClosedDate']=popular_table.apply(lambda x:re.findall(r'(\d{2}/\d{2}/\d{4})',x['L.O.B'])[0] if ((str(x['L.O.B']).count('/')==2) and ('Closed' in (x['ClaimStatus'])) and x['ClosedDate']) else x['ClosedDate'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Loss Paid'] if (x['Loss Paid']) else (x["USD"] if (x["USD"]) else x['Loss Description']),axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:'' if (str(x['TotalGrossIncurred']).count('/')>=1) else x['TotalGrossIncurred'],axis=1)

        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) & (popular_table['LossDate'] !="")| (popular_table['No Losses'] !="")  |(popular_table['CarrierClaimNumber'] !=0)  )]
        #popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !=""))]
        return popular_table

    def AIG_Post_Processing_8(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="AIG"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if row.astype(str).str.contains('No Claims').any()  else "", axis=1)



        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber2'] = popular_table['Loss State'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber2'])
        popular_table['CarrierPolicyNumber3']  = popular_table['Status Closed Date'].str.extract(r'([A-Z]{0,2}\d{8,10}-\d{3}-\d{3})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber3'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract(r'(\d{3}-\d{6}-\d{3})', expand=True).shift(1)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'] if (str(x['claim number']).count('/')==2) else( x['claim number'].split()[0] if (str(x['claim number']).count('/')>=3) else "") ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Status Closed Date']) or ('N/o' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['L.O.B']) or ('N/o' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"N/O" if (('N/O' in x['Loss State']) or ('N/o' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"ReOpen" if("ReOpen" in (x['Status Closed Date']or x['Loss State']or x['L.O.B'] ) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(1)


        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Loss State'] if (str(x['Loss State']).count('/')==2) else( x['claim number'].split()[-1] if (str(x['claim number']).count('/')>=3)  else "") ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in  x['NoticeDate']) and ('/' not in x['Loss State']) and ("/" in x['Status Closed Date'])) else x['NoticeDate'] ,axis =1 )


        popular_table['ClosedDate']=popular_table['L.O.B'].apply(lambda x:x if (str(x).count('/')==2) else "")
        popular_table['ClosedDate']=popular_table.apply(lambda x:x['Status Closed Date'] if((str(x['Status Closed Date']).count('/')==2) and ('/' not in x['ClosedDate'])) else x['ClosedDate'] ,axis=1)

        popular_table['TotalGrossIncurred']=popular_table.apply(lambda x:x['Adjuster Name'] if(x['Adjuster Name']) else x['Loss Description'] ,axis=1)

        popular_table = popular_table[( (popular_table['ClaimStatus']!="")|(popular_table['No Losses'] !="")|(popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))]
        popular_table.drop(['CarrierPolicyNumber2','CarrierPolicyNumber3'],inplace=True, axis=1)
        popular_table=popular_table[((popular_table['CarrierPolicyNumber'] !=0)  | (popular_table['CarrierClaimNumber'] !=0))|(popular_table['No Losses'] !="")]
    #     popular_table['No Losses'] = popular_table['No Losses'].shift(1)
        return popular_table

    def Post_Processing(self,df):

        post_pro_fun=['AIG_Post_Processing_8','AIG_Post_Processing_9','AIG_Post_Processing_10','AIG_Post_Processing_11','AIG_Post_Processing_12','AIG_Post_Processing_13','AIG_Post_Processing_14']
        post_pro=[self.AIG_Post_Processing_8,self.AIG_Post_Processing_9,self.AIG_Post_Processing_10,self.AIG_Post_Processing_11,self.AIG_Post_Processing_12,self.AIG_Post_Processing_13,self.AIG_Post_Processing_14]

        for idx,pp in enumerate(post_pro):

            sch_len=str(post_pro_fun[idx]).split('Processing_')[1]

            try:
                df['df_'+sch_len]=pp(df['df_'+sch_len])
            except Exception as e:
                print('there was an error with your input in schema of length-{0}  : {1}'.format(sch_len,e))
                df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
                continue

        return df

    def AIG(self, popular_table):
        # print("in AIG",popular_table)
        df=self.get_popular_table(popular_table)
        df=self.Post_Processing(df)
        # print('AIG result',df)
        return df
