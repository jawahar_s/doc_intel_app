import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz
from . import Cincinnati , Liberty, AIG, CNA, Tokio, Travelers, Zurich


def get_carrier(car_lst, Merged_table) :

    map_dict = {
    "AIG" : initialize_object(AIG.AIG),
    "CNA" : initialize_object(CNA.CNA),
    "Cincinnati" : initialize_object(Cincinnati.Cincinnati),
    "Liberty" : initialize_object(Liberty.Liberty),
    "Tokio" : initialize_object(Tokio.Tokio),
    "Travelers" : initialize_object(Travelers.Travelers),
    "Zurich" : initialize_object(Zurich.Zurich),
    "Philadelphia":initialize_object(Tokio.Tokio)
        }
    results = getattr(map_dict[re.match(r'[A-Za-z]*',car_lst).group()], car_lst)(Merged_table)

    try :
        results = getattr(map_dict[re.match(r'[A-Za-z]*',car_lst).group()], car_lst)(Merged_table)

        return results
    except AttributeError :
        print("error,here")
        return []

def initialize_object(class_name) :
    class_obj = class_name()
    return class_obj
