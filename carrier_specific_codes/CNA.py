import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class CNA():

    def __init__(self):
        print("", end="")

    def CNA(self, popular_table):
        df=self.get_popular_table(popular_table)
        df=self.Post_Processing(df)
        return df

    def get_popular_table(self,new_table):

        schemas_len=[12,13,14,15]
        schemas=[["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                    'Loss_Description',"USD","Total","Total1",'SubmissionID', 'Page#', 'Order'],
                 ["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                  'Loss_Description',"USD","Total","Total1","Total2",'SubmissionID', 'Page#', 'Order'],
                 ["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                   'Loss_Description',"USD","Total","Total1","Total2","Total3",'SubmissionID', 'Page#', 'Order'],
                 ["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                   'Loss_Description',"USD","Total","Total1","Total2","Total3","Total4",'SubmissionID', 'Page#', 'Order']
                ]
        sch={}
        df={}
        k=0
        for i in schemas_len:
            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):

            l=len(new_table[a].columns)
            if l-2 in schemas_len:
                new_table[a].columns = sch[l-2]
                sch['popular_table_'+str(l-2)].append(new_table[a])

        for k,v in df.items():
            k_sch_len=''.join(re.findall(r'\d',k))
            try:
                df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)
            except:
                continue
        return df

    def Post_Processing(self,df):
        post_pro_fun=['CNA_Post_Processing_12','CNA_Post_Processing_13','CNA_Post_Processing_14','CNA_Post_Processing_15']
        post_pro=[self.CNA_Post_Processing_12,self.CNA_Post_Processing_13,self.CNA_Post_Processing_14,self.CNA_Post_Processing_15]
        for pp in post_pro:
            sch_len=re.findall(r'([\d]{1,2})\s',str(pp))[0]

            try:
                df['df_'+sch_len]=pp(df['df_'+sch_len])
            except Exception as e:
                print('there was an error with your input in schema of length-{0}  : {1}'.format(sch_len,e))
                df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
                continue

        return df


    def CNA_Post_Processing_12(self,popular_table):
        popular_table['CarrierName'] ="CNA"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('No Claims').any()) or (row.astype(str).str.contains('No losses').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)

        popular_table['CarrierClaimNumber']  = popular_table['Status'].str.extract(r'([A-Z]{1}\d{1,2}[A-Z]{0,1}\d{4,5}|[A-Z]{1,4}\d{4,8}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Closed_Date'].str.extract(r'([A-Z]{1,4}\d{4,8}|[A-Z]{1}\d{1}[A-Z]{1}\d{4,5}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  ((str(x['lossstate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ))  else '' ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Closed_Date']) or ('Close' in x['Closed_Date']) or ('CLOSE' in x['Closed_Date'])) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Closed_Date']) or ('Open' in x['Closed_Date']) or ('OPEN' in x['Closed_Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster_Name']) or ('Close' in x['Adjuster_Name']) or ('CLOSE' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster_Name']) or ('Open' in x['Adjuster_Name']) or ('OPEN' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)


        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[0] if  ((str(x['claim number']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=1 ))  else '' ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if ((x['CarrierClaimNumber1'] != 0 )  and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else "" ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if ((x['ClosedDate'] =="" ) and (x['CarrierClaimNumber1'] ==0 ) and (str(x['lossdate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ))  else x['ClosedDate'] ,axis =1 )


        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['Total'].replace('$',"").replace('.',"").replace(',',"") if ((x['Total1'] != "" )  ) else ( x['USD'].replace('$',"").replace('.',"").replace(',',"") if ((x['Total1'] == "" ) and (str(x['USD'])!='N/A' ) ) else 0 ),axis=1)#.astype(float)

        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:0 if x=="" else x)
        popular_table['Total_Expenses'] = popular_table.apply(lambda x:x['Total1'].replace('$',"").replace('.',"").replace(',',"") if ( (x['Total1'] != "" ) ) else ( x['Total'].replace('$',"").replace('.',"").replace(',',"") if ( (x['Total1'] == "" ) and (str(x['Total']).replace('$',"").replace('.',"").replace(',',"").isdigit())) else 0 ),axis=1)#.astype(float)
        popular_table['Total_Expenses'] = popular_table['Total_Expenses'].apply(lambda x:0 if x=="" else x)
        popular_table = popular_table[ ((popular_table['CarrierClaimNumber'] !=0)|(popular_table['No Losses'] !="")) ]

        popular_table['TotalGrossIncurred'] =popular_table.apply(lambda x: int(x['TotalPaid']) + int(x['Total_Expenses']), axis=1)

        return popular_table

    def CNA_Post_Processing_15(self,popular_table):
        popular_table['CarrierName'] ="CNA"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('No Claims').any()) or (row.astype(str).str.contains('No losses').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['Status'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Closed_Date'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['claim number'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber2'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  ((str(x['lossstate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ))  else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if  ((x['CarrierClaimNumber2'] != 0 ) and (x['LossDate'] =="") and (str(x['lossdate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ))  else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if  ((x['CarrierClaimNumber2'] != 0 ) and (x['LossDate'] =="") and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Closed_Date']) or ('Close' in x['Closed_Date']) or ('CLOSE' in x['Closed_Date'])) else np.nan,axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Closed_Date']) or ('Open' in x['Closed_Date']) or ('OPEN' in x['Closed_Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster_Name']) or ('Close' in x['Adjuster_Name']) or ('CLOSE' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster_Name']) or ('Open' in x['Adjuster_Name']) or ('OPEN' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['lossstate']) or ('Close' in x['lossstate']) or ('CLOSE' in x['lossstate']) or ('Clsd' in x['lossstate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['lossstate']) or ('Open' in x['lossstate']) or ('OPEN' in x['lossstate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['lossdate']) or ('Close' in x['lossdate']) or ('CLOSE' in x['lossdate']) or ('Clsd' in x['lossdate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['lossdate']) or ('Open' in x['lossdate']) or ('OPEN' in x['lossdate'])) else x['ClaimStatus'],axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:'O' if('O' in {x['Total'],x['Total1'],x['Total2'],x['Total3']}) else ('C' if ('C'in {x['Total'],x['Total1'],x['Total2'],x['Total3']}) else x['ClaimStatus']),axis=1)
        popular_table['ClaimStatus'].fillna(0,inplace = True)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[0] if  ((str(x['claim number']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=1 ))  else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if  ((x['CarrierClaimNumber2'] != 0 ) and (x['NoticeDate'] =="") and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['NoticeDate'] ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  ((re.search(r'([A-Z]{1}\d{1}[A-Z]{1}\d{7}|[A-Z]{1}\d{9})',x['claim number']))  and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ))  else x['NoticeDate'] ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  ((x['CarrierClaimNumber2'] != 0 )  and (str(x['Closed_Date']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ))  else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if ((x['CarrierClaimNumber1'] != 0 ) and (x['ClosedDate'] =="") and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if ((x['CarrierClaimNumber2'] == 0 ) and (x['ClosedDate'] =="") and (str(x['lossdate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ))  else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:'' if (str(x['ClaimStatus'])=='O' or str(x['ClaimStatus'])=='C') else x['ClosedDate'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total4'] if  str(x['Total4']).replace('$',"").replace('.',"").replace(',',"").isdigit() else "",axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total3'] if ( (x['TotalGrossIncurred']=="")  and  (str(x['Total3']).replace('$',"").replace('.',"").replace(',',"").isdigit() ) )   else (x['Total3']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) and  (str(x['Total3']).replace('$',"").replace('.',"").replace(',',"").isdigit() ) )else x['TotalGrossIncurred']),axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total2'] if ( (x['TotalGrossIncurred']=="")  and (str(x['Total2']).replace('$',"").replace('.',"").replace(',',"").isdigit() )  )   else (x['Total2']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) and (str(x['Total2']).replace('$',"").replace('.',"").replace(',',"").isdigit() ))else x['TotalGrossIncurred']),axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total'] if ( (x['TotalGrossIncurred']=="")  and (str(x['Total']).replace('$',"").replace('.',"").replace(',',"").isdigit()) )   else (x['Total']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) and (str(x['Total']).replace('$',"").replace('.',"").replace(',',"").isdigit()))else x['TotalGrossIncurred']),axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total1'] if ( (x['TotalGrossIncurred']=="")  and (str(x['Total1']).replace('$',"").replace('.',"").replace(',',"").isdigit()) )   else (x['Total1']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) and (str(x['Total1']).replace('$',"").replace('.',"").replace(',',"").isdigit()))else x['TotalGrossIncurred']),axis=1)#.astype(float)


        popular_table['TotalGrossIncurred1']= popular_table.apply(lambda x:x['Total4'] if re.search(r'\d',str(x['Total4']) ) else(x['Total3'] if re.search(r'\d',str(x['Total3']) ) else (x['Total2'] if re.search(r'\d',str(x['Total2']) )
                                                              else(x['Total1'] if re.search(r'\d',str(x['Total1']) ) else(x['Total'] if re.search(r'\d',str(x['Total']) ) else(x['USD'] if re.search(r'\d',str(x['USD']) )
                                                              else(x['Loss_Description'] if re.search(r'\d',str(x['Loss_Description']) ) else (x['Adjuster_Name'] if re.search(r'\d',str(x['Adjuster_Name']) ) else str(x['TotalGrossIncurred']) ))))))),axis=1)

        popular_table['TotalGrossIncurred2']= popular_table.apply(lambda x:x['Total4'] if re.search(r'\d',str(x['Total4']) ) else(x['Total3'] if re.search(r'\d',str(x['Total3']) ) else (x['Total2'] if re.search(r'\d',str(x['Total2']) )
                                                              else(x['Total1'] if re.search(r'\d',str(x['Total1']) ) else(x['Total'] if re.search(r'\d',str(x['Total']) ) else(x['USD'] if re.search(r'\d',str(x['USD']) )
                                                              else(x['Loss_Description'] if re.search(r'\d',str(x['Loss_Description']) ) else (x['Adjuster_Name'] if re.search(r'\d',str(x['Adjuster_Name']) ) else str(x['TotalGrossIncurred']) ))))))),axis=1)

        popular_table['TotalGrossIncurred3']= popular_table.apply(lambda x:x['Total4'] if re.search(r'\d',str(x['Total4']) ) else(x['Total3'] if re.search(r'\d',str(x['Total3']) ) else (x['Total2'] if re.search(r'\d',str(x['Total2']) )
                                                              else(x['Total1'] if re.search(r'\d',str(x['Total1']) ) else(x['Total'] if re.search(r'\d',str(x['Total']) ) else(x['USD'] if re.search(r'\d',str(x['USD']) )
                                                              else(x['Loss_Description'] if re.search(r'\d',str(x['Loss_Description']) ) else (x['Adjuster_Name'] if re.search(r'\d',str(x['Adjuster_Name']) ) else str(x['TotalGrossIncurred']) ))))))),axis=1)
        popular_table['TotalGrossIncurred4']= popular_table.apply(lambda x:x['Total4'] if re.search(r'\d',str(x['Total4']) ) else(x['Total3'] if re.search(r'\d',str(x['Total3']) ) else (x['Total2'] if re.search(r'\d',str(x['Total2']) )
                                                              else(x['Total1'] if re.search(r'\d',str(x['Total1']) ) else(x['Total'] if re.search(r'\d',str(x['Total']) ) else(x['USD'] if re.search(r'\d',str(x['USD']) )
                                                              else(x['Loss_Description'] if re.search(r'\d',str(x['Loss_Description']) ) else (x['Adjuster_Name'] if re.search(r'\d',str(x['Adjuster_Name']) ) else str(x['TotalGrossIncurred']) ))))))),axis=1)



        popular_table['TotalGrossIncurred1']= popular_table.apply(lambda x:'' if (re.search(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})',str(x['claim number'])) or re.search(r'(\d{8})',str(x['Status']))) else x['TotalGrossIncurred1'],axis=1).shift(-1)
        popular_table['TotalGrossIncurred2']= popular_table.apply(lambda x:'' if (re.search(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})',str(x['claim number'])) or re.search(r'(\d{8})',str(x['Status'])))  else x['TotalGrossIncurred2'],axis=1).shift(-2)
        popular_table['TotalGrossIncurred3']= popular_table.apply(lambda x:'' if (re.search(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})',str(x['claim number'])) or re.search(r'(\d{8})',str(x['Status']))) else x['TotalGrossIncurred3'],axis=1).shift(-3)
        popular_table['TotalGrossIncurred4']= popular_table.apply(lambda x:'' if (re.search(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})',str(x['claim number'])) or re.search(r'(\d{8})',str(x['Status']))) else x['TotalGrossIncurred4'],axis=1).shift(-4)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:str(x['TotalGrossIncurred']).replace(',','').replace('$','') if x['TotalGrossIncurred'] else x['TotalGrossIncurred'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: '' if (re.search(r'[a-zA-Z/]+',str(x['TotalGrossIncurred']) )) else x['TotalGrossIncurred'],axis=1)

        popular_table['TotalGrossIncurred1'] = popular_table.apply(lambda x: '' if (re.search(r'[a-zA-Z/]+',str(x['TotalGrossIncurred1']) )) else (str(x['TotalGrossIncurred1']).replace(',','').replace('$','') if (x['TotalGrossIncurred1']  ) else x['TotalGrossIncurred1'] ),axis=1)
        popular_table['TotalGrossIncurred2'] = popular_table.apply(lambda x: '' if (re.search(r'[a-zA-Z/]+',str(x['TotalGrossIncurred2']) )) else (str(x['TotalGrossIncurred2']).replace(',','').replace('$','') if (x['TotalGrossIncurred2']  ) else x['TotalGrossIncurred2']  ),axis=1)
        popular_table['TotalGrossIncurred3'] = popular_table.apply(lambda x: '' if (re.search(r'[a-zA-Z/]+',str(x['TotalGrossIncurred3']) )) else (str(x['TotalGrossIncurred3']).replace(',','').replace('$','') if (x['TotalGrossIncurred3']  ) else x['TotalGrossIncurred3']  ),axis=1)
        popular_table['TotalGrossIncurred4'] = popular_table.apply(lambda x: '' if (re.search(r'[a-zA-Z/]+',str(x['TotalGrossIncurred4']) )) else (str(x['TotalGrossIncurred4']) if x['TotalGrossIncurred4'] else x['TotalGrossIncurred4'] ),axis=1)

        popular_table['TotalGrossIncurred3'] = popular_table.apply(lambda x: max(float(x['TotalGrossIncurred3']),float(x['TotalGrossIncurred'])) if ( x['TotalGrossIncurred']!='' and x['TotalGrossIncurred3']!='') else(x['TotalGrossIncurred3'] if x['TotalGrossIncurred3'] else x['TotalGrossIncurred']),axis=1)
        popular_table['TotalGrossIncurred3'] = popular_table.apply(lambda x: max(float(x['TotalGrossIncurred2']),float(x['TotalGrossIncurred3'])) if ( x['TotalGrossIncurred3']!='' and x['TotalGrossIncurred2']!='') else(x['TotalGrossIncurred3'] if x['TotalGrossIncurred3'] else x['TotalGrossIncurred2']),axis=1)
        popular_table['TotalGrossIncurred3'] = popular_table.apply(lambda x: max(float(x['TotalGrossIncurred1']),float(x['TotalGrossIncurred3'])) if ( x['TotalGrossIncurred3']!='' and x['TotalGrossIncurred1']!='') else(x['TotalGrossIncurred3'] if x['TotalGrossIncurred3'] else x['TotalGrossIncurred1']),axis=1)


        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: x['TotalGrossIncurred3'] if (x['ClaimStatus'] in {'C','O'}) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:(float(str(x['USD']).replace(',','').replace('$','') )+float(str(x['Total']).replace(',','').replace('$','') )) if ( x['USD'] and x['Total'] and  not re.search(r'[a-zA-Z]+',str(x['USD']) )  and not re.search(r'[a-zA-Z]+',str(x['Total']) )  and x['ClaimStatus'] in {'C','O'}) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred4'] if ((x['TotalGrossIncurred4'] )and x['TotalGrossIncurred3'] and (float(str(x['TotalGrossIncurred4']).replace(',','').replace('$',''))>float(x['TotalGrossIncurred3']))  and (str(x['TotalGrossIncurred4'])==str(x['USD']) or str(x['TotalGrossIncurred4'])==str(x['Total']) or str(x['TotalGrossIncurred4'])==str(x['Total1']) or str(x['TotalGrossIncurred4'])==str(x['Total2']) )) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred3'] if(x['TotalGrossIncurred3'] and x['TotalGrossIncurred'] and (float(str(x['TotalGrossIncurred3']).replace(',','').replace('$',''))-float(str(x['TotalGrossIncurred']).replace(',','').replace('$',''))==1 ) ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:'Open' if x['ClaimStatus']=='O' else('Closed' if x['ClaimStatus']  else x['ClaimStatus']),axis=1)

        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2'],inplace=True,axis=1)
        popular_table = popular_table[( (popular_table['LossDate']!='')|(popular_table['NoticeDate']!='')|(popular_table['ClosedDate']!='')|(popular_table['ClaimStatus']!=0) )]
        popular_table = popular_table[ popular_table['CarrierClaimNumber'] !=0]

        popular_table["is_duplicate"]= popular_table.duplicated(subset=['CarrierClaimNumber'],keep='first')
    #     popular_table=popular_table[(popular_table["is_duplicate"] ==False)]

        return popular_table

    def CNA_Post_Processing_14(self,popular_table):
        popular_table['CarrierName'] ="CNA"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('No Claims').any()) or (row.astype(str).str.contains('No losses').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['CarrierClaimNumber']   = popular_table['Status'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber1']  = popular_table['Closed_Date'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber2']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\d{1}[A-Z]{1}\d{5,7}|[A-Z]{1}\d{9}|[A-Z]{1,3}\d{5,7}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  ((str(x['lossstate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ))  else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if  (("/" not in  str(x['LossDate']) ) and  (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  (("/" not in  str(x['LossDate']) ) and  (str(x['Closed_Date']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ))  else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[1] if  (("/" not in  str(x['LossDate']) ) and  (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=2 ))  else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Closed_Date']) or ('Close' in x['Closed_Date']) or ('CLOSE' in x['Closed_Date'])) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Closed_Date']) or ('Open' in x['Closed_Date']) or ('OPEN' in x['Closed_Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster_Name']) or ('Close' in x['Adjuster_Name']) or ('CLOSE' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster_Name']) or ('Open' in x['Adjuster_Name']) or ('OPEN' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['lossdate']) or ('Close' in x['lossdate']) or ('CLOSE' in x['lossdate']) or ('Clsd' in x['lossdate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['lossdate']) or ('Open' in x['lossdate']) or ('OPEN' in x['lossdate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status']) or ('Close' in x['Status']) or ('CLOSE' in x['Status']) or ('Clsd' in x['Status'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status']) or ('Open' in x['Status']) or ('OPEN' in x['Status'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:'O' if('O' in {x['Total'],x['Total1'],x['Total2']}) else ('C' if ('C'in {x['Total'],x['Total1'],x['Total2']}) else x['ClaimStatus']),axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[0] if  ((str(x['claim number']).count('/')>=2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=1 ))  else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if (re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status'])) and not x['NoticeDate']) else x['NoticeDate'] ,axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if ((x['CarrierClaimNumber1'] == 0 ) and (str(x['lossdate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ))  else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if ((x['CarrierClaimNumber1'] != 0 ) and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:'' if('O' in {x['Total'],x['Total1'],x['Total2']} or 'C' in {x['Total'],x['Total1'],x['Total2']}) else x['ClosedDate'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total3'] if  '$' in str(x['Total3']) else "",axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total2'] if ( (x['TotalGrossIncurred']=="")  and ('$' in str(x['Total2']))  )   else (x['Total2']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) )else x['TotalGrossIncurred']),axis=1)#.astype(float)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Loss_Description'].replace(',','').replace('$',''))+float(x['USD'].replace(',','').replace('$',''))) if (re.search(r'\d{1,9}',x['Loss_Description']) and re.search(r'\d{1,9}',x['USD']) and (x['ClaimStatus']=='C'or x['ClaimStatus']=='O' ) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['USD'].replace(',','').replace('$','')) if ( re.search(r'\d{1,9}',str(x['USD']) ) and  not re.search(r'\d{1,9}',str(x['Loss_Description']) ) and (x['ClaimStatus']=='C'or x['ClaimStatus']=='O' )  ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Total2'].replace(',','').replace('$',''))+float(x['Total3'].replace(',','').replace('$',''))) if ( re.search(r'["$"]\d',str(x['Total2']) ) and re.search(r'["$"]\d',str(x['Total3']) ) and x['Total1']=='' and not re.search(r'["$"]\d',str(x['Total']) ) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Total'].replace(',','').replace('$',''))+float(x['Total1'].replace(',','').replace('$',''))) if ( re.search(r'["$"]\d',str(x['Total']) ) and re.search(r'["$"]\d',str(x['Total1']) ) and x['Total2']=='' and x['Total3']=='' ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Total1'].replace(',','').replace('$',''))+float(x['Total2'].replace(',','').replace('$',''))) if (re.search(r'["$"]\d',str(x['Total2']) ) and re.search(r'["$"]\d',str(x['Total1']) ) and x['Total']=='' and x['Total3']=='' ) else x['TotalGrossIncurred'] ,axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:'Open' if x['ClaimStatus']=='O' else('Closed' if x['ClaimStatus']  else x['ClaimStatus']),axis=1)

        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | ( popular_table['ClaimStatus'] !=""))]
        popular_table=popular_table[((popular_table['CarrierClaimNumber'] !=0)|(popular_table['CarrierClaimNumber'] =='NaN'))]

        return popular_table

    def CNA_Post_Processing_13(self,popular_table):
        popular_table['CarrierName'] ="CNA"
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('No Claims').any()) or (row.astype(str).str.contains('No losses').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['Status'].str.extract(r'([A-Z]{1}\d{1,2}[A-Z]{0,1}\d{4,5}|[A-Z]{1,4}\d{4,8}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['Closed_Date'].str.extract(r'([A-Z]{1,4}\d{4,8}|[A-Z]{1}\d{1}[A-Z]{1}\d{4,5}|\d{8})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['claim number'].str.extract(r'([A-Z]{1}\d{0,1}[A-Z]{0,1}\d{5,8}-\d{2})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:re.findall(r'(\w{2,4}\d{4,6})',x['lossstate'])[0] if re.findall(r'(\w{2}\d{6})',x['lossstate']) else x['CarrierClaimNumber'],axis=1)

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  ((str(x['lossstate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ))  else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if  (("/" not in  str(x['LossDate']) ) and  (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  (("/" not in  str(x['LossDate']) ) and  (str(x['Closed_Date']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ))  else x['LossDate'] ,axis =1 )

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[1] if  (("/" not in  str(x['LossDate']) ) and  (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=2 ))  else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Closed_Date']) or ('Close' in x['Closed_Date']) or ('CLOSE' in x['Closed_Date'])) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Closed_Date']) or ('Open' in x['Closed_Date']) or ('OPEN' in x['Closed_Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Adjuster_Name']) or ('Close' in x['Adjuster_Name']) or ('CLOSE' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Adjuster_Name']) or ('Open' in x['Adjuster_Name']) or ('OPEN' in x['Adjuster_Name'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['lossdate']) or ('Close' in x['lossdate']) or ('CLOSE' in x['lossdate']) or ('Clsd' in x['lossdate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['lossdate']) or ('Open' in x['lossdate']) or ('OPEN' in x['lossdate'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status']) or ('Close' in x['Status']) or ('CLOSE' in x['Status']) or ('Clsd' in x['Status'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status']) or ('Open' in x['Status']) or ('OPEN' in x['Status'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss_Description']) or ('Close' in x['Loss_Description']) or ('CLOSE' in x['Loss_Description']) or ('Clsd' in x['Loss_Description'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss_Description']) or ('Open' in x['Loss_Description']) or ('OPEN' in x['Loss_Description'])) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[0] if  ((str(x['claim number']).count('/')>=2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=1 ))  else '' ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if ((x['CarrierClaimNumber1'] == 0 ) and (str(x['lossdate']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ))  else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if ((x['CarrierClaimNumber1'] != 0 ) and (str(x['Status']).count('/')==2) and (len(re.findall(r'(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:'' if (x['CarrierClaimNumber']==x['Status']) else x['ClosedDate'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total2'] if  '$' in str(x['Total2']) else "",axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total1'] if ( (x['TotalGrossIncurred']=="")  and ('$' in str(x['Total1']))  )   else (x['Total1']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) )else x['TotalGrossIncurred']),axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total'] if ( (x['TotalGrossIncurred']=="")  and ('$' in str(x['Total']))  )   else (x['Total']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) )else x['TotalGrossIncurred']),axis=1)#.astype(float)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['USD'] if ( (x['TotalGrossIncurred']=="")  and ('$' in str(x['USD']))  )   else (x['USD']  if ((x['ClaimStatus'] !="") and ( x['TotalGrossIncurred'] =="" ) )else x['TotalGrossIncurred']),axis=1)#.astype(float)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Total'].replace(',','').replace('$',''))+float(x['Total1'].replace(',','').replace('$',''))) if (re.search(r'["$"]\d',x['Total']) and re.search(r'["$"]\d',x['Total1']) and x['Total2']=='' and not re.search(r'["$"]\d',x['USD']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['USD'].replace(',','').replace('$',''))+float(x['Total'].replace(',','').replace('$',''))) if (re.search(r'["$"]\d',x['Total']) and re.search(r'["$"]\d',x['USD']) and not re.search(r'["$"]\d',x['Total1']) and not re.search(r'["$"]\d',x['Loss_Description']) ) else x['TotalGrossIncurred'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: (float(x['Total1'].replace(',','').replace('$',''))+float(x['Total2'].replace(',','').replace('$',''))) if (re.search(r'["$"]\d',x['Total1']) and re.search(r'["$"]\d',x['Total2'])  and not re.search(r'["$"]\d',x['Total']) ) else x['TotalGrossIncurred'] ,axis=1)


        popular_table = popular_table[ popular_table['ClaimStatus'] !=""]
        return popular_table
