import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class Liberty():

    def __init__(self):
        print("", end="")

    def Liberty(self, df_clean):
        # print("df_clean->",df_clean)
        df=self.get_popular_table(df_clean)
        df=self.Post_Processing(df)
        return df

    def liberty_Post_Processing_11(self,popular_table):
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness']  = popular_table.apply(lambda x: 'WC' if 'WC' in str(x['claim number']) else "",axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' in x['Status Closed Date'])and (str(x['Status Closed Date']).count('/')==2) and (len(str(x['CarrierClaimNumber'])) >5) ) else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['L.O.B'] if (('/' not in x['LossDate']) and ('/' in x['L.O.B'])and (str(x['L.O.B']).count('/')==2)) else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) or ('CLOSE' in x['Status Closed Date'])) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) or ('OPEN' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) or ('CLOSE' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) or ('OPEN' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['claim number']) or ('Close' in x['claim number']) or ('CLOSE' in x['claim number'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['claim number']) or ('Open' in x['claim number']) or ('OPEN' in x['claim number'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['loss date'] if ((str(x['loss date']).count('/')==2 )and ('/' not in str(x['Status Closed Date'])) and (len(str(x['CarrierClaimNumber'])) >5)) else  "",axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['L.O.B'] if (('/' not in str(x['NoticeDate'])) and (str(x['L.O.B']).count('/')==2 ) and (len(str(x['CarrierClaimNumber'])) >5)) else x['NoticeDate'],axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (( '/' in str(x['Status Closed Date']) and  '/' in str(x['L.O.B']) and x['CarrierClaimNumber']!=x['claim number'] ) or ('/' in str(x['Status Closed Date']) and  '/' in str(x['L.O.B']) and '/' in str(x['loss date'])  ))
                                                                   else(x['L.O.B'] if (('/' in str(x['loss date']) and  '/' in str(x['L.O.B']) and x['CarrierClaimNumber']!=x['claim number'] ) or ('/' in str(x['Adjuster Name']) and  '/' in str(x['L.O.B']) and '/' in str(x['loss date'])  )) else ''),axis=1).shift(-3)
        popular_table['ClosedDate1'] = popular_table.apply(lambda x:x['loss date'] if (  ('/' in x['loss date']) and  ('/' not in x['ClosedDate'] ) and ('UNDEFINED' in str(x['claim number'])) and ('UNDEFINED' in str(x['Loss State'])) )
                                                           else (x['Adjuster Name'] if (('/' in x['Adjuster Name']) and  ('/' not in x['ClosedDate']) and ('UNDEFINED' in str(x['claim number'])) and ('UNDEFINED' in str(x['Status Closed Date']))) else x['ClosedDate'] ),axis =1 ).shift(-4)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['ClosedDate1'] if x['ClosedDate1'] else x['ClosedDate'],axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total paid'] if '$' in str(x['total paid']) else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total'] if (('.' not in str(x['TotalGrossIncurred']) )and ('$' in str(x['total']))) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['USD'] if (('.' not in str(x['TotalGrossIncurred']) )and ('$' in str(x['USD']))) else x['TotalGrossIncurred'],axis=1)

        popular_table['ClaimStatus'] =popular_table['ClaimStatus'].shift(-2)
        popular_table = popular_table[((popular_table['ClosedDate'].str.contains("/")) | (popular_table['ClaimStatus'] !="") | ( popular_table['CarrierClaimNumber'] !=0) )]

        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) & ( popular_table['CarrierClaimNumber'] !=0) )]
        popular_table['schema'] =11

        return popular_table

    def liberty_Post_Processing_10(self,popular_table):
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness']  = popular_table.apply(lambda x: 'WC' if 'WC' in str(x['claim number']) else "",axis=1)

        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{2,3}[A-Z]{1}\d{4,8})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if str(x['Status Closed Date']).count('/')==2 else np.nan ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['L.O.B'] if (('/' not in str(x['LossDate']))and (str(x['L.O.B']).count('/')==2)) else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss State'] if (('/' not in str(x['LossDate']))and (str(x['Loss State']).count('/')==2)) else x['LossDate'] ,axis =1 )
        popular_table['LossDate1'] = popular_table.apply(lambda x:x['claim number'] if  (str(x['claim number']).count('/')==2) else "" ,axis =1 ).shift(-1)
        popular_table['LossDate']  = popular_table['LossDate'].combine_first(popular_table['LossDate1'])

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) or ('CLOSE' in x['Loss State'])) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) or ('OPEN' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) or ('CLOSE' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) or ('OPEN' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['L.O.B'] if ((str(x['L.O.B']).count('/')==2)and (('/' in x['Status Closed Date'] ) or ('/' in x['Loss State'] ) ))  else  np.nan,axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Loss State']).count('/')==2)and ('/' not in x['L.O.B'] ) and (str(x['Status Closed Date']).count('/')==2))  else  x['NoticeDate'],axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['loss date'] if ((str(x['loss date']).count('/')==2)and ('/' in x['L.O.B'] ))  else  x['NoticeDate'],axis=1)
        popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Loss State'] if (('/' not in str(x['NoticeDate']))and (str(x['Loss State']).count('/')==2)) else "" ,axis =1 ).shift(-1)
        popular_table['NoticeDate']  = popular_table['NoticeDate'].combine_first(popular_table['NoticeDate1'])

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (( '/' in str(x['Status Closed Date']) and  '/' in str(x['L.O.B']) and x['CarrierClaimNumber']!=x['claim number'] ) or ('/' in str(x['Status Closed Date']) and  '/' in str(x['L.O.B']) and '/' in str(x['loss date'])  ))
                                                                   else(x['L.O.B'] if (('/' in str(x['loss date']) and  '/' in str(x['L.O.B']) and x['CarrierClaimNumber']!=x['claim number'] ) or ('/' in str(x['Adjuster Name']) and  '/' in str(x['L.O.B']) and '/' in str(x['loss date'])  )) else ''),axis=1).shift(-3)
        popular_table['ClosedDate1'] = popular_table.apply(lambda x:x['Loss State'] if ((str(x['Loss State']).count('/')==2) and ('/'not in x['claim number']) )  else '',axis=1).shift(-5)
        popular_table['ClosedDate2'] = popular_table.apply(lambda x:x['Loss State'] if ((str(x['Loss State']).count('/')==2) and ('/'not in x['claim number']) )  else '',axis=1).shift(-6)
        popular_table['ClosedDate3'] = popular_table.apply(lambda x:x['Loss State'] if ((str(x['Loss State']).count('/')==2) and ('/'not in x['claim number']) )  else '',axis=1).shift(-4)

        popular_table['ClosedDate4'] = popular_table.apply(lambda x:x['L.O.B'] if (  ('/' in x['L.O.B'])  and ('UNDEFINED' in str(x['claim number'])) and ('UNDEFINED' in str(x['Loss State'])) ) else (
                                       x['loss date'] if (  ('/' in x['loss date'])  and ('UNDEFINED' in str(x['claim number'])) and ('UNDEFINED' in str(x['L.O.B'])) )
                                                           else (x['Adjuster Name'] if (('/' in x['Adjuster Name']) and  ('/' not in x['ClosedDate']) and ('UNDEFINED' in str(x['claim number'])) and ('UNDEFINED' in str(x['L.O.B']))) else '' )),axis =1 ).shift(-4)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['ClosedDate'] if x['ClosedDate'] else (x['ClosedDate1'] if x['ClosedDate1'] else (x['ClosedDate2'] if x['ClosedDate2'] else (x['ClosedDate3'] if x['ClosedDate3'] else x['ClosedDate4']) )  ),axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['total'] if '$' in str(x['total']) else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['USD'] if (('.' not in str(x['TotalGrossIncurred'])) and ('$' in str(x['USD']))) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Loss Description'] if (('.' not in str(x['TotalGrossIncurred'])) and ('$' in str(x['Loss Description']))) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Adjuster Name'] if (('.' not in str(x['TotalGrossIncurred'])) and ('$' in str(x['Adjuster Name']))) else x['TotalGrossIncurred'],axis=1)

        popular_table = popular_table[(( popular_table['ClaimStatus'] !="") | ( popular_table['CarrierClaimNumber'] !=0) ) ]

        popular_table['LOB']  = popular_table.apply(lambda x:x['claim number'] if str(x['claim number'])==str(x['claim number']).upper() else "" ,axis=1 ).shift(-1)
        popular_table['ClaimStatus'] =popular_table['ClaimStatus'].shift(-1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:'' if re.search(r'[A-Za-z]',x['ClosedDate']) else x['ClosedDate'],axis=1)
        popular_table = popular_table[( (popular_table['CarrierClaimNumber']!=0) & ( popular_table['ClaimStatus'] !="")  )]
        popular_table['schema'] =10
        popular_table.drop(['NoticeDate1','LossDate1','ClosedDate1','ClosedDate2','ClosedDate3','ClosedDate4'],inplace=True,axis=1)

        return popular_table

    def liberty_Post_Processing_14(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['Status Closed Date'].split()[0] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)
        popular_table['CarrierPolicyNumber'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)

        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)

        popular_table['ClaimNumberforlob']=popular_table.apply(lambda x:x['claim number'] if str(x['claim number']) else '',axis=1).shift(-1)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss State'] if str(x['Loss State']).count('/')==2 else "" ,axis =1 )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if str(x['Status Closed Date']).count('/')==2 else "" ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if 'C' in x['total4'] else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'O' in x['total4'] else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Reopen" if 'R' in x['total4'] else x['ClaimStatus'],axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:x['loss date'] if '$' in str(x['loss date']) else "",axis=1)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table['TotalReserve'] = popular_table.apply(lambda x:x['total'] if '$' in str(x['total']) else "",axis=1)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table['TotalExpense'] = popular_table.apply(lambda x:x['total2'] if '$' in str(x['total2']) else "",axis=1)
        popular_table['TotalExpense'] = popular_table['TotalExpense'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table = popular_table[ ( popular_table['ClaimStatus'] !="")  ]
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['TotalExpense']) + float(x['TotalPaid']) + float(x['TotalReserve']),axis=1)
        popular_table['schema'] =14

        return popular_table

    def liberty_Post_Processing_15(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['Status Closed Date'].split()[0] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else '',axis=1).shift(1)
        popular_table['CarrierPolicyNumber'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)

        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss State'] if (('/' in x['Loss State'])and (str(x['Loss State']).count('/')==2) ) else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in x['LossDate'])and (str(x['Status Closed Date']).count('/')==2)) else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if 'C' in x['total5'] else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'O' in x['total5'] else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Loss State']).count('/')==2)and(str(x['Status Closed Date']).count('/')==2))  else  "",axis=1)

        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (str(x['total5'])!="" ) and (x['L.O.B'] =="") and (x['Adjuster Name'] =="") and ('$' in str(x['Loss Description']))) else "",axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['loss date']).replace('$','') if ( (str(x['total5'])!="" ) and (x['L.O.B'] !="") and ('$' in str(x['loss date'])) and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Adjuster Name']).replace('$','') if ( (x['Adjuster Name'] !="") and ('$' in str(x['Adjuster Name'])) and (x['L.O.B'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x==""))else x.replace('$',"").replace(',',""))

        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total paid']).replace('$','') if ((str(x['total5'])!="" ) and (x['Adjuster Name'] =="") and ('$' in str(x['total paid']))) else "",axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total']).replace('$','') if ( (str(x['total5'])!="" ) and (x['total2'] =="") and (x['USD'] =="") and ('$' in str(x['total'])) and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total paid']).replace('$','') if ( (x['Adjuster Name'] !="")  and (x['L.O.B'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table['TotalExpense']  = popular_table.apply(lambda x:str(x['total3']).replace('$','') if ((str(x['total5'])!="" ) and ('$' in str(x['total3'])) and (str(x['ClaimStatus'])!="" )) else "",axis=1)
        popular_table['TotalExpense'] = popular_table['TotalExpense'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table = popular_table[ ( popular_table['ClaimStatus'] !="") & ( popular_table['CarrierClaimNumber'] !=0) ]
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['TotalExpense']) + float(x['TotalPaid']) + float(x['TotalReserve']),axis=1)

        popular_table['schema'] =15

        return popular_table

    def liberty_Post_Processing_9(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']

        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)
        popular_table['CarrierClaimNumber8']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{2}[A-Z]{1}\d{8})', expand=True)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber8'])

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LineOfBusiness']  = popular_table.apply(lambda x:x['claim number'] if ((str(x['claim number'])==str(x['claim number']).upper() )and ('/' not in str(x['claim number'])) and ('unknow' not in str(x['claim number'])) ) else "" ,axis=1 ).shift(-1)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if str(x['Status Closed Date']).count('/')==2 else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['L.O.B'] if (('/' not in x['LossDate'])and (str(x['L.O.B']).count('/')==2)) else x['LossDate'] ,axis =1 )
        popular_table['LossDate1'] = popular_table.apply(lambda x:x['claim number'] if (('/' not in x['LossDate'])and (str(x['claim number']).count('/')==2)) else x['LossDate'] ,axis =1 ).shift(-1)

        popular_table['LossDate2'] = popular_table.apply(lambda x:x['claim number'] if ( (str(x['Loss State']).count('/')==2) and (str(x['claim number']).count('/')==2) and not (x['LossDate1']) ) else '' ,axis =1 ).shift(-1)
        popular_table['LossDate']  = popular_table['LossDate'].combine_first(popular_table['LossDate1'])
        popular_table['LossDate']  = popular_table.apply(lambda x:x['LossDate2'] if (not x['LossDate'] and x['LossDate2']) else x['LossDate'],axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Loss State']) or ('Close' in x['Loss State']) or ('CLOSE' in x['Loss State'])) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Loss State']) or ('Open' in x['Loss State']) or ('OPEN' in x['Loss State'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['Status Closed Date']) or ('Close' in x['Status Closed Date']) or ('CLOSE' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['Status Closed Date']) or ('Open' in x['Status Closed Date']) or ('OPEN' in x['Status Closed Date'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table['ClaimStatus'].shift(-1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['L.O.B'] if ((str(x['L.O.B']).count('/')==2)and ('/' in x['Status Closed Date'] ))  else  "",axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['loss date'] if ((str(x['loss date']).count('/')==2)and ('/' not in x['Status Closed Date'] ))  else  x['NoticeDate'],axis=1)
        popular_table['NoticeDate1'] = popular_table.apply(lambda x:x['Loss State'] if (('/' not in x['NoticeDate'])and (str(x['Loss State']).count('/')==2)) else x['NoticeDate'] ,axis =1 ).shift(-1)

        popular_table['NoticeDate2'] = popular_table.apply(lambda x:x['Loss State'] if ( (str(x['Loss State']).count('/')==2) and (str(x['claim number']).count('/')==2) and not (x['NoticeDate1']) ) else '' ,axis =1 ).shift(-1)
        popular_table['NoticeDate']  = popular_table['NoticeDate'].combine_first(popular_table['NoticeDate1'])
        popular_table['NoticeDate']  = popular_table.apply(lambda x:x['NoticeDate2'] if (not x['NoticeDate'] and x['NoticeDate2']) else x['NoticeDate'],axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Loss State'] if ((str(x['Loss State']).count('/')==2) and ('/'not in x['claim number']))  else  "",axis=1).shift(-1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['USD'] if '$' in str(x['USD']) else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Loss Description'] if (('.' not in str(x['TotalGrossIncurred'])) and ('$' in str(x['Loss Description']))) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Adjuster Name'] if (('.' not in str(x['TotalGrossIncurred'])) and ('$' in str(x['Adjuster Name']))) else x['TotalGrossIncurred'],axis=1)

        popular_table = popular_table[((popular_table['LossDate'].str.contains("/")) | ( popular_table['ClaimStatus'] !="") |(popular_table['ClosedDate'].str.contains("/"))  )]
        popular_table['ClosedDate']  = popular_table['ClosedDate'].shift(-1)

        popular_table = popular_table[((popular_table['CarrierClaimNumber'] !=0) | ( popular_table['ClaimStatus'] !="")   )]
        popular_table = popular_table[((popular_table['L.O.B'] !='') | ( popular_table['loss date'] !="")  | ( popular_table['Adjuster Name'] !="") | ( popular_table['Loss Description'] !="")  )]

        popular_table.drop(['NoticeDate1','LossDate1','NoticeDate2','LossDate2'],inplace=True,axis=1)
        popular_table['schema'] =9

        return popular_table


    def liberty_Post_Processing_13(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['Loss State'].split()[0] if (('Policy Number:' in x['claim number']) and (len(x['Loss State'].split())>1) ) else np.nan,axis=1).shift(1)
        popular_table['CarrierPolicyNumber'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)

        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)

        popular_table['ClaimNumberforlob']=popular_table.apply(lambda x:x['claim number'] if str(x['claim number']) else '',axis=1).shift(-1)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss State'] if (('/' in x['Loss State'])and (str(x['Loss State']).count('/')==2) ) else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in x['LossDate'])and (str(x['Status Closed Date']).count('/')==2)) else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('C' in x['total3']) or ('close' in x['total3']) or ('Close' in x['total3']) or ('CLOSE' in x['total3']) ) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('O' in x['total3']) or ('open' in x['total3']) or ('Open' in x['total3']) or ('OPEN' in x['total3']) ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Reopen" if 'R' in x['total3'] else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in x['L.O.B']) or ('Close' in x['L.O.B']) or ('CLOSE' in x['L.O.B'])) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in x['L.O.B']) or ('Open' in x['L.O.B']) or ('OPEN' in x['L.O.B'])) else x['ClaimStatus'],axis=1)

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Loss State']).count('/')==2) and (str(x['Status Closed Date']).count('/')==2))  else  "",axis=1)

        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['loss date']).replace('$','') if ((str(x['total2'])!="" ) and (str(x['total3'])!="" ) and ('$' in str(x['loss date']))) else "",axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['total2']).replace('$','') if ((x['total3']=="" ) and (str(x['CarrierClaimNumber'])!=0 ) and (x['TotalPaid'] =="") and ("$" in  str(x['total2']))) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x==""))else x.replace('$',"").replace(',',""))

        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['USD']).replace('$','') if ((str(x['total2'])!="" ) and (str(x['total3'])!="" ) and ('$' in str(x['USD']))) else "",axis=1)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table['TotalExpense']  = popular_table.apply(lambda x:str(x['total paid']).replace('$','') if ((str(x['total2'])!="" ) and (str(x['total3'])!="" ) and ('$' in str(x['total paid']))) else "",axis=1)
        popular_table['TotalExpense'] = popular_table['TotalExpense'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['TotalExpense']) + float(x['TotalPaid']) + float(x['TotalReserve']),axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and ((x['LossDate'] !="") or (x['NoticeDate'] !="") ) )else"" ,axis=1)

        popular_table = popular_table[( popular_table['ClaimStatus'] !="") ]

        popular_table['schema'] =13

        return popular_table

    def liberty_Post_Processing_16(self,popular_table):
        popular_table = popular_table
        popular_table['CarrierName'] ="liberty"
        popular_table['Page#']=popular_table['Page#']
        popular_table['Order']=popular_table['Order']
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['Status Closed Date'].split()[0] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)
        popular_table['CarrierPolicyNumber'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('Policy Number:' in x['claim number']) and (len(x['Status Closed Date'].split())>1) ) else np.nan,axis=1).shift(1)

    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['L.O.B'] if (re.search(r'(\d{7,9})',x['Loss State']) and not x['LineOfBusiness']) else x['LineOfBusiness'],axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{3}[A-Z]{1}\d{5})', expand=True)
        popular_table['CarrierClaimNumber5']  = popular_table['claim number'].str.extract(r'([A-Z]{2}\d{8,11})', expand=True)
        popular_table['CarrierClaimNumber3']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{9,11})', expand=True)
        popular_table['CarrierClaimNumber4']  = popular_table['claim number'].str.extract(r'([A-Z]{1}\s\d{2,4}[A-Z]{1}\d{6,8})', expand=True)
        popular_table['CarrierClaimNumber6']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['CarrierClaimNumber7']  = popular_table['claim number'].str.extract(r'(X\s\d{11})', expand=True)
    #     popular_table['CarrierClaimNumber8']  = popular_table['claim number'].str.extract(r'(\d{8,9})', expand=True)
        popular_table['ClaimNumberforlob']=popular_table.apply(lambda x:x['claim number'] if str(x['claim number']) else '',axis=1).shift(-1)

        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber4'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber5'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber6'])
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber7'])
    #     popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber8'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['Loss State'] if ((x['CarrierClaimNumber'] ==0)and ('/'not in x['Loss State']) )else x['CarrierClaimNumber'] ,axis =1 )


        popular_table['LossDate'] = popular_table.apply(lambda x:x['Loss State'] if str(x['Loss State']).count('/')==2 else "" ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if (('/' not in str(x['LossDate']))and (str(x['Status Closed Date']).count('/')==2)) else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if 'C' in x['total6'] else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'O' in x['total6'] else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Reopen" if 'R' in x['total6'] else x['ClaimStatus'],axis=1)


        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status Closed Date'] if ((str(x['Status Closed Date']).count('/')==2) and (x['claim number']  !=""))  else  "",axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['L.O.B'] if ((str(x['L.O.B']).count('/')==2) and (x['claim number']  =="") and (x['NoticeDate']=="") )  else  x['NoticeDate'],axis=1)

        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (str(x['claim number'])!="") and (x['L.O.B'] =="") and (x['Adjuster Name'] =="") and (x['USD'] =="") and ('$' in str(x['Loss Description']))) else "",axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (str(x['claim number']) =="") and (x['loss date'] =="") and (x['total3'] =="") and ('$' in str(x['Loss Description'])) and (x['TotalPaid'] =="") ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['loss date']).replace('$','') if ( (x['Adjuster Name'] =="") and ('$' in str(x['loss date'])) and (x['total'] =="") and (x['total3'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Adjuster Name']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['Adjuster Name'])) and (x['Loss Description'] =="") and (x['total1'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Adjuster Name']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['Adjuster Name'])) and (x['Loss Description'] =="") and (x['total3'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['Loss Description'])) and (x['Adjuster Name'] =="") and (x['total1'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Adjuster Name']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['Adjuster Name'])) and (x['total'] =="") and (x['total3'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (x['claim number'] =="") and ('$' in str(x['Loss Description'])) and (x['loss date'] =="") and (x['USD'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)
        popular_table['TotalPaid']  = popular_table.apply(lambda x:str(x['Loss Description']).replace('$','') if ( (x['claim number'] =="") and ('$' in str(x['Loss Description'])) and (x['loss date'] =="") and (x['total1'] =="")  and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1)



        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x==""))else x.replace('$',"").replace(',',""))

        popular_table['TotalReserve']  =  popular_table.apply(lambda x:str(x['total2']).replace('$','') if ( (str(x['claim number'])!="") and (x['L.O.B'] =="") and (x['Adjuster Name'] =="") and (x['USD'] =="") and ('$' in str(x['total2']))) else "",axis=1)
        popular_table['TotalReserve']  =  popular_table.apply(lambda x:str(x['total1']).replace('$','') if ( (str(x['claim number'])=="") and (x['loss date'] =="") and (x['total3'] =="")  and ('$' in str(x['total1'])) and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  =  popular_table.apply(lambda x:str(x['total1']).replace('$','') if ( (x['Adjuster Name'] =="") and ('$' in str(x['total1'])) and (x['total'] =="") and (x['total3'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  =  popular_table.apply(lambda x:str(x['total2']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['total2'])) and (x['Loss Description'] =="") and (x['total1'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total1']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['total1'])) and (x['Loss Description'] =="") and (x['total3'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total2']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['total2'])) and (x['Adjuster Name'] =="") and (x['total1'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total1']).replace('$','') if ( (x['L.O.B'] =="") and ('$' in str(x['total1'])) and (x['total'] =="") and (x['total3'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total2']).replace('$','') if ( (x['claim number'] =="") and ('$' in str(x['total2'])) and (x['loss date'] =="") and (x['USD'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve']  = popular_table.apply(lambda x:str(x['total2']).replace('$','') if ( (x['claim number'] =="") and ('$' in str(x['total2'])) and (x['loss date'] =="") and (x['total1'] =="")  and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))


        popular_table['TotalExpense']  = popular_table.apply(lambda x:str(x['total4']).replace('$','') if (('$' in str(x['total3'])) and (str(x['ClaimStatus'])!="" )) else "",axis=1)
        popular_table['TotalExpense'] = popular_table['TotalExpense'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',"").replace(',',""))


        popular_table = popular_table[( (popular_table['CarrierClaimNumber']!="") & ( popular_table['ClaimStatus'] !="")  )]
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['TotalExpense']) + float(x['TotalPaid']) + float(x['TotalReserve']),axis=1)
        popular_table['schema'] = 16
    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:is_lob(x['claim number']),axis=1)
    #     popular_table['LineOfBusiness']=popular_table.apply(lambda x:is_lob(str(x['ClaimNumberforlob'])) if not x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        return popular_table

    ############# combined code for getting popular table for schema of any length  ##############

    def get_popular_table(self,new_table):
        # print('new_table',new_table)
        schemas_len=[9,10,11,13,14,15,16]
        schemas=[['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description','USD', 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description','USD',"total", 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD','total','total paid', 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD','total','total paid','total2','total3', 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description','USD',"total","total1","total2","total3","total4", 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description', 'USD','total','total paid','total2','total3','total4','total5', 'SubmissionID', 'Page#', 'Order'],
                 ['claim number', 'Loss State', 'Status Closed Date', 'L.O.B', 'loss date',
            'Adjuster Name', 'Loss Description','USD',"total","total1","total2","total3","total4","total5","total6", 'SubmissionID', 'Page#', 'Order']
                ]
        sch={}
        df={}
        k=0
        for i in schemas_len:
            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):

            l=len(new_table[a].columns)

            if l-2 in schemas_len:
                new_table[a].columns = sch[l-2]
                sch['popular_table_'+str(l-2)].append(new_table[a])

        for k,v in df.items():

            k_sch_len=''.join(re.findall(r'\d',k))
            try:
                df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)
            except:
                continue

        return df


    def Post_Processing(self,df):

        post_pro_fun=['liberty_Post_Processing_9','liberty_Post_Processing_10','liberty_Post_Processing_11','liberty_Post_Processing_13','liberty_Post_Processing_14','liberty_Post_Processing_15','liberty_Post_Processing_16']
        # post_pro_fun=['liberty_Post_Processing_9','liberty_Post_Processing_10','liberty_Post_Processing_11','liberty_Post_Processing_13','liberty_Post_Processing_14','liberty_Post_Processing_15','liberty_Post_Processing_16']
        post_pro=[self.liberty_Post_Processing_9,self.liberty_Post_Processing_10,self.liberty_Post_Processing_11,self.liberty_Post_Processing_13,self.liberty_Post_Processing_14,self.liberty_Post_Processing_15,self.liberty_Post_Processing_16]#self.liberty_Post_Processing_15,self.liberty_Post_Processing_16]

        for idx ,pp in enumerate(post_pro):

            sch_len=str(post_pro_fun[idx]).split('Processing_')[1]

            # df['df_'+sch_len]=pp(df['df_'+sch_len])
            try:
                df['df_'+sch_len]=pp(df['df_'+sch_len])
            except Exception as e:
                print('there was an error with your input in schema of length-{0}  : {1}'.format(sch_len,e))
                df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
                continue
        # print("df",df)
        return df
