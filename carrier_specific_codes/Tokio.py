import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class Tokio():

    def __init__(self):
        self.replace_dict = {}
        self.cov_lst = ['Flexi Five Part2 Sex Discrimination','Flexi Five Part 1 All Othe','Flexi Five Part 2 Race Discrimination',
 'Flexi Five Part 2 Disability Discrimination','BYLAW VIOLATION','Private Co Part 2- Sex Discrimination','Private Co Part 2- All Other',
 'Cover BLDG- Fire Other','Cover Bus. Intrpt. - Fire Other','Crime Employee Dishonesty','General Liability Bodily Injur',
 'General Liability Delux','GL PROPERTY DAMAGE ON COMBINED GLBI','General Liability Bodily Injury','Private Co Part 2 - Wrongful Termination',
 'Private Company, Part 2 - Retaliation','Private Company, Part 2 - Wage and Hour','Private Co Part 2 - Race Discrimina',
 'Private Co Part 2 - Disability Disc','Private Company, Part 2 - Sexual Orientation','Non-Profit Directors & Officers','Flexi Five Part 1 Breach of Contrac',
 'Flexi Five Part 2 Sexual Harassment','General Liab. - Med Pay','COMBINED PROPERTY DAMAGE','Collision','Collision Total Loss','Bodily Injury',
 'COMP - Other','Cmbd Prp Dam Total Loss','COMP - Theft','No Fault','Uninsured Motorist Combined Prp Dam','Collision Rent Expense','Comp Flood Total Loss',
 'COMP - Flood','Cmbd Prp Dam Rent Expense','COMP - Vandalism','Comp Fire Total Loss','Medical Payment','Cover Bldg - Water Damage',
 'Cover Bus Intrpt - Water Damage','GL PROPERTY DAMAGE ON COM','Cover BLDG - Fire Other','Cover Bus Intrpt - Other','Cover BLDG - Other','Ult Cov Boiler Machinery','Cover BLDG - Riot','Cover Cnts - Water Damage','Professional Liability',]
        #print("", end="")

    def Tokio(self,popular_table):
#         print("in tokio processing")
        df=self.get_popular_table(popular_table)
        df=self.Post_Processing(df)
        #print("df----------",df)
        return df
    
    def Philadelphia(self,popular_table):
#         print("in tokio processing")
        df=self.get_popular_table(popular_table)
        df=self.Post_Processing(df)
        #print("df----------",df)
        return df

    def TM_Post_Processing_7(self,popular_table):
        searchfor = ['NET', 'Incurred','Adjuster']
        popular_table['CarrierName'] ="Tokiyo Marine & philadelphia"
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']).split('PRODUCER:')[0] if 'PRODUCER:' in str(x['claim number']) else "",axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossstate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossdate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
    #     print(popular_table['LineOfBusiness'])

        popular_table = popular_table[~popular_table['L.O.B'].str.contains('|'.join(searchfor) ,na=False)]
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ( (row.astype(str).str.contains('No Claimants').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['No claims'] = popular_table.apply(lambda x:'No Claims' if ((("Total for Account:" in  str(x['claim number']) ) and ('0 Claims' in str(x['lossstate']) ) ) or(("POLICY TOTAL:" in  str(x['claim number']) ) and ('CLAIMS: 0' in str(x['lossstate'])) ) )  else "" ,axis=1 )
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['lossstate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)

        popular_table['CarrierClaimNumber3'] = np.nan
        popular_table['CarrierClaimNumber3']  = popular_table['CarrierClaimNumber3'].combine_first(popular_table['CarrierClaimNumber'])
        popular_table['CarrierClaimNumber3']  = popular_table['CarrierClaimNumber3'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber3'].fillna(0,inplace = True)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if ( ('Close' in str(x['claim number'])) or ('CLOSE' in str(x['claim number']))) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['claim number'])) or ('Open' in str(x['claim number'])) or ('OPEN' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if((x['ClaimStatus'] =="") and ( ('close' in str(x['lossstate'])) or ('Close' in str(x['lossstate'])) or ('CLOSE' in str(x['lossstate']))) )else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if ((x['ClaimStatus'] =="") and (('open' in str(x['lossstate'])) or ('Open' in str(x['lossstate'])) or ('OPEN' in str(x['lossstate'])))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NO CLAIM" if ((x['ClaimStatus'] =="") and ('NO CLAIM' in str(x['claim number']))) else x['ClaimStatus'] ,axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NO CLAIM" if ((x['ClaimStatus'] =="") and ('NO CLAIM' in str(x['lossstate']))) else x['ClaimStatus'] ,axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})' ,str(x['lossstate']))[0] if (('/' in str(x['lossstate'])) and (str(x['lossstate']).count('/')>=2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ) ) else "" ,axis =1 )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['lossdate'] if (('/' in str(x['lossdate'])) and (str(x['lossdate']).count('/')==2) ) else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})' ,str(x['lossstate']))[1] if (('/' in str(x['lossstate'])) and (str(x['lossstate']).count('/')>=2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=2 ) and (x['NoticeDate'] =="") ) else x['NoticeDate'] ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Status'] if (('/' in str(x['Status'])) and (str(x['Status']).count('/')==2) ) else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})' ,str(x['lossstate']))[2] if (('/' in str(x['lossstate'])) and (str(x['lossstate']).count('/')>=2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=3 ) and (x['ClosedDate'] =="") ) else x['ClosedDate'] ,axis =1 )

        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$'," ") )[0] if ((x['CarrierClaimNumber1']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$'," ") )) >=1 )  ) else "",axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['lossstate']).split()[-1].replace(',',"").replace('$'," ")  if (('Claim Total:' in str(x['lossstate'])) and (x['CarrierClaimNumber1']!=0) and ('$' in str(x['lossstate'])) and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['claim number']).replace(',',"").replace('$'," ") )[0] if ((x['CarrierClaimNumber']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['claim number']).replace(',',"").replace('$'," ") )) >=1 ) and (x['TotalPaid']=="") ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="")or (str(x)=="nan") or (x=="-") ) else x.replace('$',""))

        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',""))[1] if ((x['CarrierClaimNumber1']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=2 ) and (str(x['lossstate']).count('.')>=2)  ) else "",axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:str(x['lossdate']).replace(',',"").replace('$'," ")  if (('Claim Total:' in str(x['lossstate'])) and (x['CarrierClaimNumber1']!=0) and ('$' in str(x['lossdate'])) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['claim number']).replace(',',""))[1] if ((x['CarrierClaimNumber']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['claim number']))) >=2 ) and (str(x['claim number']).count('.')>=2) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table['Total_EXPENSE'].apply(lambda x:float(0) if ((x=="")or (str(x)=="nan") or (x=="-") ) else x.replace('$',""))

        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['L.O.B']).split()[0].replace(',',"").replace('$',"") if ((x['CarrierClaimNumber1']!=0)  and (x['ClaimStatus'] =="") and (len(str(x['L.O.B']).split()) >=2)  ) else "",axis=1 )
        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"").replace('$'," ")  if (('Claim Total:' in str(x['lossstate'])) and (x['CarrierClaimNumber1']!=0) and ('$' in str(x['Closed_Date'])) and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1 )
        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"").replace('$',"") if ((x['CarrierClaimNumber']!=0)  and (x['ClaimStatus'] =="") and (('.' in str(x['Closed_Date'])) or ('$' in str(x['Closed_Date'])) or ('-' in str(x['Closed_Date']))) and (x['TotalReserve']=="") ) else x['TotalReserve'],axis=1 )
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="")or (str(x)=="nan") or (x=="-") )else x.replace('$',""))


        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['Total_EXPENSE']) + float(x['TotalPaid']) + float(x['TotalReserve']),axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and ((x['LossDate'] !="") or (x['NoticeDate'] !="") ) )else"" ,axis=1)

        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['CarrierClaimNumber3'] !=0)|(popular_table['CarrierClaimNumber'] !=0)| (popular_table['LineOfBusiness'] !="") )]
        popular_table['CarrierClaimNumber4'] = popular_table['CarrierClaimNumber3'].shift(-1)
        popular_table['CarrierClaimNumber4'].fillna(0,inplace = True)
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].shift(-1)
        popular_table['LineOfBusiness'] = popular_table['LineOfBusiness'].shift(2)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['CarrierClaimNumber4'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if (x['TotalGrossIncurred']== 0.00) else x['TotalGrossIncurred'] ,axis=1)
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber4','CarrierClaimNumber3','CarrierPolicyNumber1','TotalGrossIncurred1'],inplace=True,axis=1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")| (popular_table['LossDate'] !=""))]
        popular_table['schema']=7
        return popular_table

    def TM_Post_Processing_11(self,popular_table):
        #popular_table.to_csv("before_popular_table_11.csv", mode='w', header=False)
        searchfor = ['NET', 'Incurred','Adjuster']
        popular_table = popular_table.replace(r'\n',' ', regex=True)
        popular_table['CarrierName'] ="Tokiyo Marine & philadelphia"
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']).split('PRODUCER:')[0] if 'PRODUCER:' in str(x['claim number']) else "",axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossstate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['lossdate']) if (str(x['LineOfBusiness']) =="" ) else x['LineOfBusiness'],axis=1)
    #     popular_table['LineOfBusiness'] = popular_table['LineOfBusiness'].apply(lambda x:str(x) if str(x)!="nan" else np.nan)

        popular_table = popular_table[~popular_table['Loss_Description'].str.contains('|'.join(searchfor) ,na=False)]
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('NO CLAIM').any()) or (row.astype(str).str.contains('No Loss').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['lossstate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['lossdate'].str.extract('([A-Z]+\d{8,})|\d{11,13}', expand=True)
        popular_table['CarrierClaimNumber3'] = popular_table['Closed_Date'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber4'] = popular_table['Status'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber5'] = np.nan
        popular_table['CarrierClaimNumber5']  = popular_table['CarrierClaimNumber5'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber5']  = popular_table['CarrierClaimNumber5'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber5']  = popular_table['CarrierClaimNumber5'].combine_first(popular_table['CarrierClaimNumber3'])
        popular_table['CarrierClaimNumber5']  = popular_table['CarrierClaimNumber5'].combine_first(popular_table['CarrierClaimNumber4'])

        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber2'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber3'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber4'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber5'].fillna(0,inplace = True)

        popular_table['CoverageType'] = popular_table.apply(lambda x:str(x['Status'])+str(x['lossstate']) ,axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:self.is_lob(x['CoverageType']),axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:str(x['lossdate']) if x['CoverageType']==None and x['lossdate']!="" else x['CoverageType'] ,axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:self.is_lob(x['CoverageType']) if x['CoverageType']!=None else "",axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['claim number'])) or ('Close' in str(x['claim number'])) or ('CLOSE' in str(x['claim number']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['claim number'])) or ('Open' in str(x['claim number'])) or ('OPEN' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['lossstate'])) or ('Close' in str(x['lossstate'])) or ('CLOSE' in str(x['lossstate']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['lossstate'])) or ('Open' in str(x['lossstate'])) or ('OPEN' in str(x['lossstate']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CLosed" if 'CL' in str(x['Status']) else x['ClaimStatus'] ,axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"No Pay" if 'NP' in str(x['Status']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Re-Open" if 'RO' in str(x['Status']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'OP' in str(x['Status']) else x['ClaimStatus'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ))  else "" ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ) and (x['LossDate']=="" ) )  else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=1 )  and (x['LossDate']=="" )  )  else x['LossDate'] ,axis =1 )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 )  and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ) )  else "",axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))) >=1 ) and (x['NoticeDate']=="" ) )  else x['NoticeDate'],axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))[1] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['claim number']))) >=2 )  and (x['NoticeDate']=="" )  )  else x['NoticeDate'] ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))[0] if  ( (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))) >=1 ) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 )  and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ) )  else "" ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Adjuster_Name']))[0] if  ( (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Adjuster_Name']))) >=1 )  and (x['ClosedDate']=="" )  )  else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 )  and (x['ClosedDate']=="" )  )  else x['ClosedDate'] ,axis =1 )

        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['Status']).replace(',',"").replace('$','')) if (( ('.' in str(x['Status']) )  )and (x['CarrierClaimNumber1'] !=0) ) else (float(0) if ('-' in str(x['Status']) and (x['CarrierClaimNumber1'] !=0)) else "") ,axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['Loss_Description']).replace(',',"").replace('$','')) if (('.' in str(x['Loss_Description'])) and ('.' in str(x['Total']) ) and (x['CarrierClaimNumber2'] ==0) ) else (float(0) if ('-' in str(x['Loss_Description']) and (x['CarrierClaimNumber2'] ==0) and ('.' in str(x['Total']))) else x['TotalPaid']),axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['lossdate']).split()[1].replace(',',"").replace('$','')) if ( ( ('.' in str(x['lossdate']) )  ) and (len(str(x['lossdate']).split()) >1) and (x['CarrierClaimNumber2'] !=0)  )  else  (float(0) if ('-' in str(x['lossdate']) and (x['CarrierClaimNumber2'] !=0)) else x['TotalPaid']),axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['L.O.B']).replace(',',"").replace('$','')) if ( ( ('.' in str(x['L.O.B']) ) or ('$' in str(x['L.O.B']) )  )  and (x['CarrierClaimNumber3'] !=0)  )  else  (float(0) if ('-' in str(x['L.O.B']) and (x['CarrierClaimNumber3'] !=0)) else x['TotalPaid']),axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['Closed_Date']).replace(',',"").replace('$','')) if ( (('$' in str(x['Closed_Date']) ) or ('.' in str(x['Closed_Date']) )  ) and  (x['CarrierClaimNumber4'] !=0)  )  else (float(0) if (('-' in str(x['Closed_Date']) )  and (x['CarrierClaimNumber4'] !=0) ) else x['TotalPaid']),axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(str(x['Status']).replace(',',"").replace('$','') )if ( ( ('.' in str(x['Status']) )  ) and (x['CarrierClaimNumber2'] !=0) and (x['TotalPaid']=="") )  else (float(0) if ('-' in str(x['Status']) and (x['CarrierClaimNumber2'] !=0) and (x['TotalPaid']=="") ) else x['TotalPaid']),axis=1 )
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="") or (str(x)=='0.00')) else x )

        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['Status']).split()[-1].replace(',',"").replace('$','')) if ( (str(x['Status']).count('.')>=2 ) and (len(str(x['Status']))>2 )    and  (x['CarrierClaimNumber4'] !=0)  )  else "",axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['Closed_Date']).replace(',',"").replace('$','')) if (( ('.' in str(x['Closed_Date']) )  )and (x['CarrierClaimNumber1'] !=0) )  else (float(0) if (('-' in str(x['Closed_Date']) ) and (x['CarrierClaimNumber1'] !=0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1)#.astype(float)
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['USD']).replace(',',"").replace('$','')) if (('.' in str(x['USD'])) and ('.' in str(x['Total'])) and (x['CarrierClaimNumber2'] ==0) and (x['Total_EXPENSE']=="")  ) else (float(0) if (('-' in str(x['USD']) ) and ('.' in str(x['Total'])) and (x['CarrierClaimNumber2'] ==0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1)#.astype(float)
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['Closed_Date']).replace(',',"").replace('$','')) if ( ('.' in str(x['Closed_Date']) or ('$' in str(x['Closed_Date']) ) ) and  (x['CarrierClaimNumber2'] !=0) and (x['Total_EXPENSE']=="")  )  else (float(0) if (('-' in str(x['Closed_Date']) )  and (x['CarrierClaimNumber2'] !=0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['Adjuster_Name']).replace(',',"").replace('$','')) if ( (('$' in str(x['Adjuster_Name']) ) or ('.' in str(x['Adjuster_Name']) )  ) and  (x['CarrierClaimNumber3'] !=0) and (x['Total_EXPENSE']=="")  )  else (float(0) if (('-' in str(x['Adjuster_Name']) )  and (x['CarrierClaimNumber3'] !=0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['L.O.B']).replace(',',"").replace('$','')) if ( (('$' in str(x['L.O.B']) ) or ('.' in str(x['L.O.B']) ))   and  (x['CarrierClaimNumber4'] !=0) and (x['Total_EXPENSE']=="")  )  else (float(0) if (('-' in str(x['L.O.B']) )  and (x['CarrierClaimNumber4'] !=0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:float(str(x['Status']).replace(',',"").replace('$','')) if ( (('.' in str(x['Status']) )  or ('$' in str(x['Status'])) ) and  (x['CarrierClaimNumber2'] !=0) and (x['Total_EXPENSE']=="") )  else (float(0) if (('-' in str(x['Status']) ) and ('$' in str(x['Status'])) and (x['CarrierClaimNumber2'] !=0) and (x['Total_EXPENSE']=="") ) else x['Total_EXPENSE']),axis=1 )
        popular_table['Total_EXPENSE'] = popular_table['Total_EXPENSE'].apply(lambda x:float(0)  if ((x=="") or (str(x)=='-') ) else x )

        popular_table['TotalReserve'] = popular_table.apply(lambda x:float(str(x['USD']).replace(',',"").replace('$','')) if (( ('.' in str(x['USD']) )  )and ((x['CarrierClaimNumber1'] !=0) or (x['CarrierClaimNumber2'] !=0) or (x['CarrierClaimNumber3'] !=0) or (x['CarrierClaimNumber4'] !=0) ) )  else (float(0) if (('-' in str(x['USD']) ) and ((x['CarrierClaimNumber1'] !=0) or (x['CarrierClaimNumber2'] !=0) or (x['CarrierClaimNumber3'] !=0) or (x['CarrierClaimNumber4'] !=0) ) ) else ""),axis=1)#.astype(float)
        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Total']).split()[0] if ((len(str(x['Total']).split())>=2 )  and (x['CarrierClaimNumber2'] !=0) and (x['TotalReserve']=="")  )  else x['TotalReserve'],axis=1)#.astype(float)
        popular_table['TotalReserve'] = popular_table.apply(lambda x:float(str(x['Loss_Description']).replace(',',"").replace('$','')) if ( ('.' in str(x['Loss_Description']) or ('$' in str(x['Loss_Description']) ) ) and  (x['CarrierClaimNumber4'] !=0) and (x['TotalReserve']=="")  )  else (float(0) if (('-' in str(x['Loss_Description']) ) and  (x['CarrierClaimNumber4'] !=0) and (x['TotalReserve']=="") ) else x['TotalReserve']),axis=1 )
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0)  if ((x=="") or (str(x)=='-') ) else x )

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:float(str(x['Loss_Description']).replace(',',"").replace('$','')) if (('.' not in str(x['Total'])) and ( str(x['Loss_Description']).replace('.','').replace(',',"").replace('$','' ).isdigit() )) else "",axis=1)
        popular_table['TotalGrossIncurred1'] = popular_table.apply(lambda x: float(x['Total_EXPENSE']) + float(x['TotalPaid'] + float(x['TotalReserve'])) if x['TotalGrossIncurred'] =="" else "" ,axis=1)

        popular_table['TotalGrossIncurred_comment'] = popular_table.apply(lambda x:'TotalPaid + Total_EXPENSE' if x['TotalGrossIncurred1'] !='' else 'TotalGrossIncurred sepratly found' ,axis=1)
        popular_table['TotalGrossIncurred']  = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if x['TotalGrossIncurred']=="" else x['TotalGrossIncurred'] ,axis=1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and (x['LossDate'] !="")) else"" ,axis=1)

        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['CarrierClaimNumber5'] !=0)|(popular_table['CarrierClaimNumber'] !=0)|(popular_table['LineOfBusiness'] !="") )]
        popular_table['CarrierClaimNumber6'] = popular_table['CarrierClaimNumber5'].shift(-1)
        popular_table['TotalGrossIncurred2'] = popular_table['TotalGrossIncurred'].shift(-1)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['CarrierClaimNumber6'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred2'] if (x['CarrierClaimNumber6']!= 0) else x['TotalGrossIncurred'] ,axis=1)
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber3','CarrierClaimNumber4','CarrierClaimNumber5','TotalGrossIncurred1'],inplace=True,axis=1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="") | (popular_table['LossDate'] !="")|(popular_table['LineOfBusiness'] !=""))]
        popular_table['LineOfBusiness'] = popular_table['LineOfBusiness'].shift(1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")| (popular_table['LineOfBusiness'] !=""))]
        popular_table['schema']=11
        #popular_table.to_csv("after_popular_table_11.csv", mode='w', header=False)
        return popular_table

    def TM_Post_Processing_8(self,popular_table):
        searchfor = ['NET', 'Incurred','Adjusftter']
        popular_table['CarrierName'] ="Tokiyo Marine & philadelphia"
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']).split('PRODUCER:')[0] if 'PRODUCER:' in str(x['claim number']) else "",axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossstate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossdate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
    #     print(popular_table['LineOfBusiness'])
    #     popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['lossstate']) if (str(x['LineOfBusiness']) =="" and str(x['LineOfBusiness']).count("/")==2) else x['LineOfBusiness'],axis=1)
    #     popular_table['LineOfBusiness'] = popular_table['lossstate'].str.extract('([A-Za-z _]*)', expand=True)

        popular_table = popular_table[~popular_table['Loss_Description'].str.contains('|'.join(searchfor) ,na=False)]
        popular_table['No Losses'] = popular_table['claim number'].apply(lambda x:"No Claims" if ('NO CLAIM' in str(x))  else "")
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['lossstate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
    #     popular_table['CarrierClaimNumber1']  = popular_table['CarrierClaimNumber1'].combine_first(popular_table['CarrierClaimNumber'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)

        popular_table['CoverageType'] = popular_table['lossdate'].str.extract('([A-Za-z _]*)', expand=True)
        popular_table['CoverageType'] = popular_table.apply(lambda x:x['CoverageType'] if x['CarrierClaimNumber'] !="" else ['CoverageType'],axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:self.is_lob(str(x['CoverageType'])) ,axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['claim number'])) or ('Close' in str(x['claim number'])) or ('CLOSE' in str(x['claim number']))) else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['claim number'])) or ('Open' in str(x['claim number'])) or ('OPEN' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CLosed" if 'CL' in str(x['lossdate']) else x['ClaimStatus'] ,axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NP" if 'NP' in str(x['lossdate']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"RO" if 'RO' in str(x['lossdate']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'OP' in str(x['lossdate']) else x['ClaimStatus'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))[0] if  ( (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossstate']))) >=1 ))  else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if  (  (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ) and (x['LossDate']==""))  else x['LossDate'] ,axis =1 )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[0] if ((str(x['lossdate']).count('/')==2)  and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ) ) else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[1] if  ( (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=2 ) and (x['NoticeDate']=="") )  else x['NoticeDate'] ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))[0] if ((str(x['Status']).count('/')==2)  and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Status']))) >=1 ) ) else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))[2] if  ( (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=3 ) and (x['ClosedDate']==""))  else x['ClosedDate'] ,axis =1 )


        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(0) if((x['CarrierClaimNumber1'] !=0) and (len(str(x['lossstate']).split())>=2 ) and (str(x['lossstate']).split()[1] =='-') ) else "",axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$',""))[0] if(((x['CarrierClaimNumber1'] !=0)or (x['CarrierClaimNumber'] !=0) )and   (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=1) and (x['TotalPaid'] =="") ) else x['TotalPaid'],axis=1 )

        popular_table['TotalPaid'] = popular_table.apply(lambda x:float(0) if((x['CarrierClaimNumber'] !=0) and (len(str(x['lossstate']).split())>=2 ) and (str(x['lossstate']).split()[0] =='-')and (x['TotalPaid'] =="") ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Status']).replace(',',"").replace('$',""))[0] if((x['CarrierClaimNumber'] !=0) and   (len(re.findall('(\d+\.\d{2})', str(x['Status']))) >=1) and (x['TotalPaid'] =="") ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Status']).replace(',',"").replace('$',''))[0] if ((x['CarrierClaimNumber'] !=0) and ('.' not in str(x['USD']))  and (len(re.findall('(\d+\.\d{2})', str(x['Status']))) >=1)and (x['TotalPaid'] =="") )else x['TotalPaid'],axis=1)#.astype(float)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="")or(str(x)=='nan')or(str(x)=='-')) else str(x).replace('$',""))

        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$',''))[0] if((x['CarrierClaimNumber1'] !=0) and (len(str(x['lossstate']).split())>=2 ) and (str(x['lossstate']).split()[1] =='-') and   (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=1) ) else "",axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$',''))[0] if((x['CarrierClaimNumber'] !=0) and (len(str(x['lossstate']).split())>=2 ) and (str(x['lossstate']).split()[0] =='-') and (x['Total_EXPENSE'] =="") and   (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=1) ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$',''))[1] if (((x['CarrierClaimNumber1'] !=0)or (x['CarrierClaimNumber'] !=0) ) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=2)and (str(x['lossstate']).count('.')>=2)and (x['Total_EXPENSE'] =="") )else x['Total_EXPENSE'],axis=1)#.astype(float)
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"").replace('$','') if ((x['CarrierClaimNumber'] !=0) and ('.' not in str(x['USD']))and (x['Total_EXPENSE'] =="") )else x['Total_EXPENSE'],axis=1)#.astype(float)
        popular_table['Total_EXPENSE'] = popular_table['Total_EXPENSE'].apply(lambda x:float(0) if ((x=="")or(str(x)=='nan')or(str(x)=='-')) else str(x).replace('$',""))

        popular_table['TotalReserve'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Loss_Description']).replace(',',"").replace('$',''))[0] if(((x['CarrierClaimNumber1'] !=0) or ((x['ClaimStatus'] =="") and (x['CarrierClaimNumber'] !=0) ) ) and  (len(re.findall('(\d+\.\d{2})', str(x['Loss_Description']))) >=1) and (len(str(x['Loss_Description']).split()) ==1) ) else "",axis=1 )
        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Loss_Description']).split()[0].replace(',',"").replace('$','') if((x['CarrierClaimNumber1'] !=0)and (x['TotalReserve'] =="") and  (len(re.findall('(\d+\.\d{2})', str(x['Loss_Description']))) >=1) and (len(str(x['Loss_Description']).split()) ==2) ) else x['TotalReserve'],axis=1 )
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="")or(str(x)=='nan')or(str(x)=='-')) else str(x).replace('$',""))


        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['Total_EXPENSE']) + float(x['TotalPaid']) +float(x['TotalReserve']) ,axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and (x['LossDate'] !="")) else"" ,axis=1)

        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['CarrierClaimNumber'] !=0)|(popular_table['CarrierClaimNumber1'] !=0)|(popular_table['LineOfBusiness'] !="") )]

        popular_table['CarrierClaimNumber2']  = popular_table.apply(lambda x:x['CarrierClaimNumber'] if x['CarrierClaimNumber']!=0 else (x['CarrierClaimNumber1'] if x['CarrierClaimNumber1']!=0 else "" ) ,axis=1)

        popular_table['CarrierClaimNumber3'] = popular_table['CarrierClaimNumber2'].shift(-1)
        popular_table['CarrierClaimNumber4'] = popular_table['CarrierClaimNumber2'].shift(1)
        popular_table['TotalGrossIncurred2'] = popular_table['TotalGrossIncurred'].shift(-1)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['CarrierClaimNumber3'] if ((x['CarrierClaimNumber2']=="")and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['CarrierClaimNumber4'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") and (x['CarrierClaimNumber']!=0)  ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred2'] if ((x['TotalGrossIncurred']== 0.00)and (x['CarrierClaimNumber2']=="") )else x['TotalGrossIncurred'] ,axis=1)
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber3','CarrierClaimNumber4','TotalGrossIncurred2'],inplace=True,axis=1)

        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['LineOfBusiness'] !=""))]
        popular_table['LineOfBusiness'] = popular_table['LineOfBusiness'].shift(1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !=""))]
        popular_table['schema']=8
        return popular_table

    def TM_Post_Processing_12(self,popular_table):
        searchfor = ['NET', 'Incurred','Adjuster']
        popular_table['CarrierName'] ="Tokiyo Marine & philadelphia"
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']).split('PRODUCER:')[0] if 'PRODUCER:' in str(x['claim number']) else "",axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossstate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossdate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[~popular_table['Loss_Description'].str.contains('|'.join(searchfor) ,na=False)]
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ((row.astype(str).str.contains('NO CLAIM').any()) or (row.astype(str).str.contains('No Loss').any()) or (row.astype(str).str.contains('No Reported Losses').any()) or (row.astype(str).str.contains('NO LOSSES REPORTED').any()) )  else "", axis=1)
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['lossdate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['Status'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1']  = popular_table['CarrierClaimNumber1'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)

        popular_table['CoverageType'] = popular_table['Status'].str.extract('([A-Za-z _]*)', expand=True)
        popular_table['CoverageType'] = popular_table.apply(lambda x:x['CoverageType'] if x['CarrierClaimNumber'] !=0 else"",axis=1)
    #     popular_table['CoverageType'] = popular_table.apply(lambda x:self.is_lob(str(x['CoverageType'])),axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['lossstate'])) or ('Close' in str(x['lossstate'])) or ('CLOSE' in str(x['lossstate']))) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['lossstate'])) or ('Open' in str(x['lossstate'])) or ('OPEN' in str(x['lossstate']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['claim number'])) or ('Close' in str(x['claim number'])) or ('CLOSE' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['claim number'])) or ('Open' in str(x['claim number'])) or ('OPEN' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CLosed" if 'CL' in str(x['Status']) else x['ClaimStatus'] ,axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NP" if 'NP' in str(x['Status']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"RO" if 'RO' in str(x['Status']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'OP' in str(x['Status']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CL" if 'CL' in str(x['Closed_Date']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NP" if 'NP' in str(x['Closed_Date']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"RO" if 'RO' in str(x['Closed_Date']) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if 'OP' in str(x['Closed_Date']) else x['ClaimStatus'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))[0] if  ((str(x['Closed_Date']).count('/')==2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Closed_Date']))) >=1 ))  else '' ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))[0] if (('/' not in  str(x['LossDate'])) and (str(x['L.O.B']).count('/')==2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))) >=1 ) ) else x['LossDate']  ,axis =1 )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))[0] if ((str(x['Closed_Date']).count('/')==2) and (str(x['L.O.B']).count('/')==2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['L.O.B']))) >=1 ) ) else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Adjuster_Name']))[0] if ( ('/' not in x['NoticeDate']) and (str(x['Adjuster_Name']).count('/')==2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Adjuster_Name']))) >=1 ) ) else x['NoticeDate'] ,axis =1 )

        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Adjuster_Name']))[0] if ((str(x['Closed_Date']).count('/')==2) and (str(x['Adjuster_Name']).count('/')==2) ) else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Loss_Description']))[0] if ( ('/' not in x['ClosedDate']) and (str(x['Loss_Description']).count('/')==2)and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['Loss_Description']))) >=1 ) ) else x['ClosedDate'] ,axis =1 )

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and ((x['LossDate'] !="") or (x['NoticeDate'] !="")  )) else"" ,axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Loss_Description']).replace(',',"").replace('$',''))[0] if ((len(re.findall('(\d+\.\d{2})', str(x['Loss_Description']))) >=1 ) and ((x['CarrierClaimNumber'] !=0) or (x['ClaimStatus']!='' )) )else "",axis=1)#.astype(float)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['USD']).replace(',',"").replace('$',''))[0] if ((x['TotalPaid']=="") and (len(re.findall('(\d+\.\d{2})', str(x['USD']))) >=1 ) and ('.'  in  str(x['Total1'])) and ((x['CarrierClaimNumber'] !=0) or (x['ClaimStatus']!='' ))  )  else x['TotalPaid'],axis=1)#.astype(float)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Closed_Date']).replace(',',"").replace('$',''))[0] if ((x['CarrierClaimNumber1'] !=0) and (len(re.findall('(\d+\.\d{2})', str(x['Closed_Date']))) >=1  ) and (x['TotalPaid'] =="") ) else x['TotalPaid'] ,axis=1)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="") or (str(x)=='0.00')) else x )

        popular_table['Total_Expense'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['USD']).replace(',',"").replace('$',''))[0] if (('.' not in  str(x['Total1'])) and (len(re.findall('(\d+\.\d{2})', str(x['USD']))) >=1)  and (x['CarrierClaimNumber'] !=0) and (x['ClaimStatus']!='' )) else "",axis=1)#.astype(float)
        popular_table['Total_Expense'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Total']).replace(',',"").replace('$',''))[0] if ((x['Total_Expense'] =="") and (len(re.findall('(\d+\.\d{2})', str(x['Total']))) >=1) and ('.'  in  str(x['Total1'])) and ((x['CarrierClaimNumber'] !=0) or (x['ClaimStatus']!='' )) ) else x['Total_Expense'],axis=1)#.astype(float)
        popular_table['Total_Expense'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['L.O.B']).replace(',',"").replace('$',''))[0] if ((x['CarrierClaimNumber1'] !=0) and (len(re.findall('(\d+\.\d{2})', str(x['L.O.B']))) >=1  ) and (x['Total_Expense'] =="") ) else x['Total_Expense'] ,axis=1)
        popular_table['Total_Expense'] = popular_table['Total_Expense'].apply(lambda x:float(0) if ((x=="") or (str(x)=='0.00')) else x )

        popular_table['TotalReserve'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Total']).replace(',',"").replace('$',''))[0] if ( (len(re.findall('(\d+\.\d{2})', str(x['Total']))) >=1)  and ((x['CarrierClaimNumber1'] !=0) or (x['ClaimStatus'] =='' ))) else "",axis=1)#.astype(float)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") or (str(x)=='nan')) else x )


        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['Total_Expense']) + float(x['TotalPaid']) ,axis=1)


        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['CarrierClaimNumber'] !=0)|(popular_table['CarrierClaimNumber1'] !=0)|(popular_table['LineOfBusiness'] !="") )]
        popular_table['CarrierClaimNumber3'] = popular_table['CarrierClaimNumber1'].shift(-1)
        popular_table['CarrierClaimNumber4'] = popular_table['CarrierClaimNumber'].shift(1)
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].shift(-1)
        popular_table['CarrierClaimNumber']  = popular_table.apply(lambda x:x['CarrierClaimNumber3'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['TotalGrossIncurred']  = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if (x['TotalGrossIncurred']== "") else x['TotalGrossIncurred'] ,axis=1)
        popular_table['CarrierClaimNumber']  = popular_table.apply(lambda x:x['CarrierClaimNumber4'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].apply(lambda x:np.nan if x==0 else x)
        popular_table['CarrierClaimNumber'].fillna(method='ffill', inplace=True)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x: np.nan if x['No Losses'] !="" else x['CarrierClaimNumber'] ,axis=1 )
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber3','CarrierClaimNumber4','TotalGrossIncurred1'],inplace=True,axis=1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="") | (popular_table['LossDate'] !="")|(popular_table['LineOfBusiness'] !="") )]
        popular_table['schema']=12
        return popular_table

    def TM_Post_Processing_9(self,popular_table):
        searchfor = ['NET', 'Incurred','Adjuster']
        popular_table['CarrierName'] ="Tokiyo Marine & philadelphia"
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']).split('PRODUCER:')[0] if 'PRODUCER:' in str(x['claim number']) else "",axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossstate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:str(x['claim number']) if (('PRODUCER:' in str(x['lossdate'])) and (str(x['LineOfBusiness']) =="" ) ) else x['LineOfBusiness'],axis=1)

        popular_table = popular_table[~popular_table['Loss_Description'].str.contains('|'.join(searchfor) ,na=False)]
        popular_table['No Losses'] = popular_table.apply(lambda row:"No Claims" if ( (row.astype(str).str.contains('No Claimants').any()) or (row.astype(str).str.contains('No Reported Losses').any()) )  else "", axis=1)
        popular_table['No claims'] = popular_table.apply(lambda x:'No Claims' if ((("Total for Account:" in  str(x['claim number']) ) and ('0 Claims' in str(x['lossstate']) ) ) or(("POLICY TOTAL:" in  str(x['claim number']) ) and ('CLAIMS: 0' in str(x['lossstate'])) ) )  else "" ,axis=1 )
        popular_table['CarrierClaimNumber'] = popular_table['claim number'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber1'] = popular_table['lossstate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber2'] = popular_table['lossdate'].str.extract('([A-Z]+\d{8,}|\d{11,13})', expand=True)
        popular_table['CarrierClaimNumber3'] = np.nan
        popular_table['CarrierClaimNumber3']  = popular_table['CarrierClaimNumber3'].combine_first(popular_table['CarrierClaimNumber1'])
        popular_table['CarrierClaimNumber3']  = popular_table['CarrierClaimNumber3'].combine_first(popular_table['CarrierClaimNumber2'])
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber1'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber2'].fillna(0,inplace = True)
        popular_table['CarrierClaimNumber3'].fillna(0,inplace = True)

        popular_table['CarrierPolicyNumber'] = popular_table['claim number'].apply(lambda x:x if 'POLICY NO.:' in str(x) else np.nan).shift(1)
        popular_table['CarrierPolicyNumber1'] = popular_table['claim number'].str.extract('([A-Z]{3}-[A-Z]{2}[0-9]{3}-[0-9]{2})', expand=True)
        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber1'])
        popular_table['CarrierPolicyNumber'].fillna(0,inplace = True)

        popular_table['CoverageType'] = popular_table['lossdate'].str.extract('([A-Za-z _]*)', expand=True)
        popular_table['CoverageType'] = popular_table.apply(lambda x:x['CoverageType'] if x['CarrierClaimNumber'] !=0 else x['CoverageType'],axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:self.is_lob(str(x['CoverageType'])),axis=1)
        #     popular_table['CoverageType']

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('NO CLAIM' in str(x['claim number'])) or ('Close' in str(x['claim number'])) or ('CLOSE' in str(x['claim number']))) else"",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['claim number'])) or ('Open' in str(x['claim number'])) or ('OPEN' in str(x['claim number']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if (('close' in str(x['lossstate'])) or ('Close' in str(x['lossstate'])) or ('CLOSE' in str(x['lossstate']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (('open' in str(x['lossstate'])) or ('Open' in str(x['lossstate'])) or ('OPEN' in str(x['lossstate']))) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NO CLAIM" if 'NO CLAIM' in str(x['claim number']) else x['ClaimStatus'] ,axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CL" if (('CL' in str(x['Status']) ) and (x['ClaimStatus']=="") ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NP" if (('NP' in str(x['Status']) ) and (x['ClaimStatus']=="") ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"CL" if (('CL' in str(x['lossstate']) ) and (x['ClaimStatus']=="") ) else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"NP" if (('NP' in str(x['lossstate']) ) and (x['ClaimStatus']=="") ) else x['ClaimStatus'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:re.findall('(\d{2}\/\d{2}\/\d{2,4})' ,str(x['lossdate']))[0] if (('/' in str(x['lossdate'])) and (str(x['lossdate']).count('/')==2) and (len(re.findall('(\d{2}\/\d{2}\/\d{2,4})', str(x['lossdate']))) >=1 ) ) else (x['Status'] if (('/' in str(x['Status'])) and (str(x['Status']).count('/')==2) )  else "") ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['claim number'].split()[-3] if (('/' not in str(x['lossdate'])) and ('/' in str(x['claim number']) ) and ( len(str(x['claim number']).split() ) >3) )else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Status'].split()[-3] if (('/' not in str(x['lossdate'])) and ('/' in str(x['Status']) ) and ( len(str(x['Status']).split() ) >3) )else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:str(x['lossstate']).split()[2] if (  (len(str(x['lossstate']).split()) >2) and ('/' not in str(x['LossDate'])) and (str(x['lossstate']).count('/')>2) ) else x['LossDate'] ,axis =1 )
        popular_table['LossDate'] = popular_table['LossDate'].apply(lambda x:x if str(x).count('/')==2 else "" )

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status'] if (('/' in str(x['Status'])) and (str(x['Status']).count('/')==2) ) else '' ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['claim number'].split()[-2] if (('/' not in str(x['NoticeDate'])) and ('/' in str(x['claim number']) ) and ( len(str(x['claim number']).split() ) >3)) else x['NoticeDate'] ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Status'].split()[-2] if (('/' not in str(x['NoticeDate'])) and ('/' in str(x['Status']) ) and ( len(str(x['Status']).split() ) >3)) else x['NoticeDate'] ,axis =1 )
        popular_table['NoticeDate'] = popular_table.apply(lambda x:str(x['lossstate']).split()[3] if (  (len(str(x['lossstate']).split()) >3) and ('/' not in str(x['NoticeDate'])) and (str(x['lossstate']).count('/')>2) ) else x['NoticeDate'] ,axis =1 )
        popular_table['NoticeDate'] = popular_table['NoticeDate'].apply(lambda x:x if str(x).count('/')==2 else "" )


        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Closed_Date'] if (('/' in str(x['Closed_Date'])) and (str(x['Closed_Date']).count('/')==2) ) else '' ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['claim number'].split()[-1] if (('/' not in str(x['ClosedDate'])) and ('/' in str(x['claim number']) ) and ( len(str(x['claim number']).split() ) >3)) else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Status'].split()[-1] if (('/' not in str(x['ClosedDate'])) and ('/' in str(x['Status']) ) and ( len(str(x['Status']).split() ) >3)) else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table.apply(lambda x:str(x['lossstate']).split()[4] if (  (len(str(x['lossstate']).split()) >4) and ('/' not in str(x['ClosedDate'])) and (str(x['lossstate']).count('/')>2) ) else x['ClosedDate'] ,axis =1 )
        popular_table['ClosedDate'] = popular_table['ClosedDate'].apply(lambda x:x if str(x).count('/')==2 else "" )

        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$'," ") )[0] if (((x['CarrierClaimNumber1']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$'," ") )) >=1 )) or (('$' in str(x['lossstate']) ) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',"").replace('$'," ") )) >=1 ) ) ) else "",axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossdate']).replace(',',""))[0] if (((x['CarrierClaimNumber2']!=0) and (x['TotalPaid'] =="") and (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=1 )) or (('$' in str(x['lossdate']) ) and (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=1 ) and (x['TotalPaid']=="") ) ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['Status']).replace(',',"") if ((x['CarrierClaimNumber1']!=0) and (x['TotalPaid']=="") and (('.' in str(x['Status'])) or ('$' in str(x['Status']) and (x['TotalPaid']=="") ))  ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['Status']).replace(',',"") if ((x['CarrierClaimNumber2']!=0) and (x['TotalPaid']=="") and (('.' in str(x['Status'])) or ('$' in str(x['Status']) and (x['TotalPaid']=="") ))  ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"") if ((x['CarrierClaimNumber']!=0) and (x['TotalPaid']=="") and (('.' in str(x['Closed_Date'])) or ('$' in str(x['Closed_Date']) and (x['TotalPaid']=="") ))  ) else x['TotalPaid'],axis=1 )
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if x=="" else x.replace('$',""))

        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossstate']).replace(',',""))[1] if (((x['CarrierClaimNumber1']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=2 ) and (str(x['lossstate']).count('.')>=2)) or (('$' in str(x['lossstate']) ) and (len(re.findall('(\d+\.\d{2})', str(x['lossstate']))) >=2 )  and (str(x['lossstate']).count('.')>=2) ) ) else "",axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossdate']).replace(',',""))[1] if (((x['CarrierClaimNumber2']!=0) and (x['Total_EXPENSE']=="") and  (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=2 ) and (str(x['lossdate']).count('.')>=2)) or (('$' in str(x['lossdate']) ) and (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=2 )  and (str(x['lossdate']).count('.')>=2) ) ) else x['Total_EXPENSE'] ,axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['lossdate']).replace(',',""))[0] if (((x['CarrierClaimNumber1']!=0) and (x['Total_EXPENSE']=="") and (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=1 )) or (('$' in str(x['lossdate']) ) and (x['Total_EXPENSE']=="") and  (len(re.findall('(\d+\.\d{2})', str(x['lossdate']))) >=1 ) ) ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"") if ((x['CarrierClaimNumber1']!=0) and (x['Total_EXPENSE']=="") and  (('$' in str(x['Closed_Date'])) or ('.' in str(x['Closed_Date']) ) )  ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:str(x['Closed_Date']).replace(',',"") if ((x['CarrierClaimNumber2']!=0) and (x['Total_EXPENSE']=="") and  (('$' in str(x['Closed_Date'])) or ('.' in str(x['Closed_Date']) ) )  ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table.apply(lambda x:str(x['L.O.B']).replace(',',"") if ((x['CarrierClaimNumber']!=0) and (x['Total_EXPENSE']=="") and  (('$' in str(x['L.O.B'])) or ('.' in str(x['L.O.B']) ) )  ) else x['Total_EXPENSE'],axis=1 )
        popular_table['Total_EXPENSE'] = popular_table['Total_EXPENSE'].apply(lambda x:float(0) if x=="" else x.replace('$',""))

        popular_table['TotalReserve'] = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Adjuster_Name']).replace(',',""))[0] if ((x['CarrierClaimNumber3']!=0) and (len(re.findall('(\d+\.\d{2})', str(x['Adjuster_Name']))) >=1 ) and (x['ClaimStatus'] =="")  ) else (str(x['Adjuster_Name']).replace(',',"").replace('$',"") if (((x['CarrierClaimNumber3']!=0) or (x['CarrierClaimNumber']!=0)) and ('$' in str(x['Adjuster_Name']) ) and (x['ClaimStatus'] =="") ) else"" ),axis=1 )

        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") or (str(x)=="nan") or ("-" in str(x)) ) else x.replace('$',""))


        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(str(x['Total_EXPENSE'])) + float(str(x['TotalPaid'])) + float(str(x['TotalReserve'])),axis=1)
    #     popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(str(x['Total_EXPENSE'])) + float(str(x['TotalPaid'])),axis=1)
    #     print(popular_table['TotalGrossIncurred'])

        popular_table['Premium']  = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Closed_Date']).replace(',',""))[0] if ((len(re.findall('(\d+\.\d{2})', str(x['Closed_Date']))) >=1) and (x['CarrierPolicyNumber'] != 0))   else "" ,axis=1)
        popular_table['Claims + LAE']  = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['L.O.B']).replace(',',""))[0] if ((len(re.findall('(\d+\.\d{2})', str(x['L.O.B']))) >=1) and (x['CarrierPolicyNumber'] != 0)) else "",axis=1)
        popular_table['Paid Claims +LAE']  = popular_table.apply(lambda x:re.findall('(\d+\.\d{2})', str(x['Adjuster_Name']).replace(',',""))[0] if ((len(re.findall('(\d+\.\d{2})', str(x['Adjuster_Name']))) >=1) and (x['CarrierPolicyNumber'] != 0) ) else "",axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:x['ClaimStatus'] if ((x['ClaimStatus'] != "") and ((x['LossDate'] !="") or (x['NoticeDate'] !="") ) )else"" ,axis=1)

        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="")|(popular_table['CarrierPolicyNumber'] !=0)|(popular_table['CarrierClaimNumber3'] !=0)|(popular_table['CarrierClaimNumber'] !=0)| (popular_table['LineOfBusiness'] !="") )]
        popular_table['CarrierClaimNumber4'] = popular_table['CarrierClaimNumber3'].shift(-1)
        popular_table['CarrierClaimNumber4'].fillna(0,inplace = True)
        popular_table['TotalGrossIncurred1'] = popular_table['TotalGrossIncurred'].shift(-1)
        popular_table['CarrierClaimNumber'] = popular_table.apply(lambda x:x['CarrierClaimNumber4'] if ((x['CarrierClaimNumber']==0)and (x['No Losses'] =="") ) else x['CarrierClaimNumber'] ,axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['TotalGrossIncurred1'] if (x['TotalGrossIncurred']== 0.00) else x['TotalGrossIncurred'] ,axis=1)
        popular_table.drop(['CarrierClaimNumber1','CarrierClaimNumber2','CarrierClaimNumber4','CarrierClaimNumber3','TotalGrossIncurred1'],inplace=True,axis=1)
        popular_table=popular_table[( (popular_table['No Losses'] !="") | (popular_table['ClaimStatus'] !="") |(popular_table['CarrierPolicyNumber'] !=0)| (popular_table['LossDate'] !="")| (popular_table['LineOfBusiness'] !="") )]
        popular_table['schema']=9
        return popular_table

    def is_lob(self,string):
        pal = []
        ps = []
        lp = []
        for _val in self.cov_lst:
            ls = self.match_keys(string,_val)
            pal.append(ls)
    #         print(ls)
        try:
            test_list = [i for i in pal if i]
        #     print(test_list)
            a = [(test_list[i][1]) for i in range(len(test_list))]
            for i in range(len(test_list)):
                if max(a) == test_list[i][1]:
                    ps.append(test_list[i][0])
            for _val in self.cov_lst:
                lp = self.match_keys(ps[0],_val)

                try:
                    if lp[1]>=60:
                        return _val
                except:
                    continue
        except:
            pass


    def replace(self,text) :
        for k,v in self.replace_dict.items():
            text = text.replace(k,v)
        return text

    def lcs_sim(self,s1_text, s2_text , fuzzy_thresh=40):
        s1_text , s2_text = self.replace(s1_text) , self.replace(s2_text)
        s1 = s1_text.split(' ')#nltk.word_tokenize(s1_text)
        s2 = s2_text.split(' ')
        matrix = [["" for x in range(len(s2))] for x in range(len(s1))]
        for i in range(len(s1)):
            for j in range(len(s2)):
                if fuzz.ratio(s1[i] , s2[j]) >= 65 :
                    if i == 0 or j == 0:
                        matrix[i][j] = s1[i]# + '|' + str(i)
                    else:
                        matrix[i][j] = matrix[i-1][j-1] + ' ' + s1[i]# + '|' + str(i)
                else:
                    matrix[i][j] = max(matrix[i-1][j], matrix[i][j-1],
                                       key=lambda x: fuzz.ratio(x , s2[j]) + len(x.split()))

        matrix = [x for x in matrix if x and x[-1] in s1_text]
        cs = matrix[-1][-1]
        if fuzz.ratio(cs,s2_text) > 0:
            return cs,fuzz.ratio(cs,s2_text)#,len(cs.split(' ')), cs
        else:
            return ''#,0

    def match_keys(self,text,key):
        text = text.lower()
        key = key.lower()
        return self.lcs_sim(text,key)

    ############# combined code for getting popular table for schema of any length  ##############

    def get_popular_table(self,new_table):

        schemas_len=[7,8,9,11,12]
        schemas=[["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B',
                                    'SubmissionID'],
                 ['claim number', 'lossstate', 'lossdate', 'Status', 'Closed_Date',
                                    "Loss_Description",'USD','SubmissionID'],
                 ["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                    'Loss_Description','SubmissionID'],
                 ['claim number', 'lossstate', 'lossdate', 'Status', 'Closed_Date',
                           'L.O.B', 'Adjuster_Name', 'Loss_Description', 'USD', 'Total','SubmissionID'],
                 ["claim number",'lossstate',"lossdate",'Status','Closed_Date','L.O.B','Adjuster_Name',
                                    'Loss_Description',"USD","Total","Total1",'SubmissionID']
                ]
        sch={}
        df={}
        k=0
        for i in schemas_len:
            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):

            l=len(new_table[a].columns)
            if l in schemas_len:
                new_table[a].columns = sch[l]
                sch['popular_table_'+str(l)].append(new_table[a])

        for k,v in df.items():
            try:
                k_sch_len=''.join(re.findall(r'\d',k))
                df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)
            except:
                continue

        return df

    ############# combined code for post_processing for schemas of any length  ##############
    def Post_Processing(self,df):
        post_pro=[self.TM_Post_Processing_7,self.TM_Post_Processing_8,self.TM_Post_Processing_9,self.TM_Post_Processing_11,
                 self.TM_Post_Processing_12]
        post_pro_fun=['TM_Post_Processing_7','TM_Post_Processing_8','TM_Post_Processing_9','TM_Post_Processing_11',
                 'TM_Post_Processing_12']
        for idx ,pp in enumerate(post_pro):
            sch_len=str(post_pro_fun[idx]).split('Processing_')[1]

            try:
                df['df_'+sch_len]=pp(df['df_'+sch_len])
            except Exception as e:
                print('there was an error with your input in schema of length-{0}  : {1}'.format(sch_len,e))
                df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
                continue

        return df
