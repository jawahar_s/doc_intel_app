import pandas as pd
import numpy as np
import re
from  fuzzywuzzy import fuzz

class Travelers():

    def __init__(self):
        self.new_tab=[]
        print("", end="")

    def Travelers_1(self, popular_table):
        popular_table['CarrierName'] ="Travelers"
        popular_table['CarrierClaimNumber'] = popular_table['Claim Number']
        popular_table['CarrierClaimNumber1']  = popular_table['FP'].str.extract(r'([A-Z]{1,3}.*\d{4})', expand=True)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].apply(lambda x:x if ((len(str(x)) ==7) and ("$" not in x )) else np.nan)
        popular_table['CarrierClaimNumber']  = popular_table['CarrierClaimNumber'].combine_first(popular_table['CarrierClaimNumber1'])

        popular_table['LineOfBusiness'] = popular_table.apply(lambda x:x['Claimant'].split("Line of Insurance:")[1] if 'Line of Insurance:' in x['Claimant'] else np.nan,axis=1)

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Medical'] if (('$' in str(x['Total_inc']) ) and ('$' in str(x['Medical']) ) and ('$' not in str(x['Total'])) )else "",axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Total'] if '$' in str(x['Total']) else x['TotalGrossIncurred'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:x['Claim Number'] if (('$' in str(x['Claim Number'])) and ('$' not in str(['TotalGrossIncurred'])) ) else x['TotalGrossIncurred'],axis=1)

        popular_table['LossDate'] = popular_table.apply(lambda x:x['Claim Number'] if (('/' in x['Claim Number']) and (str(x['Claim Number']).count('/')==2) ) else ("loss date not found" if len(str(x['CarrierClaimNumber']))>5 else np.nan) ,axis =1 )
        popular_table['LossDate'] = popular_table.apply(lambda x:x['Accident Date'] if (('/' in x['Accident Date']) and ('/' not in str(x['LossDate'])) ) else x['LossDate'] ,axis =1 )

        popular_table['ClaimStatus']  = popular_table.apply(lambda x:"Closed" if  str(x['O/C']) =='C'  else np.nan,axis=1)
        popular_table['ClaimStatus']  = popular_table.apply(lambda x:"Open" if  str(x['O/C']) =='O'  else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus']  = popular_table.apply(lambda x:"Closed" if  str(x['Close Date']) =='C'  else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus']  = popular_table.apply(lambda x:"Open" if  str(x['Close Date']) =='O'  else x['ClaimStatus'],axis=1)
        popular_table['ClaimStatus1'] = popular_table.apply(lambda x:"Closed" if  str(x['Total']) =='C'  else np.nan,axis=1)
        popular_table['ClaimStatus1'] = popular_table.apply(lambda x:"Open" if  str(x['Total']) =='O'  else x['ClaimStatus1'],axis=1).shift(1)
        popular_table['ClaimStatus']  = popular_table['ClaimStatus'].combine_first(popular_table['ClaimStatus1'])

        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Accident Date'] if str(x['Claim Number']).count('/')==2  else ("Notice date not found" if len(str(x['CarrierClaimNumber']))>5 else np.nan),axis=1)
        popular_table['NoticeDate'] = popular_table.apply(lambda x:x['Notice Date'] if (('/' not in str(x['NoticeDate'])) and (str(x['Notice Date']).count('/')==2))  else x['NoticeDate'],axis=1)

        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Notice Date'] if str(x['Claim Number']).count('/')==2  else  ("Close date not found" if len(str(x['CarrierClaimNumber']))>5 else np.nan),axis=1)
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Close Date'] if '/' in str(x['Close Date']) else x['ClosedDate'],axis=1)

        popular_table = popular_table[(popular_table['TotalGrossIncurred'] !="") | (popular_table['LineOfBusiness'] !="") | (popular_table['CarrierClaimNumber'] !=0) ]
        popular_table.drop(['CarrierClaimNumber1','ClaimStatus1'],inplace=True, axis=1)
        popular_table['LineOfBusiness']=popular_table['LineOfBusiness'].shift(1)
    #     popular_table['CarrierClaimNumber'].ffill(axis = 0,inplace=True)
        popular_table['CarrierClaimNumber'].fillna(0,inplace = True)
        popular_table['ClosedDate'].ffill(axis = 0,inplace=True)
        popular_table['NoticeDate'].ffill(axis = 0,inplace=True)
        popular_table['ClaimStatus'].ffill(axis = 0,inplace=True)
        popular_table['LossDate'].ffill(axis = 0,inplace=True)
        popular_table['LineOfBusiness'].ffill(axis = 0,inplace=True)
        popular_table = popular_table[((popular_table['TotalGrossIncurred'] !="") & (popular_table['CarrierClaimNumber'] !=0) )]

        return {'df_10':popular_table}

    def Travelers(self, new_table):
        # print('Travelers',new_table)

        # self.trav_result=pd.DataFrame()
        for a in range(len(new_table)):
            # print(len(new_table[a].columns))
            if len(new_table[a].columns) == 16:
                new_table[a].columns =['Claimant', 'Adj Off', 'FP', 'Claim Number', 'Accident Date',
               'Notice Date', 'Close Date', 'O/C', 'Total', 'Claim', 'Medical',
               'Expense','Total_inc','SubmissionID','Page#','Order']

                self.new_tab.append(new_table[a])
        if len(self.new_tab) >0:
            df=pd.DataFrame()
            df = pd.concat(self.new_tab)
            return self.Travelers_1(df)
        else:
            df=pd.DataFrame()
            return {'df_10':df}
