import pandas as pd
import numpy as np
import re

class Cincinnati() :

    def __init__(self) :
        print("cincinnati")

    def Cincinnati(self, popular_table):
        """
        """
        df=self.get_popular_table(popular_table)
        df=self.Post_Processing(df)
        return df


    def get_popular_table(self,new_table):

        schemas_len=[16]
        schemas=[
                ['Sorted by Date of Loss Policy Loc', 'Oc Policy', 'Loss Effect',
           'Loss Named Insured', '#', 'End Rsv or Cat', 'Date', 'Loss Description',
           'Type', 'Claimant/Payee', 'Paid', 'Salv/Subr', 'Expense',
           'Month Closed', 'Incurred', 'SubmissionID', 'Page#', 'Order']
        ]
        sch={}
        df={}
        k=0
        for i in schemas_len:
            sch['popular_table_'+str(i)] = []
            sch[i]=schemas[k]
            k+=1
            df['df_'+str(i)]=pd.DataFrame()
        for a in range(len(new_table)):

            l=len(new_table[a].columns)
            if l-2 in schemas_len:
                new_table[a].columns = sch[l-2]
                sch['popular_table_'+str(l-2)].append(new_table[a])

        for k,v in df.items():
            k_sch_len=''.join(re.findall(r'\d',k))
            df[k]=pd.concat(sch['popular_table_'+k_sch_len],ignore_index=True)

        return df

    def Post_Processing(self,df):

        post_pro=[self.cincinnati_Post_Processing_16]
        post_pro1=['cincinnati_Post_Processing_16']
        for idx,pp in enumerate(post_pro):
            sch_len=str(post_pro1[idx])[-2:]

            try:
                df['df_'+sch_len]=pp(df['df_'+sch_len])
            except Exception as e:
                print('there was an error with your input in schema of length-{0}  : {1}'.format(sch_len,e))
                df['df_'+sch_len]=pd.DataFrame(columns=df['df_'+sch_len].columns)
        # print("post processing",df)
        return df

    def cincinnati_Post_Processing_16(self,popular_table):
        popular_table['CarrierName'] ="cincinnati"
        popular_table['No Losses'] = popular_table.apply(lambda row:"NO LOSSES" if row.astype(str).str.contains('NO LOSSES').any()  else "", axis=1)


        popular_table['CarrierPolicyNumber']  = popular_table['Sorted by Date of Loss Policy Loc'].str.extract(r'([A-Z]*[0-9]{7})', expand=True)
        popular_table['CarrierPolicyNumber1'] = popular_table['Oc Policy'].str.extract(r'([A-Z]*[0-9]{7})', expand=True)

        popular_table['CarrierPolicyNumber']  = popular_table['CarrierPolicyNumber'].combine_first(popular_table['CarrierPolicyNumber1'])

        popular_table['LossDate'] = popular_table['Date'].str.extract(r'([0-9]{2}-[0-9]{2}-[0-9]*)', expand=True)
        popular_table['LossDate1'] = popular_table['End Rsv or Cat'].str.extract(r'([0-9]{2}-[0-9]{2}-[0-9]*)', expand=True)
        popular_table['LossDate']  = popular_table['LossDate'].combine_first(popular_table['LossDate1'])

        popular_table['PolicyPeriodStart'] = popular_table['Sorted by Date of Loss Policy Loc'].str.extract(r'([0-9]{2}-[0-9]{2}-[0-9]*)', expand=True)
        popular_table['PolicyPeriodStart'] = popular_table['Oc Policy'].str.extract(r'([0-9]{2}-[0-9]{2}-[0-9]*)', expand=True)
        popular_table['PolicyPeriodStart'] = popular_table['Loss Effect'].str.extract(r'([0-9]{2}-[0-9]{2}-[0-9]*)', expand=True)

        popular_table['ClosedDate'] = popular_table['Month Closed'].apply(lambda x:x if "/" in x else "")
        popular_table['ClosedDate'] = popular_table.apply(lambda x:x['Expense'] if (("/" in x['Expense'])and (x['ClosedDate'] =="" ) ) else x['ClosedDate'],axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['Paid']).replace(',',"") if ((x['Incurred'] !="") and (str(x['Paid']).replace(',',"").isdigit()))  else ""  ,axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x:str(x['Claimant/Payee']).replace(',',"") if ( (x['TotalPaid'] =="") and (str(x['Claimant/Payee']).replace(',',"").isdigit()) and (x['Incurred'] =="") ) else x['TotalPaid']  ,axis=1)
        popular_table['TotalPaid'] = popular_table['TotalPaid'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',""))

        popular_table['TotalExpense'] = popular_table.apply(lambda x:str(x['Expense']).replace(',',"") if ((x['Incurred'] !="") and ( str(x['Expense']).replace(',',"").isdigit()) ) else ""  ,axis=1)
        popular_table['TotalExpense'] = popular_table.apply(lambda x:str(x['Salv/Subr']).replace(',',"") if ( (x['TotalExpense'] =="") and (str(x['Salv/Subr']).replace(',',"").isdigit()) and (x['Incurred'] =="")  ) else x['TotalExpense']  ,axis=1)
        popular_table['TotalExpense'] = popular_table['TotalExpense'].apply(lambda x:float(0) if ((x=="") ) else x.replace('$',""))

        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Month Closed']).replace(',',"") if ((x['Incurred'] !="") and (str(x['Month Closed']).replace(',',"").isdigit()) and ('/' not in x['Month Closed'])) else ""  ,axis=1)
        popular_table['TotalReserve'] = popular_table.apply(lambda x:str(x['Expense']).replace(',',"") if ( (x['TotalReserve'] =="")and (str(x['Expense']).replace(',',"").isdigit()) and (x['Incurred'] =="") and ('/' not in x['Expense']) ) else x['TotalReserve']  ,axis=1)
        popular_table['TotalReserve'] = popular_table['TotalReserve'].apply(lambda x:float(0) if ((x=="") )else x.replace('$',""))

        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x: float(x['TotalPaid']) + float(x['TotalExpense']) + float(x['TotalReserve']),axis=1)

        popular_table['CoverageType'] = popular_table['Type']
        popular_table['CoverageType'] = popular_table.apply(lambda x:x['End Rsv or Cat'] if re.search(r'\d',x['CoverageType']) else x['CoverageType'],axis=1)
        popular_table['CoverageType'] = popular_table.apply(lambda x:x['CoverageType'] if ( not x['CoverageType'] or len(x['CoverageType'].split())==1)
                                                            else(x['CoverageType'] if (len(x['CoverageType'].split())==2 and len(x['CoverageType'].split()[0])<5 and len(x['CoverageType'].split()[1])<5)
                                                                 else (x['CoverageType'].split()[0]+' '+x['CoverageType'].split()[1] if (len(x['CoverageType'].split())>2 and len(x['CoverageType'].split()[0])<5 and len(x['CoverageType'].split()[1])<5)
                                                                       else (x['Loss Description'] if (len(x['CoverageType'].split())>=2 and (len(x['CoverageType'].split()[0])>=5 or len(x['CoverageType'].split()[1])>=5))  else x['CoverageType']))),axis=1)


        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Closed" if "/" in x['ClosedDate'] else "",axis=1)
        popular_table['ClaimStatus'] = popular_table.apply(lambda x:"Open" if (("/" not in x['ClosedDate']) and (x['TotalReserve'] != "") )else x['ClaimStatus'],axis=1)

        popular_table['ClaimStatus'] = popular_table.apply(lambda x:'' if (x['No Losses']=='NO LOSSES' or (x['Oc Policy']=='' and re.search(r'([0-9]{2}-[0-9]{2}-[0-9]*)',str(x['Date'])) ) ) else x['ClaimStatus'],axis=1)

        popular_table['TotalPaid'] = popular_table.apply(lambda x:'' if (x['No Losses']=='NO LOSSES') else x['TotalPaid'] ,axis=1)
        popular_table['TotalReserve'] = popular_table.apply(lambda x:'' if (x['No Losses']=='NO LOSSES') else x['TotalReserve'],axis=1)
        popular_table['TotalGrossIncurred'] = popular_table.apply(lambda x:'' if (x['No Losses']=='NO LOSSES') else x['TotalGrossIncurred'],axis=1)


        popular_table = popular_table[~popular_table['Claimant/Payee'].str.contains("Total") ]
        popular_table = popular_table[~popular_table['Date'].str.contains("Total") ]
        popular_table = popular_table[(popular_table['LossDate'] !="") |  (popular_table['ClaimStatus'] !="")]
        popular_table = popular_table[((popular_table['No Losses']=='NO LOSSES')|(popular_table['Type']!='')|(popular_table['Claimant/Payee']!='')|(popular_table['Oc Policy']==popular_table['CarrierPolicyNumber']))]
        popular_table = popular_table[((popular_table['Date']!='')|(popular_table['CoverageType']!='')|(popular_table['Oc Policy']!=''))]


        popular_table['LossDate'].ffill(axis = 0,inplace=True)
        popular_table['LossDate'] = popular_table.apply(lambda x:'' if (x['No Losses']=='NO LOSSES') else x['LossDate'],axis=1)

        popular_table['CarrierPolicyNumber'].ffill(axis = 0,inplace=True)

        popular_table = popular_table.sort_values(["SubmissionID",'Page#','Order'], ascending = (True,True,True))
        popular_table['LineOfBusiness']=None
        popular_table['LineOfBusiness']=popular_table.apply(lambda x:x['CoverageType'].upper() if x['CoverageType'] else x['LineOfBusiness'],axis=1)
        popular_table['LineOfBusiness']=popular_table.apply(lambda x:x['LineOfBusiness'].upper() if x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)

        popular_table.LineOfBusiness.replace( ['WTR AD','BI','MED','PILFER','COLL','PDLPCO','WTR PR','PROF','FALOBJ','V&MM','PD LPS','BIPIPS','BILPCO',
                                    'HAIL','LIA','GLASS','TR CAN','WTRSE','FLOOD','UIM BI','PHY DM','FIRE','WC MED','UM PD','LTNG','WIND',
                                    'A THEF','WC IND','COBF','EMPL L','WT I&S','ELECTL','COLAPS','TH PRT','UNDGR',
                                    'BS','DIS BN','THEFT','RENTAL','BURG'],
                                   ['Property','GL','GL','Property','Auto','GL','Property','E&O','Property','Auto','Auto','Auto','GL',
                                    'Property','GL','Auto','Property','Property','Property','Auto','Auto','Property','WC','Auto',
                                   'Property','Property','Property','WC','Auto','GL','Property','Property','Property','Property','Property',
                                   'Auto','WC','Property','Auto','Property'], inplace=True)

        popular_table.LineOfBusiness.replace( ['WC','PO','P/O','PD','PL','CPP','FL','F&A','F/A','AL','K&R','K/R','EPL','P','D&O','E&O','GL',
                                     'AUTO LIABILITY','AUTO PROPERTY DAMAGE','AUTO PROP DAM',
                                      'AUTO LIAB (PROP DAMAGE/RENTAL)','AUTO LIABILITY / PD ONLY','AUTO LIABILITY - BODILY INJURY',
                                      'AUTO LIABILITY BODILY INJ','AUTO BODILY INJURY','UNINS MOTORIST','POLLUTION COVERAGES',
                                    'CONTRACTUAL LIABILITY','PRODUCTS','BA','CU','ECO','CBP','BAA','BIA','BLA','BKS','PR',
                                     'AUTO PHYSICAL DAMAGE'],
                                   ['Workers Compensation','Premises Operations','Premises Operations','Property Damage',
                                    'Public Liability','Commercial Package Policy','Financial Lines','Fire and Allied',
                                   'Fire and Allied','Auto','Kidnap and Ransom','Kidnap and Ransom',
                                    'Employment Practices Liability','Property','Directors and Officers','Errors and Omissions',
                                   'General Liability','Auto','Auto','Auto','Auto','Auto','Auto','Auto','Auto',
                                   'Auto','Auto','Auto','Auto','Business Auto','Commercial Umbrella','Commercial Excess Liability',
                                    'Commercial Package Prop','Business Auto','Emp Benefits Liability','General Liability',
                                    'Commercial Package','Property and Casualty','Auto'], inplace=True)
        # print("popular_table",popular_table)
        return popular_table
