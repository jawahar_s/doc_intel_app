Page: 5 of 20
                                                                                  Philadelphia Insurance Companies
9/3/2015 12:09 PM
                                                                                     Customer Loss Detail Report


Account     8038461    HILLEL ACADEMY
            9
Product     FF         Flexi Plus Five

Policy Number       PHSD787389             12/01/2012   TO   12/01/2013

                                                                                                                                                                                  Subro/Sal
Claim Number           Claimant/Driver Name                      Loss Type                             Status   Loss Date      Open       Closed       Loss/Exps Paid            Recovered Rep
PHFF13020701045        Thomas Giddens                            Flexi Five Part2 Wrongful Terminati    OP      02/26/2013   02/26/2013                        9,006.34                0.00 Lawson

PHFF13040711628        William Kraft                             Flexi Five Part2 Age Discrimination    OP      04/11/2013   04/12/2013                        6,360.19                0.00 Lawson

Total Number of Claims For Policy        PHSD787389          2                                                                                     Total Loss/Exp Paid:   15,366.53

                                                                                                                                              Salv/Subro Recovered:       0.00

                                                                                                                                                       Total Reserves:    177,779.42

                                                                                                                                                        Total Incurred:   193,145.95
