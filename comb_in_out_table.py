import pandas as pd
import time
import numpy as np
import re

class in_out:
    def __init__(self):
        pass


    def merage_in_out(self,lob_df,master_table):
        print(",lob_df,master_table",lob_df,master_table)
        if master_table.empty == False:
            if len(master_table['Page#'].value_counts())>0 and len(master_table['Order'].value_counts())>0:
                # print("master_table --->1-->>>",master_table)
                lob_df['Submission_ID']=lob_df.apply(lambda x : x['Submission_ID']+'.pdf',axis=1)
                lob_df.rename(columns = {'Submission_ID':'SubmissionID'}, inplace = True)
                lob_df['lob']=lob_df.apply(lambda x : x['LINE OF BUSINESS'],axis=1)
                lob_df['lob'] = lob_df.apply(lambda x:np.nan if x['lob']=='NOT FOUND' else x['lob'],axis=1)
                lob_df['lob'] = lob_df.groupby(['SubmissionID'], sort=False)['lob'].apply(lambda x: x.ffill())
                lob_df['lob'] = lob_df['lob'].replace([np.nan], [None])
                result_join = pd.merge(master_table, lob_df, how='left',on=['SubmissionID','Page#','Order'])
                result_join = result_join.fillna(np.nan).replace([np.nan], [None])
                result_join['Intable/Outtable']=result_join.apply(lambda x: 'In_table' if x['LineOfBusiness'] else 'Out_table',axis=1)
                result_join['LineOfBusiness']=result_join.apply(lambda x: x['LineOfBusiness'] if x['LineOfBusiness'] else x['LINE OF BUSINESS'],axis=1)
                result_join['LineOfBusiness'] = result_join.apply(lambda x:'' if (x['LineOfBusiness']=='NOT FOUND' or (x['Intable/Outtable']=='Out_table' and not x['LineOfBusiness'] )) else x['LineOfBusiness'],axis=1)
                result_join['LineOfBusiness'] = result_join.apply(lambda x:np.nan if (not x['LineOfBusiness'] and x['Intable/Outtable']=='Out_table') else x['LineOfBusiness'],axis=1)
                result_join['LineOfBusiness'] = result_join.groupby(['SubmissionID','Intable/Outtable'], sort=False)['LineOfBusiness'].apply(lambda x: x.ffill())
                result_join['LineOfBusiness'] = result_join['LineOfBusiness'].replace([np.nan], [None])
                result_join['LINE OF BUSINESS'] = result_join.apply(lambda x:x['lob'] if (x['lob'] and x['LINE OF BUSINESS']=='NOT FOUND') else x['LINE OF BUSINESS'],axis=1)
                result_join['LineOfBusiness'] = result_join.apply(lambda x:x['LINE OF BUSINESS'] if ( (x['LineOfBusiness']=='NOT FOUND' or not x['LineOfBusiness']) and x['LINE OF BUSINESS'] and x['LINE OF BUSINESS']!='NOT FOUND') else x['LineOfBusiness'],axis=1)
                result_join.drop(['lob','LINE OF BUSINESS'],inplace=True,axis=1)
            if len(master_table['Page#'].value_counts())>=0 and len(master_table['Order'].value_counts())>=0:
                if len(master_table['Page#'].value_counts())==0 and len(master_table['Order'].value_counts())==0:
                    result_join=master_table
                result_join['LineOfBusiness']=result_join.apply(lambda x:str(x['LineOfBusiness']).upper() if x['LineOfBusiness'] else x['LineOfBusiness'],axis=1)
                result_join.LineOfBusiness.replace( ['WC','PO','P/O','PD','PL','CPP','FL','F&A','F/A','AL','K&R','K/R','EPL','P','D&O','E&O','GL',
                                                'AUTO LIABILITY','AUTO PROPERTY DAMAGE','AUTO PROP DAM',
                                                 'AUTO LIAB (PROP DAMAGE/RENTAL)','AUTO LIABILITY / PD ONLY','AUTO LIABILITY - BODILY INJURY',
                                                 'AUTO LIABILITY BODILY INJ','AUTO BODILY INJURY','UNINS MOTORIST','POLLUTION COVERAGES',
                                               'CONTRACTUAL LIABILITY','PRODUCTS','BA','CU','ECO','CBP','BAA','BIA','BLA','BKS','PR',
                                                'AUTO PHYSICAL DAMAGE'],
                                              ['Workers Compensation','Premises Operations','Premises Operations','Property Damage',
                                               'Public Liability','Commercial Package Policy','Financial Lines','Fire and Allied',
                                              'Fire and Allied','Auto','Kidnap and Ransom','Kidnap and Ransom',
                                               'Employment Practices Liability','Property','Directors and Officers','Errors and Omissions',
                                              'General Liability','Auto','Auto','Auto','Auto','Auto','Auto','Auto','Auto',
                                              'Auto','Auto','Auto','Auto','Business Auto','Commercial Umbrella','Commercial Excess Liability',
                                               'Commercial Package Prop','Business Auto','Emp Benefits Liability','General Liability',
                                               'Commercial Package','Property and Casualty','Auto'], inplace=True)

                result_join['CarrierClaimNumber'] = result_join.apply(lambda x:np.nan if not x['CarrierClaimNumber'] else x['CarrierClaimNumber'],axis=1)
                result_join['CarrierClaimNumber'] = result_join.groupby(['SubmissionID'], sort=False)['CarrierClaimNumber'].apply(lambda x: x.ffill())
                result_join['CarrierClaimNumber'] = result_join['CarrierClaimNumber'].replace([np.nan], [None])
                return result_join
        else:
            return master_table

    def get_defined_columns(self,dframe):
        if dframe.empty==True:

            dframe = pd.DataFrame(columns=['No Losses','CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
                  'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalReserve','TotalPaid',
                  'TotalGrossIncurred','TotalRecoveries','ValuationDate','NoticeDate','ClosedDate',
                   'CarrierPolicyNumber','CoverageType','LossState','LossDescription','PolicyYear',
                   'LocationOfProperty','LossBasis','LossCause','Peril', 'SubmissionID','CarrierName','Page#','Order'])
            return dframe
        else:
            original_cols=['No Losses','CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
                  'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalReserve','TotalPaid',
                  'TotalGrossIncurred','TotalRecoveries','ValuationDate','NoticeDate','ClosedDate',
                   'CarrierPolicyNumber','CoverageType','LossState','LossDescription','PolicyYear',
                   'LocationOfProperty','LossBasis','LossCause','Peril', 'SubmissionID','CarrierName','Page#','Order']
            cols=list(dframe.columns)
    #             print(cols)
            absents=set(original_cols).difference(cols)
    #             print(absents)
            for absent in absents:
                dframe[absent]=None
            dframe=dframe[original_cols]
            return dframe
