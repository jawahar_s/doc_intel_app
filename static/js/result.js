var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}


function my() {
    var checkboxes = document.getElementById("checkboxes");
    checkboxes.style.display = "none";
    var ss = document.getElementById("ss");
    var file_visible1 = document.getElementById("file_style1");
    var file_visible2 = document.getElementById("file_style2");
    var file_visible3 = document.getElementById("file_style3");
    var file_visible4 = document.getElementById("file_style4");
    var file_visible5 = document.getElementById("file_style5");
    var file_visible6 = document.getElementById("file_style6");

    ss.style.visibility = "visible";
    // 'CarrierName','CarrierClaimNumber','LineOfBusiness','LossDate','NoticeDate','ClosedDate','ClaimStatus','TotalGrossIncurred'

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == false && (document.getElementById('NoticeDate').checked) == false && (document.getElementById('ClosedDate').checked) == false && (document.getElementById('ClaimStatus').checked) == false && (document.getElementById('TotalGrossIncurred').checked) == false) {
        file_visible1.style.display = "block";
        file_visible2.style.display = "none";
        file_visible3.style.display = "none";
        file_visible4.style.display = "none";
        file_visible5.style.display = "none";
        file_visible6.style.display = "none";
    }

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == true && (document.getElementById('NoticeDate').checked) == false && (document.getElementById('ClosedDate').checked) == false && (document.getElementById('ClaimStatus').checked) == false && (document.getElementById('TotalGrossIncurred').checked) == false) {
        file_visible1.style.display = "none";
        file_visible2.style.display = "block";
        file_visible3.style.display = "none";
        file_visible4.style.display = "none";
        file_visible5.style.display = "none";
        file_visible6.style.display = "none";
    }

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == true && (document.getElementById('NoticeDate').checked) == true && (document.getElementById('ClosedDate').checked) == false && (document.getElementById('ClaimStatus').checked) == false && (document.getElementById('TotalGrossIncurred').checked) == false) {
        file_visible1.style.display = "none";
        file_visible2.style.display = "none";
        file_visible3.style.display = "block";
        file_visible4.style.display = "none";
        file_visible5.style.display = "none";
        file_visible6.style.display = "none";
    }

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == true && (document.getElementById('NoticeDate').checked) == true && (document.getElementById('ClosedDate').checked) == true && (document.getElementById('ClaimStatus').checked) == false && (document.getElementById('TotalGrossIncurred').checked) == false) {
        file_visible1.style.display = "none";
        file_visible2.style.display = "none";
        file_visible3.style.display = "none";
        file_visible4.style.display = "block";
        file_visible5.style.display = "none";
        file_visible6.style.display = "none";
    }

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == true && (document.getElementById('NoticeDate').checked) == true && (document.getElementById('ClosedDate').checked) == true && (document.getElementById('ClaimStatus').checked) == true && (document.getElementById('TotalGrossIncurred').checked) == false) {
        file_visible1.style.display = "none";
        file_visible2.style.display = "none";
        file_visible3.style.display = "none";
        file_visible4.style.display = "none";
        file_visible5.style.display = "block";
        file_visible6.style.display = "none";
    }

    if ((document.getElementById('LineOfBusiness').checked) == true && (document.getElementById('LossDate').checked) == true && (document.getElementById('NoticeDate').checked) == true && (document.getElementById('ClosedDate').checked) == true && (document.getElementById('ClaimStatus').checked) == true && (document.getElementById('TotalGrossIncurred').checked) == true) {
        file_visible1.style.display = "none";
        file_visible2.style.display = "none";
        file_visible3.style.display = "none";
        file_visible4.style.display = "none";
        file_visible5.style.display = "none";
        file_visible6.style.display = "block";
    }
}

function show(shown, hidden) {
    document.getElementById(shown).style.display='block';
    document.getElementById(hidden).style.display='none';
    return false;
  }

// CODE FOR SLIDE SHOW
// ===================

// var slideIndex = 1;
// showSlides(slideIndex);

// // Next/previous controls
// function plusSlides(n) {
//     showSlides(slideIndex += n);
// }

// // Thumbnail image controls
// function currentSlide(n) {
//     showSlides(slideIndex = n);
// }

// function showSlides(n) {
//     var i;
//     var slides = document.getElementsByClassName("mySlides");
//     var dots = document.getElementsByClassName("dot");
//     if (n > slides.length) { slideIndex = 1 }
//     if (n < 1) { slideIndex = slides.length }
//     for (i = 0; i < slides.length; i++) {
//         slides[i].style.display = "none";
//     }
//     for (i = 0; i < dots.length; i++) {
//         dots[i].className = dots[i].className.replace(" active", "");
//     }
//     slides[slideIndex - 1].style.display = "block";
//     dots[slideIndex - 1].className += " active";
// }

// ===========================